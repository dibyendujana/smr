package au.com.wp.corp.sys.corporate.selfreader.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;

/**
 *
 */
public class HelpPanelImpl extends Composite implements HelpPanel {
    interface HelpPanelUiBinder extends UiBinder<HTMLPanel, HelpPanelImpl> {
    }

    private static HelpPanelUiBinder ourUiBinder = GWT.create(HelpPanelUiBinder.class);

    @UiField
    Button okButton;
    @UiField
    ScrollPanel scrollPanel;

    // this is the actual popup that appears. we add a help panel to this popup.
    private PopupPanel popup;

    public HelpPanelImpl() {
        initWidget(ourUiBinder.createAndBindUi(this));

        popup = new PopupPanel();
        popup.setStyleName("help-PopUpPanel");
        popup.setWidget(this);
        popup.setAutoHideEnabled(true);
        popup.setGlassEnabled(true);
    }

    /**
     * Show the help panel
     * @param alert the message to display
     * @param callingWidget the widget to display stuff relative to
     */
    public void show(String alert, Widget callingWidget) {
        scrollPanel.clear();
        scrollPanel.add(new HTML(alert));
        popup.showRelativeTo(callingWidget);
    }

    /**
     * Hide the help panel
     */
    @Override
    public void hide() {
        popup.hide();
    }

    /**
     * Clicking on either the ok or close button will close the window
     * @param e unused
     */
    @SuppressWarnings("unused")
    @UiHandler("okButton")
    public void onCloseClick(ClickEvent e) {
        popup.hide();
    }
}
