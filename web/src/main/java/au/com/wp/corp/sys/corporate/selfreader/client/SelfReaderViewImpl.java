package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.gwtuploadext.WPBaseUploadStatus;
import au.com.wp.corp.sys.corporate.selfreader.client.meters.*;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.*;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import eu.maydu.gwt.validation.client.DefaultValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.actions.StyleAction;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;
import gwtupload.client.*;

import java.util.Date;
import java.util.List;

public class SelfReaderViewImpl extends Composite implements SelfReaderView {
    private static SelfReaderViewImplUiBinder uiBinder = GWT.create(SelfReaderViewImplUiBinder.class);

    interface SelfReaderViewImplUiBinder extends UiBinder<Widget, SelfReaderViewImpl> {
    }

    private static final DateTimeFormat DDMMYYYY = DateTimeFormat.getFormat("dd/MM/yyyy");
    private static final DateTimeFormat DDMMYY = DateTimeFormat.getFormat("dd/MM/yy");
    private static final DateTimeFormat DAYMONTHYEAR = DateTimeFormat.getFormat("dd MMMM yyyy");
    private static final String INITIAL_METER_MESSAGE = "Please complete the below details, otherwise your account will be estimated.";
    private static final String INITIAL_EMAIL_ADDRESS_MESSAGE = "Enter your email address here if receipt required.";
    private static final String SUCCESS_MESSAGE = "Thankyou for your meter reading. Your reading will be used for your next invoice.";
    private static final String SUCCESS_WITH_DISCLAIMER_MESSAGE = "Thankyou for your meter reading. Your reading, or an estimate, may be used for your next invoice as ";
    private static final String REGISTER_VALIDATION_MESSAGE = "There was a validation warning when validating the register submissions.";

    private Presenter presenter;
    private List<SkipCodeDTO> skipCodes;
    private PopupPanel savingPanel;
    private PopupPanel responsePanel;
    private ErrorPanel errorPanel;

    private Label responseMessage;
    @UiField
    Button submitButton;
    @UiField
    CheckBox overrideBox;
    @UiField
    Label nmiNumber;
    @UiField
    Label meterNumber;
    @UiField
    Label lastReadDate;
    @UiField
    Label nextReadDate;
    @UiField
    DateBox readDate;
    @UiField
    Label meterUserMessage;
    @UiField
    Label address;
    @UiField
    SimplePanel meterDetailsPanel;

    // File upload required functionality
    //   @UiField
    //   FormPanel uploadImageForm;
    //   @UiField
    //   FileUpload fileUpload;
    //   @UiField
    //   Button uploadButton;
    @UiField
    HTMLPanel uploadImagePanel;
    @UiField
    Label warningMessage;
    @UiField
    CaptionPanel warningMessageContainer;
    @UiField
    Image meterInformationHelp;
    @UiField
    Image meterReadHelp;
    @UiField(provided = true)
    MultiUploader uploader;
    @UiField
    ValidationErrorWidget meterReadDateValidationError;
    @UiField
    ValidationErrorWidget emailAddressValidationError;
    @UiField
    HTMLPanel overridePanel;
    @UiField
    TextBox emailAddress;


    private MeterDTO meter;
    private MeterWidget meterWidget;

    private ValidationProcessor validator;
    private ValidationMessages messages;


    public SelfReaderViewImpl() {

        initGWTUploadFileUpload();

        setupValidator();

        initWidget(uiBinder.createAndBindUi(this));
        createLoadingPanel();
        createResponseMessagePanel();
        readDate.setFormat(new DateBox.DefaultFormat(DDMMYY));
        meterUserMessage.setText(INITIAL_METER_MESSAGE);
        emailAddress.setText(INITIAL_EMAIL_ADDRESS_MESSAGE);
        meterWidget = null;
        errorPanel = new ErrorPanel();

        readDate.getTextBox().addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent focusEvent) {
                if (readDate.getTextBox().getText().length() > 0) {
                    readDate.hideDatePicker();
                }
            }
        });

    }

    public void initGWTUploadFileUpload() {
        uploader = new MultiUploader(IFileInput.FileInputType.BROWSER_INPUT, new WPBaseUploadStatus(this));


        IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {
            @Override
            public void onFinish(IUploader uploader) {
                if (uploader.getStatus() == IUploadStatus.Status.SUCCESS) {
                    // The server sends useful information to the client by default
                    IUploader.UploadedInfo info = uploader.getServerInfo();

                    //System.out.println("File name " + info.name);
                    //System.out.println("File content-type " + info.ctype);
                    //System.out.println("File size " + info.size);
                    //System.out.println("Server message " + info.message);

                    JSONObject response = (JSONObject) JSONParser.parse(info.message);
                    JSONString fileIdValue = (JSONString) response.get("fileId");

                    if (fileIdValue != null) {
                        String fileId = fileIdValue.stringValue();
                        presenter.setUploadedImageFileName(fileId);
                    }

                    // Enable the submit button
                    submitButton.setEnabled(true);
                } else {
                    submitButton.setEnabled(true);
                }
            }
        };

        IUploader.OnChangeUploaderHandler onChangeUploaderHandler = new IUploader.OnChangeUploaderHandler() {
            @Override
            public void onChange(IUploader uploader) {
                switch (uploader.getStatus()) {
                    case SUBMITING:
                    case INPROGRESS:
                    case CANCELING:
                    case QUEUED:
                    case REPEATED:
                    case UNINITIALIZED:
                    case CHANGED:
                        // On any change disable ability to submit
                        submitButton.setEnabled(false);
                        break;
                    case CANCELED:
                        // When its cancelled enable ability to submit
                        submitButton.setEnabled(true);
                        break;
                    default:
                        // Anything else anyable ability to submit
                        submitButton.setEnabled(true);
                        break;

                }
            }
        };

        IUploader.OnCancelUploaderHandler onCancelUploaderHandler = new IUploader.OnCancelUploaderHandler() {
            @Override
            public void onCancel(IUploader uploader) {
                submitButton.setEnabled(true);
            }
        };


        //uploader.addOnStartUploadHandler(onStartUploaderHandler);
        uploader.addOnCancelUploadHandler(onCancelUploaderHandler);
        uploader.addOnChangeUploadHandler(onChangeUploaderHandler);
        uploader.addOnFinishUploadHandler(onFinishUploaderHandler);
    }

    @Override
    public void hideShowingCalendar() {
        if (readDate != null && readDate.isDatePickerShowing()) {
            readDate.hideDatePicker();
        }
    }

    @Override
    public void setSkipCodes(List<SkipCodeDTO> skipCodes) {
        this.skipCodes = skipCodes;
    }

    private void createLoadingPanel() {
        savingPanel = new PopupPanel(false, true);
        savingPanel.setGlassEnabled(true);
        savingPanel.setTitle("Submitting");
        VerticalPanel loadingContent = new VerticalPanel();
        loadingContent.add(new Label("Submitting"));
        loadingContent.add(new Image("images/loading.gif"));
        savingPanel.setWidget(loadingContent);
    }

    private void createResponseMessagePanel() {
        responsePanel = new PopupPanel(true, true);
        responsePanel.setGlassEnabled(true);
        responsePanel.setTitle("Meter Reading submission");
        responseMessage = new Label();
        responsePanel.setWidget(responseMessage);
    }

    @Override
    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showSubmitting() {
        savingPanel.center();
    }

    @Override
    public void hideSubmitting() {
        savingPanel.hide();
    }

    @Override
    public void setMeter(MeterDTO meter, boolean displayOverride) {
        // first reset/enable all fields
        this.meter = meter;
        readDate.removeStyleName("invalidField");
        readDate.setEnabled(true);
        meterDetailsPanel.clear();
        overrideBox.setValue(false);
        overridePanel.setVisible(displayOverride);
        submitButton.setEnabled(true);
        emailAddress.setEnabled(true);

        if (meter == null) {
            readDate.setValue(null);
            meterNumber.setText(null);
            nmiNumber.setText(null);
            address.setText(null);
            lastReadDate.setText(null);
            nextReadDate.setText(null);
            meterWidget = null;
            emailAddress.setText("");
            uploader.reset();
        } else {
            // By default dont show upload
            showFileUpload(false);

            readDate.setValue(meter.getActualReadDate());
            meterNumber.setText(meter.getMeterNumber());
            nmiNumber.setText(meter.getNmi());
            address.setText(meter.getSiteAddress());
            Date lastReadDateValue = meter.getLastReadDate();
            if (lastReadDateValue != null) {
                lastReadDate.setText(DDMMYY.format(lastReadDateValue));
            } else {
                lastReadDate.setText(null);
            }
            Date nextReadDateValue = meter.getNextReadDate();
            if (nextReadDateValue != null) {
                nextReadDate.setText(DDMMYY.format(nextReadDateValue));
            } else {
                nextReadDate.setText(null);
            }
            emailAddress.setText(meter.getEmailAddress());
            switch (meter.getMeterType()) {
                case NDL:
                    meterWidget = new NDLMeterWidget(meter, skipCodes, presenter, validator);
                    // Always show file upload for this meter
                    showFileUpload(true);

                    break;
                case REV:
                    meterWidget = new REVMeterWidget(meter, skipCodes, presenter, validator);
                    // Always show file upload for this meter
                    showFileUpload(true);

                    break;
                case SEVENDL:
                    // TODO - Decommission this meter ???
                    meterWidget = new SevenDLMeterWidget(meter, skipCodes, validator);
                    break;
                case ONEFIVED:
                    meterWidget = new OneFiveDMeterWidget(meter, skipCodes, presenter, validator, false);
                    break;
                case RBTWO:
                    meterWidget = new RBTwoMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case RBONE:
                    meterWidget = new RBOneMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case OFP:
                    meterWidget = new OFPMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case TBTWO:
                    meterWidget = new TBTwoMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case TOU:
                    meterWidget = new TOUMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case TBONE:
                    // TODO - Decommission this meter ???
                    meterWidget = new TBOneMeterWidget(meter, skipCodes, validator, true);
                    break;
                case SMA:
                    meterWidget = new SMAMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case TSM:
                    meterWidget = new TSMMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case REONE:
                    meterWidget = new REOneMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case RETWO:
                    meterWidget = new RETwoMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case RB3:
                    meterWidget = new RB3MeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case RE3:
                    meterWidget = new RE3MeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case LE3:
                    meterWidget = new LE3MeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case LE1:
                    meterWidget = new LE1MeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
                case LZR:
                    meterWidget = new LZRMeterWidget(meter, skipCodes, presenter, validator, true);
                    break;
            }
            if (meterWidget != null) {
                meterDetailsPanel.setWidget(meterWidget);
            }
        }
        if (emailAddress.getText() == null || emailAddress.getText().isEmpty()) {
            emailAddress.setText(INITIAL_EMAIL_ADDRESS_MESSAGE);
        }

    }

    @Override
    public void disableFields() {
        //disable all the fields
        readDate.setEnabled(false);
        submitButton.setEnabled(false);
        meterWidget.disable(true);
        emailAddress.setEnabled(false);

    }

    @Override
    public void showSuccessMessage() {
        // show the success message dialog.
        responseMessage.setText(SUCCESS_MESSAGE);
        responsePanel.center();
    }

    @Override
    public void showSuccessWithDisclaimerMessage(String message) {
        // show the success message dialog with the additional disclaimer message.
        responseMessage.setText(SUCCESS_WITH_DISCLAIMER_MESSAGE + message);
        responsePanel.center();
    }

    @Override
    public void showFailureMessage(String message) {
        responseMessage.setText(message);
        responsePanel.center();
    }

    @Override
    public void showRegisterValidationMessage() {
        responseMessage.setText(REGISTER_VALIDATION_MESSAGE);
        responsePanel.center();
    }

    @UiHandler("submitButton")
    public void onSubmit(ClickEvent e) {
        if (validateFields()) {
            meter = meterWidget.getMeter();
            meter.setActualReadDate(readDate.getValue());
            // grab the override flag from the override checkbox
            boolean overrideFlag = overrideBox.getValue();
            if (emailAddress.getText().equals(INITIAL_EMAIL_ADDRESS_MESSAGE)) {
                meter.setEmailAddress(null);
            }
            else {
                meter.setEmailAddress(emailAddress.getText());
            }

            presenter.submitMeter(meter, overrideFlag);
        }
    }

    @UiHandler("emailAddress")
    public void onEmailAddressFocus(FocusEvent e) {
        if (INITIAL_EMAIL_ADDRESS_MESSAGE.equals(emailAddress.getText())) {
            emailAddress.setText("");
        }
    }
    @UiHandler("emailAddress")
    public void onEmailAddressBlur(BlurEvent e) {
        if ((emailAddress.getText() == null) || ("".equals(emailAddress.getText().trim()))) {
            emailAddress.setText(INITIAL_EMAIL_ADDRESS_MESSAGE);
        }
    }

    private boolean validateFields() {
        boolean valid = doValidation();
        return valid;
    }


    @Override
    public void displayErrorMessage(String message) {
        errorPanel.show(message, new Command() {
            @Override
            public void execute() {
                // refresh the captcha widget on close of errorpanel.
            }
        });
    }

    @Override
    public void showFileUpload(boolean visible) {
        uploadImagePanel.setVisible(visible);
    }

    public void showUnsubmittableMeterMessage() {
        showWarningMessage("This meter is outside its reading window. You can submit a reading, however it may not be used for billing purposes.");
    }

    public void showWarningMessage(String message) {
        warningMessageContainer.setVisible(true);
        warningMessage.setText(message);
    }

    public void hideWarningMessage() {
        warningMessageContainer.setVisible(false);
    }

    @UiHandler("meterInformationHelp")
    public void onMeterInformationHelp(ClickEvent event) {
        presenter.displayHelp("Your NMI and Meter number have automatically populated your address." +
                " Please ensure this is correct.", meterInformationHelp);
    }

    @UiHandler("meterReadHelp")
    public void onMeterReadHelp(ClickEvent event) {
        presenter.displayHelp("Please select the date you read your meter from the calendar in 'This read date' box." +
                " The read date must be within the read window on or after the 'Next Read Date' displayed." +
                " We will accept readings outside the reading window but the reading may not be used for billing purposes." +
                " You must input the date you actually read the meter not the 'scheduled' read date."
                , meterReadHelp);
    }

    @Override
    public void showErrorInformation(String message) {
        presenter.showErrorInformation(message);
    }

    private void setupValidator() {
        messages = GWT.create(SelfreaderValidationMessagesImpl.class);
        validator = new DefaultValidationProcessor(messages);
    }

    public void resetValidator() {
        validator.reset();
        validator.removeValidatorsAndGlobalActions();


    }

    public void validateThisReadDate() {
        validator.addValidators("thisReadDate", new NotNullValidator(readDate.getTextBox())
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(meterReadDateValidationError))
        );
        validator.addValidators("thisReadDate1", new CompareToTodayValidator(readDate, CompareToTodayValidator.BeforeOrAfterTodaysDate.AFTER)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(meterReadDateValidationError))
        );
        validator.addValidators("thisReadDate2", new CompareToGivenDateTimeValidator(readDate, readDate.getValue(), meter.getLastReadDate(), "last read date", CompareToGivenDateTimeValidator.BeforeOrAfterCompareDate.BEFORE)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(meterReadDateValidationError))
        );

        Date oneDayBeforeNextReadDate = addDaysToDate(meter.getNextReadDate(), -1);

        validator.addValidators("thisReadDate3", new CompareToGivenDateTimeValidator(readDate, readDate.getValue(), oneDayBeforeNextReadDate, "next read date or at most one day before it", CompareToGivenDateTimeValidator.BeforeOrAfterCompareDate.BEFORE)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(meterReadDateValidationError))
        );
    }

    private Date addDaysToDate(Date date, int days) {
        Date dateResult = new Date(date.getTime());
        CalendarUtil.addDaysToDate(dateResult, days);
        return dateResult;
    }

    public void validateEmailAddress() {
        validator.addValidators("emailAddress", new EmailAddressFormatValidator(emailAddress)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(emailAddressValidationError))
        );
    }

    public boolean doValidation() {
        resetValidator();
        validateThisReadDate();
        meterWidget.validateMeter(validator);
        validateEmailAddress();

        return validator.validate();
    }

    @UiHandler("overrideBox")
    void clickOverrideWarnings(ClickEvent event) {
        if (overrideBox.getValue()) {
            presenter.showInformation("You can now submit your readings, however they may be ignored and an estimate used instead.");
            validator.reset();
        }
    }

}
