package au.com.wp.corp.sys.corporate.selfreader.server.util.logging;

import au.com.wp.corp.sys.corporate.selfreader.client.LoggingService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

/**
 * GWT RPC Service impl to log client side exceptions
 *
 * @author Thomas Buckel (n041982)
 */
public class LoggingServiceImpl extends RemoteServiceServlet implements LoggingService {

    private final Logger log = Logger.getLogger(LoggingServiceImpl.class);
    
    private CustomStackTraceDeobfuscator stackTraceDeobfuscator;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String symbolMapsDirectory = config.getInitParameter("symbolMapsDirectory");
        if (StringUtils.hasText(symbolMapsDirectory)) {
            stackTraceDeobfuscator = new CustomStackTraceDeobfuscator(getServletContext(), symbolMapsDirectory);
        } else {
            throw new IllegalArgumentException("Servlet parameter 'symbolMapsDirectory' not set for logging servlet in web.xml.");
        }
    }

    @Override
    public void logUncaughtException(String message, String exceptionClassName, StackTraceElement[] stackTrace) {
        String strongName = getPermutationStrongName();
        String remoteUser = getThreadLocalRequest().getRemoteUser();
        try {
            StackTraceElement[] deobfuscatedStackTrace;
            if ("HostedMode".equals(strongName)) {
                deobfuscatedStackTrace = stackTrace;
            } else {
                deobfuscatedStackTrace = (stackTrace != null) ? stackTraceDeobfuscator.deobfuscateStackTrace(stackTrace, strongName) : null;
            }
            ClientException exception = new ClientException(remoteUser, exceptionClassName, message,deobfuscatedStackTrace);
            log.error("Error in GWT client for user " + remoteUser, exception);
        } catch (Exception e) {
            log.error("Error deobfuscating GWT Exception (remoteUser=" + remoteUser + ", message=" + message + ", exceptionClassName=" + exceptionClassName + ")", e);
        }
    }

    private class ClientException extends Exception {
        
        private final String user;
        private final String clientExceptionClassName;
        private final String clientMessage;

        private ClientException(String user, String clientExceptionClassName, String clientMessage, StackTraceElement[] stackTrace) {
            this.user = user;
            this.clientExceptionClassName = clientExceptionClassName;
            this.clientMessage = clientMessage;
            if (stackTrace != null) {
                setStackTrace(stackTrace);
            }
        }

        @Override
        public String getMessage() {
            return "Exception on Client for user [" + user + "]: " + clientExceptionClassName + ": " + clientMessage;
        }
        
    }

}
