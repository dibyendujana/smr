package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import java.io.Serializable;
import java.lang.Exception;

/**
 * This Exception is thrown when the values entered by the user fail to validate with Google's ReCaptcha webservice.
 * <p>
 *     Possible causes of this exception are {@link #MISMATCH} and {@link #EXPIRED}.
 * </p>
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class FailedCaptchaValidationException extends Exception implements Serializable {

    /**
     * When a web session times out, the attributes that once existed are no longer present.
     * <p>
     *     As such it is not possible to compare the value entered by the user with
     *     anything that the server is aware of.
     * </p>
     */
    public static final String EXPIRED =  "Captcha expired. Try Again";

    /**
     * This is the normal use case where the person types something in that
     * does not match the Captcha value stored in the session.
     */
    public static final String MISMATCH =  "The words you entered for the Captcha failed. Please try again.";

    public FailedCaptchaValidationException() {
    }

    public FailedCaptchaValidationException(String message) {
        super(message);
    }
}
