package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

import au.com.westernpower.wp.mr.wsp.meterreadingsservices.Register;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The different types of available Meters.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public enum MeterType {
    REV("REV", 1),
    NDL("NDL", 1),
    ONEFIVED("15D", 1),
    SEVENDL("7DL", 1),
    RBONE("RB1", 2),
    RBTWO("RB2", 2),
    OFP("OFP", 2),
    TBTWO("TB2", 2),
    TOU("TOU", 3),
    TBONE("TB1", 3),
    TSM("TSM", 5),
    SMA("SMA", 5),
    REONE("RE1", 10),
    RETWO("RE2", 10),
    RB3("RB3", 2),
    RE3("RE3", 10),
    LE3("LE3", 6),
    LE1("LE1", 6),
    LZR("LZR", 2);

    private String code;
    private static final Map<String, MeterType> types = new HashMap<String, MeterType>();
    private int registerCount;

    static {
        for (MeterType meterType : EnumSet.allOf(MeterType.class)) {
            types.put(meterType.getCode(), meterType);
        }
    }

    MeterType(String code, int registerCount) {
        this.code = code;
        this.registerCount = registerCount;
    }

    public static MeterType getFromCode(String code) {
        return types.get(code);
    }

    public String getCode() {
        return code;
    }

    public int getRegisterCount() {
        return registerCount;
    }
}
