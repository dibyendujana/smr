package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.validation.*;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterError;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.actions.StyleAction;

/**
 * Handles common functionality for Dial type meters
 *
 * @author Isaac Stephes <isaac.stephens@westernpower.com.au>
 */
public abstract class AbstractDialMeterWidget extends AbstractMeterWidget {

    protected void assignValuesFromRegister(RegisterDTO registerDTO, CheckBox skipCodeBox, Label oldValue, IntegerBox fiveZero, IntegerBox fourZero, IntegerBox threeZero, IntegerBox twoZero, IntegerBox oneZero, IntegerBox noZero, ListBox skipCodeList, ValidationErrorWidget errorWidget) {

        if (registerDTO.getSkipCode() == null) {
            // Set the Value field
            String registerValue = "";
            if (registerDTO.getValue() != null) {
                Integer value = registerDTO.getValue();
                // Set all the registers
                fiveZero.setValue((value / 100000) % 10);
                fourZero.setValue((value / 10000) % 10);
                threeZero.setValue((value / 1000) % 10);
                twoZero.setValue((value / 100) % 10);
                oneZero.setValue((value / 10) % 10);
                noZero.setValue((value / 1) % 10);
            }

            // Set validation error
            RegisterError error = registerDTO.getErrorType();

            validator.addValidators("val01", new ServerRegisterValidator(fiveZero, error)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
            );
            validator.addValidators("val02", new ServerRegisterValidator(fourZero, error)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
            );
             validator.addValidators("val03", new ServerRegisterValidator(threeZero, error)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
            );
             validator.addValidators("val04", new ServerRegisterValidator(twoZero, error)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
            );
             validator.addValidators("val05", new ServerRegisterValidator(oneZero, error)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
            );
            validator.addValidators("val06", new ServerRegisterValidator(noZero, error)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
            );

        } else {
            skipCodeBox.setValue(true);

            fiveZero.setEnabled(false);
            fourZero.setEnabled(false);
            threeZero.setEnabled(false);
            twoZero.setEnabled(false);
            oneZero.setEnabled(false);
            noZero.setEnabled(false);

            for (int i = 0; i < skipCodeList.getItemCount(); i++) {
                if (skipCodeList.getValue(i).equals(registerDTO.getSkipCode().getCode())) {
                    skipCodeList.setItemSelected(i, true);
                    break;
                }
            }

            skipCodeList.setEnabled(true);
        }

        String registerOldValue = "";
        if (registerDTO.getOldValue() != null) {
            registerOldValue = String.valueOf(registerDTO.getOldValue());
        }
        oldValue.setText(registerOldValue);


    }

    protected boolean isFieldValid(IntegerBox box) {
        Integer value = box.getValue();
        if (value != null) {
            box.removeStyleName("invalidField");
            return true;
        } else {
            box.setTitle("A reading must be specified");
            box.addStyleName("invalidField");
            return false;
        }
    }

    protected String getClockAntiImage(Integer value) {
        if (value == null)
            return "images/clock_anti.gif";

        if (value >= 0 && value <= 9) {
            return "images/clock_anti_" + value + ".gif";
        }

        return "images/clock_anti.gif";
    }

    protected String getClockWiseImage(Integer value) {
        if (value == null)
            return "images/clock_wise.gif";

        if (value >= 0 && value <= 9) {
            return "images/clock_wise_" + value + ".gif";
        }

        return "images/clock_wise.gif";
    }


    protected void validateFieldRegister(ValidationProcessor validate, String validationName, IntegerBox value, CheckBox checkBox, ListBox listBox, ValidationErrorWidget errorWidget) {
        validate.addValidators(validationName + "0", new NotNullRegisterIntegerBoxValidator(value, checkBox, listBox)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
        );
    }

    protected void validateListBoxRegister(ValidationProcessor validate, String validationName, IntegerBox value, CheckBox checkBox, ListBox listBox, ValidationErrorWidget errorWidget) {
        validate.addValidators(validationName + "1", new NotNullRegisterListBoxValidator(value, checkBox, listBox)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
        );
    }

    protected String getDefaultDialMeterHelp() {
        String help = getDefaultNonDialMeterHelp() +
                "</br></br>If you experience a problem with sumbitting your meter reading and you are confident your reading is correct" +
                " you may upload a photo of your meter reading. Select the image from your local directory and then click 'upload' to store the photo.";
        return help;
    }
}
