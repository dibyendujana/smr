package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;

import java.io.Serializable;

/**
 * Thrown when the Webmethods webservice returns that the submission of the Meter Reading has been accepted, but there
 * are issues with the submission (so the submission may not be used).
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class SubmittedWithWarningException extends Exception implements Serializable {
    @SuppressWarnings("unused")
    private SubmittedWithWarningException() {
    }

    public SubmittedWithWarningException(String message) {
        super(message);
    }
}
