package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.InitialResultDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.FailedCaptchaValidationException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.InvalidMeterDataForDisplayTypeException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.MeterDetailsMismatchException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.RegisterSubmissionFailedException;
import com.google.gwt.user.client.rpc.AsyncCallback;
import net.tanesha.recaptcha.ReCaptchaException;

/**
 * This interface contains all of the remote service calls (RPC) for the Self Reader application.
 * 
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 *
 */
public interface SelfReaderServiceAsync {
    /**
     * Requests the validation of the ReCaptcha challenge, and the nmi/meter number combination.
     * If successful it will return the details of the meter.
     * @param nmi the NMI of the meter
     * @param meterNumber The meter number
     * @param challenge recaptcha challenge
     * @param response recaptcha response
     * @param browser the string representing the calling browser
     * @param asyncCallback the Async Callback containing the result from the request.
     */
	void getMeterDetails(String nmi, String meterNumber, String challenge, String response, boolean arrivedByLink, String browser, AsyncCallback<InitialResultDTO> asyncCallback);

    /**
     * Submits a meter containing a reading for all of the registers of that meter.
     * @param meter The meter dto.
     * @param overrideFlag Indicates if the override checkbox was selected by the user.
     * @param attemptNumber The submission attempt number (incremented each time that this method is called for this meter for this session (eg since authentication)).
     * @param browser the string representing the calling browser
     * @param asyncCallback The Async Callback containing the result from the request.
     */
	void submitMeterDetails(MeterDTO meter, boolean overrideFlag, int attemptNumber, String browser, String fileName, AsyncCallback<MeterDTO> asyncCallback);
}
