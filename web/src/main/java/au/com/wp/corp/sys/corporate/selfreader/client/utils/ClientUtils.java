package au.com.wp.corp.sys.corporate.selfreader.client.utils;

import com.claudiushauptmann.gwt.recaptcha.client.RecaptchaWidget;
import com.google.gwt.event.dom.client.KeyCodes;

import java.util.HashSet;

/**
 * Class for static helper methods to be used by the views in this app
 */
public final class ClientUtils {
    private ClientUtils() {
    }

    public static final RecaptchaWidget RECAPTCHA_WIDGET = new RecaptchaWidget("6LdPGMsSAAAAAFxua2aTyvDVwqqfGCTpmKvgJ8sJ");

    public static HashSet<Integer> keyCodes = new HashSet<Integer>();
    static {
        keyCodes.add(KeyCodes.KEY_ALT);
        keyCodes.add(KeyCodes.KEY_BACKSPACE);
        keyCodes.add(KeyCodes.KEY_CTRL);
        keyCodes.add(KeyCodes.KEY_DELETE);
        keyCodes.add(KeyCodes.KEY_DOWN);
        keyCodes.add(KeyCodes.KEY_END);
        keyCodes.add(KeyCodes.KEY_ENTER);
        keyCodes.add(KeyCodes.KEY_ESCAPE);
        keyCodes.add(KeyCodes.KEY_HOME);
        keyCodes.add(KeyCodes.KEY_LEFT);
        keyCodes.add(KeyCodes.KEY_PAGEDOWN);
        keyCodes.add(KeyCodes.KEY_PAGEUP);
        keyCodes.add(KeyCodes.KEY_RIGHT);
        keyCodes.add(KeyCodes.KEY_SHIFT);
        keyCodes.add(KeyCodes.KEY_TAB);
        keyCodes.add(KeyCodes.KEY_UP);
    }

    /**
     * Gets the name of the used browser.
     */
    public static native String getBrowserName() /*-{
        return navigator.userAgent.toLowerCase();
    }-*/;

    /**
     * Returns true if the current browser is Chrome.
     */
    public static boolean isChromeBrowser() {
        return getBrowserName().contains("chrome");
    }

    /**
     * Returns true if the current browser is Firefox.
     */
    public static boolean isFirefoxBrowser() {
        return getBrowserName().contains("firefox");
    }

    /**
     * Returns true if the current browser is IE (Internet Explorer).
     */
    public static boolean isIEBrowser() {
        return getBrowserName().contains("microsoft")||getBrowserName().contains("trident");
    }

    public static String extractSingularEmailAddress(String emailAddress){

        String response = null;

        if(emailAddress!=null){

            String[] elements = emailAddress.split(";|,");

            if(elements[0].trim().length()>0){
                response=elements[0].trim();
            }
        }

        return response;

    }


}
