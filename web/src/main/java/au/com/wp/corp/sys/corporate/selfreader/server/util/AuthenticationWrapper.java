package au.com.wp.corp.sys.corporate.selfreader.server.util;

/**
 * Created by IntelliJ IDEA.
 * User: N041844
 * Date: 2/12/11
 * Time: 9:43 AM
 * To change this template use File | Settings | File Templates.
 */
public interface AuthenticationWrapper {

    String getRemoteUser();

    boolean isUserInRole(String roleName);

}