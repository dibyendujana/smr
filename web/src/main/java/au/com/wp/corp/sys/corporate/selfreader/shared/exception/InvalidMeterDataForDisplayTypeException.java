package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import java.io.Serializable;

/**
 * Self Reader Meter Data does not conform to specified Meter Display Type
 *
 * @author Noel Bannon <noel.bannon@westernpower.com.au>
 */
public class InvalidMeterDataForDisplayTypeException extends Exception implements Serializable {

    public InvalidMeterDataForDisplayTypeException() {
    }
}
