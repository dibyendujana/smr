package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import java.io.Serializable;

/**
 * An exception representing when the Webmethods webservices return meter details that do not match those entered by th user in the portal
 *
 * @author Noel Bannon
 */
public class MeterDetailsMismatchException extends Exception implements Serializable {
    @SuppressWarnings("unused")
    private MeterDetailsMismatchException() {
    }

    public MeterDetailsMismatchException(String message) {
        super(message);
    }
}
