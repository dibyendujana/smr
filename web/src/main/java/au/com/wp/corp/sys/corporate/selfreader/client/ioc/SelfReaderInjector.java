package au.com.wp.corp.sys.corporate.selfreader.client.ioc;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderApp;

import com.google.gwt.inject.client.Ginjector;

public interface SelfReaderInjector extends Ginjector {
	SelfReaderApp getApplication();
}
