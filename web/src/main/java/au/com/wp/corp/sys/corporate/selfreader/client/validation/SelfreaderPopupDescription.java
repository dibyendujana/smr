/*
* Extended from Anatol Mayen PopupDescriptionClass from gwt-val project
*/
package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderApp;
import au.com.wp.corp.sys.corporate.selfreader.client.events.HidePopupEvent;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import eu.maydu.gwt.validation.client.description.Description;

/**
 * Extension of Anatol Mayen's PopupDescription class from gwt-vl
 * <p/>
 * Creates an popup that is shown whenever the associated widget
 * gets the focus. The popup will contain arbitrary HTML interpreted
 * text that can easily be i18n.
 * <p/>
 * The popup is shown directly under the associated widget. It will
 * autohide if the user clicks somewhere or moves the focus to another
 * component.
 *
 * @author Noel Bannon
 */
public class SelfreaderPopupDescription implements Description<Focusable>, HidePopupEvent.Handler {

    private LocaleInfo localeInfo = LocaleInfo.getCurrentLocale();
    private final PopupPanel p = new PopupPanel(true);

    private HandlerRegistration focusHandlerRegistration;
    private HandlerRegistration clickHandlerRegistration;
    private HandlerRegistration blurHandlerRegistration;


    private final PopupPanelAction.Position position;

    private final EventBus eventBus;

    private ScrollPanel scrollPanel;


    /**
     */
    public SelfreaderPopupDescription(PopupPanelAction.Position position) {
        this.position = position;
        this.eventBus = SelfReaderApp.getEventBus();
        eventBus.addHandler(HidePopupEvent.TYPE, this);
    }


    /**
     * Adds a popup description to a widget.
     *
     * @param key    This key is used to get the i18n text that should be displayed. This key will be passed as argument to the <code>ValidationMessages.getDescriptionMessage</code> method.
     * @param widget The widget that should show a popup description when it gets the focus.
     */
    public void addDescription(String key, final Focusable widget) {

        p.setStyleName("errorPopup");

        if (localeInfo.isRTL())
            key = "<div align=\"right\" style=\"color:white\">" + key + "</div>";

        HTML html = new HTML(key, false);
        html.setStyleName("errorPopupText");
        p.setWidget(html);

        if (widget instanceof HasAllFocusHandlers) {

            focusHandlerRegistration = ((HasAllFocusHandlers) widget).addFocusHandler(new FocusHandler() {
                @Override
                public void onFocus(FocusEvent focusEvent) {
                    showMessagePopup(p, widget);
                }
            });

            blurHandlerRegistration = ((HasAllFocusHandlers) widget).addBlurHandler(new BlurHandler() {
                @Override
                public void onBlur(BlurEvent blurEvent) {
                    p.hide();
                }
            });

        }

        if (widget instanceof HasClickHandlers) {

            clickHandlerRegistration = ((HasClickHandlers) widget).addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    showMessagePopup(p, widget);
                }
            });
        }
    }

    private void showMessagePopup(final PopupPanel p, final Focusable widget) {
        p.setPopupPositionAndShow(new PositionCallback() {
            @Override
            public void setPosition(int i, int i1) {
                if (position.equals(PopupPanelAction.Position.BELOW)) {
                    p.showRelativeTo((UIObject) widget);
                } else {
                    int left = ((UIObject) widget).getAbsoluteLeft();
                    int top = ((UIObject) widget).getAbsoluteTop();
                    int height = ((UIObject) widget).getOffsetHeight();
                    p.setPopupPosition(left -  10, top - height - 10);
                }
            }
        });
        //hack to get the triangle to show in IE8 - weird
        p.hide();
        p.show();

    }

    public void removeAll() {
        p.clear();

        //override existing handlers so popup panel stays hidden
        if (focusHandlerRegistration != null) {
            focusHandlerRegistration.removeHandler();
        }

        if (clickHandlerRegistration != null) {
            clickHandlerRegistration.removeHandler();
        }

        if (blurHandlerRegistration != null) {
            blurHandlerRegistration.removeHandler();
        }

    }


    @Override
    public void onEvent() {
        if (p != null && p.isVisible()) {
            p.hide();
        }


    }
}