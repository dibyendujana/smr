package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.events.MeterSelectedEvent;
import au.com.wp.corp.sys.corporate.selfreader.client.events.SkipCodesFoundEvent;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.MessageBox;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.InitialResultDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.*;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;

public class LoginPresenter implements LoginView.Presenter {
	private final EventBus eventBus;
	private final SelfReaderServiceAsync serviceAsync;
	private final LoginView display;
    private boolean arrivedByLink;
    private final MessageBox messageBox;

    private static final String SELF_SERVICE_ERROR_MESSAGE = "The Self Read portal is currenlty experiencing problems. Please contact Western Power on 13 10 87.";
    private static final String NMI_METER_INVALID_MESSAGE = "The NMI and Meter Number combination you have entered is either incorrect or not registered for Self Read service. Please contact Western Power on 13 10 87 for further information.";
    private static final String INVALID_METER_DATA_FOR_DISPLAY_MSG = "Your meter is currently not registered. Please contact Western Power on 13 10 87 to submit your readings.";
    private static final String METER_DETAILS_MISMATCH_DISPLAY_MSG = "Dear customer, unfortunately due to a technical error your reading cannot be submitted at this time. Please contact Western Power on 13 10 87 to submit your reading.";

    @Inject
	public LoginPresenter(EventBus eventBus, SelfReaderServiceAsync serviceAsync, LoginView display, MessageBox messageBox) {
		this.eventBus = eventBus;
		this.serviceAsync = serviceAsync;
		this.display = display;
        this.messageBox = messageBox;
        display.setPresenter(this);
	}

    @Override
    public void setNmiAndMeter(String nmi, String meterNumber) {
        getDisplay().setNmiAndMeter(nmi, meterNumber);
        this.arrivedByLink = true;
    }

    @Override
    public void resetCaptcha() {
        display.reloadRecaptcha();
    }

    @Override
	public void go(HasWidgets root) {
		root.add(this.display.asWidget());
	}

    @Override
    public void login(String nmi, String meterNumber) {
        getDisplay().showLoading();

        String browser = Window.Navigator.getUserAgent();

        serviceAsync.getMeterDetails(nmi, meterNumber, display.getRecaptchaChallenge(), display.getRecaptchaResponse(), arrivedByLink, browser, new AsyncCallback<InitialResultDTO>() {
            @Override
            public void onFailure(Throwable caught) {
                // Always reload the recaptcha on failure
                display.reloadRecaptcha();
                display.resetValidator();

                if (caught instanceof FailedCaptchaValidationException) {
                    display.displayRecaptchaError();
                } else if (caught instanceof WebmethodsException) {
                    messageBox.showErrorInformation(SELF_SERVICE_ERROR_MESSAGE);
                } else if (caught instanceof InvalidNMIException) {
                    // handles nmi failing validation, meter number failing validation, nmi/meter combination being
                    // incorrect, meter not being self reader
                    messageBox.showErrorInformation(NMI_METER_INVALID_MESSAGE);

                } else if (caught instanceof InvalidMeterException) {
                    // handles scenarios where the meter is not valid (eg missing mandatory information, or has none or incorrect number of registers)
                    messageBox.showErrorInformation(NMI_METER_INVALID_MESSAGE);
                } else if(caught instanceof InvalidMeterDataForDisplayTypeException){
                    messageBox.showErrorInformation(INVALID_METER_DATA_FOR_DISPLAY_MSG);
                } else if(caught instanceof MeterDetailsMismatchException){
                    messageBox.showErrorInformation(METER_DETAILS_MISMATCH_DISPLAY_MSG);
                } else {
                    // Ok something that we havent thought of occurred assume its fatal
                    messageBox.showErrorInformation(SELF_SERVICE_ERROR_MESSAGE);
                }
                getDisplay().hideLoading();
            }

            @Override
            public void onSuccess(InitialResultDTO result) {
                getDisplay().hideLoading();
                eventBus.fireEvent(new SkipCodesFoundEvent(result.getSkipCodes()));
                eventBus.fireEvent(new MeterSelectedEvent(result.getMeterDTO()));
            }
        });
    }

    @Override
    public void show(boolean show) {
        getDisplay().asWidget().setVisible(show);
        if (show) {
            getDisplay().show();
        } else {
            getDisplay().reset();
        }
    }

    public LoginView getDisplay() {
        return display;
    }
}
