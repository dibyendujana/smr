package au.com.wp.corp.sys.corporate.selfreader.client.address;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.AddressDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

/**
 * Date: 12/10/11
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class AddressLabelWidget extends Composite {
    interface MyUiBinder extends UiBinder<HTMLPanel, AddressLabelWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    Label addressLine1;
    @UiField
    Label addressLine2;
    @UiField
    Label addressLine3;
    @UiField
    Label suburb;
    @UiField
    Label postCode;
    @UiField
    Label state;

    public AddressLabelWidget() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    public void setAddress(AddressDTO address) {
        addressLine1.setText(address.getLine1());
        addressLine2.setText(address.getLine2());
        addressLine3.setText(address.getLine3());
        suburb.setText(address.getSuburb());
        postCode.setText(address.getPostcode());
        state.setText(address.getState());
    }
}
