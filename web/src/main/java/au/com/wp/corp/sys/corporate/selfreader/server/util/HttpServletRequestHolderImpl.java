package au.com.wp.corp.sys.corporate.selfreader.server.util;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Spring Bean providing access to the current {@code HttpServletRequest}. The current
 * {@code HttpServletRequest} is set by {@link SpringExtendedDispatcherServlet}.
 *
 * @author n041982
 */
@Component("httpServletRequestHolder")
public class HttpServletRequestHolderImpl implements HttpServletRequestHolder {

    private final ThreadLocal<HttpServletRequest> requestThreadLocal = new ThreadLocal<HttpServletRequest>();

    @Override
    public void setHttpServletRequest(HttpServletRequest request) {
        requestThreadLocal.set(request);
    }

    @Override
    public HttpServletRequest getHttpServletRequest() {
        return requestThreadLocal.get();
    }

    @Override
    public AuthenticationWrapper getAuthenticationWrapper() {
        return new AuthenticationWrapperImpl(getHttpServletRequest());
    }

}
