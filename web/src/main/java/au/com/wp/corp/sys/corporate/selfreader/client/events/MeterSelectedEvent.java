package au.com.wp.corp.sys.corporate.selfreader.client.events;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class MeterSelectedEvent extends GwtEvent<MeterSelectedEvent.Handler> {
    public static final Type<MeterSelectedEvent.Handler> TYPE = new Type<MeterSelectedEvent.Handler>();

    public interface Handler extends EventHandler{
        void onMeterSelected(MeterDTO meter);
    }

    private MeterDTO meter;

    public MeterSelectedEvent(MeterDTO meter) {
        this.meter = meter;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
       handler.onMeterSelected(meter);
    }
}
