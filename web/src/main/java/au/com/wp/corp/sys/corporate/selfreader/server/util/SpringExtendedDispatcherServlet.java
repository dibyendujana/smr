package au.com.wp.corp.sys.corporate.selfreader.server.util;

import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

/**
 * Created by IntelliJ IDEA.
 * User: N041844
 * Date: 2/12/11
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class SpringExtendedDispatcherServlet extends DispatcherServlet {

    private HttpServletRequestHolder servletRequestHolder;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        servletRequestHolder = BeanFactoryUtils.beanOfType(ctx, HttpServletRequestHolder.class);
    }

    @Override
    protected void doDispatch(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws java.lang.Exception {
        servletRequestHolder.setHttpServletRequest(request);
        super.doDispatch(request, response);
    }


}
