package au.com.wp.corp.sys.corporate.selfreader.shared;

import java.util.ArrayList;

/**
 * Bean to store List of Proxies injected by Spring
 */
public class ProxyConfigBean {

    ArrayList<ProxyBean> proxyBeans = new ArrayList<ProxyBean>();

    public ProxyConfigBean() {
    }

    public ArrayList<ProxyBean> getProxyBeans() {
        return proxyBeans;
    }

    public void setProxyBeans(ArrayList<ProxyBean> proxyBeans) {
        this.proxyBeans = proxyBeans;
    }
}
