package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import java.io.Serializable;

/**
 * An exception representing when the Webmethods webservices either return a soap fault, or a response with a status
 * code indicating an error has occurred internally.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class WebmethodsException extends Exception implements Serializable {
    @SuppressWarnings("unused")
    private WebmethodsException() {
    }

    public WebmethodsException(String message) {
        super(message);
    }
}
