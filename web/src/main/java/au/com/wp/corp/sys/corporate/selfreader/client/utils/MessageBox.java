package au.com.wp.corp.sys.corporate.selfreader.client.utils;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.Command;

/**
 * @author Noel Bannon
 */
public interface MessageBox {

    /**
     * A command that does nothing
     */
    public static final Command NO_ACTION = new Command() {
        @Override
        public void execute() {
        }
    };

    void showYesNoQuestion(String title, String message, final Command yesClickedAction, final Command noClickedAction);

    void showYesNoQuestion(String title, SafeHtml message, Command yesClickedAction, Command noClickedAction);

    void showYesNoCancelQuestion(String title, String message, final Command yesClickedAction, final Command noClickedAction);

    void showYesNoCancelQuestion(String title, SafeHtml message, Command yesClickedAction, Command noClickedAction);

    /**
     * Show a generic error dialog box.
     *
     * @param exceptionMessage Message of the exception.
     */
    void showDefaultErrorDialog(String exceptionMessage);

    /**
     * Show an error dialog box.
     *
     * @param message  Message to display (as SafeHtml)
     * @param okAction Action to perform when "Ok" is clicked.
     */
    void showErrorInformation(SafeHtml message, Command okAction);

    /**
     * Show an error dialog box.
     *
     * @param message  Message to display
     */
    void showErrorInformation(String message);

    /**
     * Show an warning dialog box.
     *
     * @param message  Message to display (as SafeHtml)
     * @param okAction Action to perform when "Ok" is clicked.
     */
    void showWarningInformation(SafeHtml message, Command okAction);

    /**
     * Show an warning dialog box.
     *
     * @param message  Message to display
     */
    void showWarningInformation(String message);

}
