package au.com.wp.corp.sys.corporate.selfreader.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import java.awt.event.WindowListener;

public class RootViewImpl extends Composite implements RootView, ScrollHandler {
	private static RootViewImplUiBinder uiBinder = GWT.create(RootViewImplUiBinder.class);

    @Override
    public void onScroll(ScrollEvent event) {
          if(presenter!=null){
              presenter.onScrollEvent();
          }
    }

    interface RootViewImplUiBinder extends UiBinder<Widget, RootViewImpl> {}

	private Presenter presenter;
    @UiField
    HTMLPanel contentPanel;
    @UiField
    Anchor logoutButton;
    @UiField
    ScrollPanel rootScrollPanel;
    private PopupPanel unsubmittableMessagePanel;

    public RootViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));

        unsubmittableMessagePanel = new PopupPanel(false, true);
        unsubmittableMessagePanel.setGlassEnabled(true);
        unsubmittableMessagePanel.setTitle("Meter is outside it's reading window");
        VerticalPanel messageContent = new VerticalPanel();
        messageContent.add(new Label("This meter is outside its reading window. You can submit a reading, however it may not be used for billing purposes."));
        Button closeButton = new Button("Close", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                unsubmittableMessagePanel.hide();
            }
        });
        messageContent.add(closeButton);
        unsubmittableMessagePanel.setWidget(messageContent);

       rootScrollPanel.addScrollHandler(this);

        Window.addResizeHandler(new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event) {
                presenter.onScrollEvent();
            }
        });
	}

    @Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}


    @Override
    public HasWidgets getContentPanel() {
        return contentPanel;
    }

    @UiHandler("logoutButton")
    public void logoutButton(ClickEvent e) {
        presenter.logout();
    }

    @Override
    public void showLogoutButton(boolean show) {
        logoutButton.setVisible(show);
    }

    @Override
    public void showUnsubmittableMeterMessage() {
        // display a warning message before allowing the user to continue that the meter is not submittable (eg outside it's viewing window), and submissions may be ignored.
        unsubmittableMessagePanel.center();
    }
}
