package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 5/06/12
 * Time: 3:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class NotNullRegisterIntegerBoxValidator extends Validator<NotNullRegisterIntegerBoxValidator> {

    private final IntegerBox integerBox;
    private final CheckBox checkBox;
    private final ListBox listBox;


    public NotNullRegisterIntegerBoxValidator(IntegerBox integerBox, CheckBox checkBox, ListBox listBox) {
        this.integerBox = integerBox;
        this.checkBox = checkBox;
        this.listBox = listBox;
    }

    /**
     * Checks that the 2 email fields match and
     *
     * @param messages
     * @return
     */
    @Override
    public <V extends ValidationMessages> ValidationResult validate(V messages) {

        SelfreaderValidationMessages msgs = (SelfreaderValidationMessages) messages.getStandardMessages();

        boolean valid = true;
        boolean isNull = false;
        boolean isNegative = false;
        if (!checkBox.getValue()) {
            if (integerBox.getValue() == null) {
                isNull = true;
                valid = false;
            } else if (integerBox.getValue() < 0) {
                isNegative = true;
                valid = false;
            } else {
                valid = true;
            }
        }

        if (!valid) {
            if (isNull) {
                return new ValidationResult(getErrorMessage(messages, msgs.notEmptyIntegerBoxRegister()));
            }

            if (isNegative) {
                return new ValidationResult(getErrorMessage(messages, msgs.notNegativeIntegerBoxRegister()));
            }
        }


        return null;
    }

    /**
     * Method that invokes the actions that are to be performed
     * if the validate method returns a non null value (indicating)
     * validation has failed)
     *
     * @param validationResult
     */
    @Override
    public void invokeActions(ValidationResult validationResult) {
        for (ValidationAction va : this.getFailureActions())
            va.invoke(validationResult, integerBox);
    }
}
