package au.com.wp.corp.sys.corporate.selfreader.server.util;

import org.apache.log4j.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Wrap up some of the calendar transformation guff in a utility class
 * */
public class CalendarHelper {
    private static final Logger log = Logger.getLogger(CalendarHelper.class);

    /**
     * Return an XMLGregorianCalendar representing the current time
     * @return the current time in an XMLGregorianCalendar object
     */
    public static XMLGregorianCalendar now() {
        XMLGregorianCalendar xmlGregorianCalendar = null;
        GregorianCalendar calendar = new GregorianCalendar();
        try {
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException e) {
            log.error("Unable to create an XMLCalendar object for now",e);
        }

        return xmlGregorianCalendar;
    }

    /**
     * Return an xmlGregorianCalander for the date
     * @param theDate the date to return an XMLGregorian calendar for
     * @return and XMLGregorianCalendar representation of the date
     */
    public static XMLGregorianCalendar convertDateToXMLGregorianCalendar(Date theDate) {
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(theDate);
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            log.warn("Unable to map actual read date", e);
        }
        return xmlGregorianCalendar;
    }

    /**
     * Converts a XMLGregorianCalendar to a Date
     * @param xmlGregorianCalendar to convert to date
     * @return a date or null if input is null
     */
    public static Date convertXMLGregorianCalendarToDate(XMLGregorianCalendar xmlGregorianCalendar) {
        Date date = null;
        if (xmlGregorianCalendar != null) {
            GregorianCalendar gregorianCalendar = xmlGregorianCalendar.toGregorianCalendar();
            date = gregorianCalendar.getTime();
        }
        return date;
    }
}
