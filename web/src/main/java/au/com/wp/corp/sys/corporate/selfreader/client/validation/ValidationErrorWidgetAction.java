package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.UIObject;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;

import java.util.HashMap;
import java.util.Map;

/**
 * Custom Action class to be used to show ValidationErrorWidget
 */
public class ValidationErrorWidgetAction extends ValidationAction<UIObject> {

    private final ValidationErrorWidget errorWidget;
    private final boolean multiField;
    private final String genericErrorMsg;

    private static final String SUPPLIED_READING_ERR_PREFIX = "Supplied reading is";

    /**
     * Constructor
     */

    public ValidationErrorWidgetAction(ValidationErrorWidget errorWidget) {
        this.errorWidget = errorWidget;
        this.multiField = false;
        this.genericErrorMsg = null;
    }

    public ValidationErrorWidgetAction(ValidationErrorWidget errorWidget, boolean multiField, String genericErrorMsg) {
        this.errorWidget = errorWidget;
        this.multiField = multiField;
        this.genericErrorMsg = genericErrorMsg;
    }


    @Override
    public void invoke(ValidationResult validationResult, UIObject uiObject) {
        ValidationResult.ValidationError error = validationResult.getErrors().get(0);
        //check if this if a server side error for multiple registers and replace with a more generic error msg 
        String errorMsg = error.error;
        if (multiField) {
            errorWidget.setErrorMsg(genericErrorMsg);
        } else {
            errorWidget.setErrorMsg(error.error);
        }

        errorWidget.setVisible(true);
    }

    @Override
    public void reset(UIObject obj) {
        errorWidget.setVisible(false);
        errorWidget.setErrorMsg(null);
    }

    @Override
    public void reset() {
        errorWidget.setVisible(false);
        errorWidget.setErrorMsg(null);
    }

}
