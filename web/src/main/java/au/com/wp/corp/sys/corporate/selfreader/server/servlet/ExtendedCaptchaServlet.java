package au.com.wp.corp.sys.corporate.selfreader.server.servlet;

import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.servlet.SimpleCaptchaServlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ExtendedCaptchaServlet extends SimpleCaptchaServlet {
    private static final long serialVersionUID = 6560171562324177699L;
    private int width = 0;
    private int height = 0;

    public void init(ServletConfig servletConfig) throws ServletException {
        width = Integer.parseInt(servletConfig.getInitParameter("width"));
        height = Integer.parseInt(servletConfig.getInitParameter("height"));
        super.init();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        Captcha captcha = new Captcha.Builder(width, height)
                .addText()
                .addBackground(new GradiatedBackgroundProducer())
                .gimp()
                .addNoise()
                .addBorder()
                .build();
        session.setAttribute(Captcha.NAME, captcha);
        CaptchaServletUtil.writeImage(resp, captcha.getImage());

    }
}
