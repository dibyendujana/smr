package au.com.wp.corp.sys.corporate.selfreader.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class ValidationErrorWidget extends Composite{
	private static ValidationErrorWidgetUiBinder uiBinder = GWT.create(ValidationErrorWidgetUiBinder.class);

    @UiField
    Label errorMsg;

    interface ValidationErrorWidgetUiBinder extends UiBinder<Widget, ValidationErrorWidget> {}

    public ValidationErrorWidget() {
        initWidget(uiBinder.createAndBindUi(this));
	}

    public Label getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        getErrorMsg().setText(errorMsg);
    }
}
