package au.com.wp.corp.sys.corporate.selfreader.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event to broadcast if you wish to hide popups
 * @author Noel
 */
public class HidePopupEvent extends GwtEvent<HidePopupEvent.Handler> {
    public static final Type<HidePopupEvent.Handler> TYPE = new Type<HidePopupEvent.Handler>();


    public interface Handler extends EventHandler {
        void onEvent();
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onEvent();
    }


}
