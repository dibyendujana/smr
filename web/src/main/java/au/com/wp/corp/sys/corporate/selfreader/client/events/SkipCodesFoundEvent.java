package au.com.wp.corp.sys.corporate.selfreader.client.events;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import java.util.List;

public class SkipCodesFoundEvent extends GwtEvent<SkipCodesFoundEvent.Handler> {
    public static final Type<SkipCodesFoundEvent.Handler> TYPE = new Type<SkipCodesFoundEvent.Handler>();

    public interface Handler extends EventHandler{
        void onSkipCodesFound(List<SkipCodeDTO> skipCodes);
    }

    private List<SkipCodeDTO> skipCodes;

    public SkipCodesFoundEvent(List<SkipCodeDTO> skipCodes) {
        this.skipCodes = skipCodes;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
       handler.onSkipCodesFound(skipCodes);
    }
}
