package au.com.wp.corp.sys.corporate.selfreader.server.util.logging;

import com.google.gwt.logging.server.StackTraceDeobfuscator;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Extended GWT's StackTraceDeobfuscator (which can only handle relative paths to symbolMaps) to read the
 * symbolMaps from the WAR file as a resource (via ServletContext).
 *
 * @author Thomas Buckel (n041982)
 */
public class CustomStackTraceDeobfuscator extends StackTraceDeobfuscator {
    
    private final String symbolMapsDirectory;
    private final ServletContext servletContext;

    public CustomStackTraceDeobfuscator(ServletContext servletContext, String symbolMapsDirectory) {
        super(symbolMapsDirectory);
        this.servletContext = servletContext;
        this.symbolMapsDirectory = symbolMapsDirectory;
    }

    @Override
    protected InputStream getSymbolMapInputStream(String permutationStrongName) throws IOException {
        String filename = symbolMapsDirectory + File.separatorChar + permutationStrongName + ".symbolMap";
        return servletContext.getResourceAsStream(filename);
    }
}
