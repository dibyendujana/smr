package au.com.wp.corp.sys.corporate.selfreader.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.safehtml.shared.SimpleHtmlSanitizer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.inject.Singleton;

/**
 * @author Noel Bannon
 */
@Singleton
public class MessageBoxImpl implements MessageBox {

    interface MyUiBinder extends UiBinder<DialogBox, MessageBoxImpl> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    interface Styles extends CssResource {
        String buttonStyle();

        String buttonBar();

        String messageStyle();
    }

    @UiField
    Styles style;
    @UiField
    HTML messageWidget;
    @UiField
    FlowPanel buttonBar;

    private final DialogBox dialogBox;

    public MessageBoxImpl() {
        this.dialogBox = uiBinder.createAndBindUi(this);

    }

    private SafeHtml sanitizeMessage(String message) {
        // Sanitize HTML
        String sanitizedMessage = SimpleHtmlSanitizer.sanitizeHtml(message).asString();
        // Replace newlines with <br/>
        sanitizedMessage = sanitizedMessage.replace("\n", "<br/>");
        // Create SafeHtml from trusted String
        return SafeHtmlUtils.fromTrustedString(sanitizedMessage);
    }

    private void addButton(String text, final Command command) {
        Button button = new Button(text);
        button.addStyleName(style.buttonStyle());
        buttonBar.add(button);
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                dialogBox.hide();
                command.execute();
            }
        });
    }

    @Override
    public void showYesNoQuestion(String title, String message, final Command yesClickedAction, final Command noClickedAction) {
        SafeHtml sanitizedMessage = sanitizeMessage(message);
        this.showYesNoQuestion(title, sanitizedMessage, yesClickedAction, noClickedAction);
    }

    @Override
    public void showYesNoQuestion(String title, SafeHtml message, final Command yesClickedAction, final Command noClickedAction) {
        dialogBox.getCaption().setText(title);
        this.messageWidget.setHTML(message);

        buttonBar.clear();
        addButton("Yes", yesClickedAction);
        addButton("No", noClickedAction);
        dialogBox.show();
        dialogBox.center();
    }

    @Override
    public void showYesNoCancelQuestion(String title, String message, Command yesClickedAction, Command noClickedAction) {
        showYesNoCancelQuestion(title, sanitizeMessage(message), yesClickedAction, noClickedAction);
    }

    @Override
    public void showYesNoCancelQuestion(String title, SafeHtml message, Command yesClickedAction, Command noClickedAction) {
        showYesNoQuestion(title, message, yesClickedAction, noClickedAction);
        addButton("Cancel", new Command() {
            @Override
            public void execute() {
                dialogBox.hide();
            }
        });
    }

    @Override
    public void showDefaultErrorDialog(String errorMessage) {
        //not showing exceptionMessage as it may confuse users
        SafeHtml message = new SafeHtmlBuilder()
                .appendEscapedLines("The Self Read portal is currenlty experiencing problems. Please contact Western Power on 13 10 87.")
                .toSafeHtml();
        showErrorInformation(message, NO_ACTION);
    }


    @Override
    public void showErrorInformation(SafeHtml message, Command okAction) {

        dialogBox.getCaption().setText("Error");
        this.messageWidget.setHTML(message);

        buttonBar.clear();
        addButton("Ok", okAction);
        dialogBox.show();
        dialogBox.center();
    }

    @Override
    public void showErrorInformation(String message) {
        showErrorInformation(SafeHtmlUtils.fromString(message), NO_ACTION);
    }

    @Override
    public void showWarningInformation(SafeHtml message, Command okAction) {
        dialogBox.getCaption().setText("Warning");
        this.messageWidget.setHTML(message);
        buttonBar.clear();
        addButton("Ok", okAction);
        dialogBox.show();
        dialogBox.center();
    }

    @Override
    public void showWarningInformation(String message) {
        showWarningInformation(SafeHtmlUtils.fromString(message), NO_ACTION);
    }

}
