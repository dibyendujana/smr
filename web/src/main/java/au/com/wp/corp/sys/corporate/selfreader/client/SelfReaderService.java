package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.InitialResultDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.*;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import net.tanesha.recaptcha.ReCaptchaException;

/**
 * @See{SelfReaderServiceAsync}
 */
@RemoteServiceRelativePath("rpc/selfreaderService")
public interface SelfReaderService extends RemoteService {
	InitialResultDTO getMeterDetails(String nmi, String meterNumber, String challenge, String response, boolean arrivedByLink, String browser) throws FailedCaptchaValidationException, InvalidMeterException, InvalidNMIException, WebmethodsException, InvalidMeterDataForDisplayTypeException, MeterDetailsMismatchException;
    MeterDTO submitMeterDetails(MeterDTO meter, boolean overrideFlag, int attemptNumber, String browser, String uploadImageFileName) throws SubmittedWithWarningException, RegisterSubmissionFailedException, WebmethodsException, InvalidImageFileException;
}
