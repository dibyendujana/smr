package au.com.wp.corp.sys.corporate.selfreader.server.services;

import au.com.westernpower.wp.mr.wsp.meterreadingsservices.GetReadingSkipCodesOutput;
import au.com.westernpower.wp.mr.wsp.meterreadingsservices.MeterReadingsServices;
import au.com.westernpower.wp.mr.wsp.meterreadingsservices.MeterReadingsServicesPortType;
import au.com.westernpower.wp.mr.wsp.meterreadingsservices.ObjectFactory;
import au.com.westernpower.wp.mr.wsp.meterreadingsservices.SkipCodeType;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.BindingProvider;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class SkipCodeServiceImpl implements SkipCodeService {
    private List<SkipCodeDTO> skipCodes;
    private Date updated;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    MeterReadingsServicesPortType meterReadingsServices;


    public SkipCodeServiceImpl() {
        updated = null;
        skipCodes = new ArrayList<SkipCodeDTO>();
    }

    @Override
    public List<SkipCodeDTO> getSkipCodes() {
        if ((updated == null) || (moreThanOneDayOld(updated))) {
            updateSkipCodes();
        }
        return skipCodes;
    }

    private synchronized void updateSkipCodes() {
        // Having this check in the calling methods means that this synchronized method is only called when the
        // skipcodes need updating (thus a performance benefit). It is repeated here so that if multiple requests
        // arrive and they are waiting on this synchronized method, we don't refetch the data once the block is released.
        if ((updated == null) || (moreThanOneDayOld(updated))) { 
            ObjectFactory objectFactory = new ObjectFactory();
            GetReadingSkipCodesOutput readingSkipCodes = meterReadingsServices.getReadingSkipCodes(objectFactory.createGetReadingSkipCodesInput());
            List<SkipCodeType> wsSkipCodes = readingSkipCodes.getSkipCodes();
            skipCodes.clear();
            for (SkipCodeType wsSkipCode : wsSkipCodes) {
                skipCodes.add(new SkipCodeDTO(wsSkipCode.getCode(), wsSkipCode.getDescription()));
            }
            updated = new Date();
        }
    }

    private boolean moreThanOneDayOld(Date oldDate) {
        Calendar oldCal = Calendar.getInstance();
        oldCal.setTime(oldDate);
        oldCal.add(Calendar.DAY_OF_MONTH, 1);
        Calendar cal = Calendar.getInstance();
        return cal.after(oldCal);
    }

}
