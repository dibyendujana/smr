package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.Arrays;
import java.util.List;

public class LE1MeterWidget extends AbstractMeterWidget {

    interface MyUiBinder extends UiBinder<HTMLPanel, LE1MeterWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register7Value;
    @UiField
    CheckBox register7SkipCodeBox;
    @UiField
    ListBox register7SkipCodeList;
    @UiField
    Label register7PreviousValue;
    @UiField
    IntegerBox register47Value;
    @UiField
    CheckBox register47SkipCodeBox;
    @UiField
    ListBox register47SkipCodeList;
    @UiField
    Label register47PreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;
    @UiField
    Label register10PreviousValue;
    @UiField
    IntegerBox register10Value;
    @UiField
    CheckBox register10SkipCodeBox;
    @UiField
    ListBox register10SkipCodeList;
    @UiField
    Label register50PreviousValue;
    @UiField
    IntegerBox register50Value;
    @UiField
    CheckBox register50SkipCodeBox;
    @UiField
    ListBox register50SkipCodeList;
    @UiField
    Label register20PreviousValue;
    @UiField
    IntegerBox register20Value;
    @UiField
    CheckBox register20SkipCodeBox;
    @UiField
    ListBox register20SkipCodeList;
    @UiField
    Label register60PreviousValue;
    @UiField
    IntegerBox register60Value;
    @UiField
    CheckBox register60SkipCodeBox;
    @UiField
    ListBox register60SkipCodeList;

    public LE1MeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter,validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_7:
                    assignValuesFromRegister(register, register7SkipCodeBox, register7PreviousValue, register7Value, register7SkipCodeList,serverError);
                    break;
                case REGISTER_47:
                    assignValuesFromRegister(register, register47SkipCodeBox, register47PreviousValue, register47Value, register47SkipCodeList,serverError);
                    break;
                case REGISTER_10:
                    assignValuesFromRegister(register, register10SkipCodeBox, register10PreviousValue, register10Value, register10SkipCodeList, serverError);
                    break;
                case REGISTER_50:
                    assignValuesFromRegister(register, register50SkipCodeBox, register50PreviousValue, register50Value, register50SkipCodeList, serverError);
                    break;
                case REGISTER_20:
                    assignValuesFromRegister(register, register20SkipCodeBox, register20PreviousValue, register20Value, register20SkipCodeList, serverError);
                    break;
                case REGISTER_60:
                    assignValuesFromRegister(register, register60SkipCodeBox, register60PreviousValue, register60Value, register60SkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);
    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(register7SkipCodeList);
        addSkipCodesToListBox(register47SkipCodeList);
        addSkipCodesToListBox(register10SkipCodeList);
        addSkipCodesToListBox(register50SkipCodeList);
        addSkipCodesToListBox(register20SkipCodeList);
        addSkipCodesToListBox(register60SkipCodeList);
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register7Value, register7SkipCodeBox, register7SkipCodeList);
        valid = valid & validateRegister(register47Value, register47SkipCodeBox, register47SkipCodeList);
        valid = valid & validateRegister(register10Value, register10SkipCodeBox, register10SkipCodeList);
        valid = valid & validateRegister(register50Value, register50SkipCodeBox, register50SkipCodeList);
        valid = valid & validateRegister(register20Value, register20SkipCodeBox, register20SkipCodeList);
        valid = valid & validateRegister(register60Value, register60SkipCodeBox, register60SkipCodeList);
        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_7.getCode(), register7SkipCodeBox, register7Value, register7SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_47.getCode(), register47SkipCodeBox, register47Value, register47SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_10.getCode(), register10SkipCodeBox, register10Value, register10SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_20.getCode(), register20SkipCodeBox, register20Value, register20SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_50.getCode(), register50SkipCodeBox, register50Value, register50SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_60.getCode(), register60SkipCodeBox, register60Value, register60SkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }


    @Override
    public void disable(boolean disable) {
        register7Value.setEnabled(!disable);
        register7SkipCodeList.setEnabled(!disable);
        register7SkipCodeBox.setEnabled(!disable);
        register47Value.setEnabled(!disable);
        register47SkipCodeList.setEnabled(!disable);
        register47SkipCodeBox.setEnabled(!disable);
        register10Value.setEnabled(!disable);
        register10SkipCodeList.setEnabled(!disable);
        register10SkipCodeBox.setEnabled(!disable);
        register50Value.setEnabled(!disable);
        register50SkipCodeList.setEnabled(!disable);
        register50SkipCodeBox.setEnabled(!disable);
        register20Value.setEnabled(!disable);
        register20SkipCodeList.setEnabled(!disable);
        register20SkipCodeBox.setEnabled(!disable);
        register60Value.setEnabled(!disable);
        register60SkipCodeList.setEnabled(!disable);
        register60SkipCodeBox.setEnabled(!disable);
    }

    @UiHandler("register7SkipCodeBox")
    public void onReg7UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register7SkipCodeBox.getValue();
        register7Value.setEnabled(!unreadable);
        register7SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register7SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register47SkipCodeBox")
    public void onReg47UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register47SkipCodeBox.getValue();
        register47Value.setEnabled(!unreadable);
        register47SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register47SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register10SkipCodeBox")
    public void onReg10UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register10SkipCodeBox.getValue();
        register10Value.setEnabled(!unreadable);
        register10SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register10SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register50SkipCodeBox")
    public void onReg50UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register50SkipCodeBox.getValue();
        register50Value.setEnabled(!unreadable);
        register50SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register50SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register20SkipCodeBox")
    public void onReg20UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register20SkipCodeBox.getValue();
        register20Value.setEnabled(!unreadable);
        register20SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register20SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register60SkipCodeBox")
    public void onReg60UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register60SkipCodeBox.getValue();
        register60Value.setEnabled(!unreadable);
        register60SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register60SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }
    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        // Note: Register IDs are sometimes stored with leading zeroes in MBS, which is why
        // we keep multiple register IDs for each logical register
        REGISTER_7("007,07,7"),
        REGISTER_47("047,47"),
        REGISTER_10("010,10"),
        REGISTER_50("050,50"),
        REGISTER_20("020,20"),
        REGISTER_60("060,60"),
        NOVALUE("");

        private String value;

        Register(String value) {
            this.value=value;
        }

         /**
          * If we cant find the specific register we return novalue
          * @param value the value to convert to enum
          * @return the enum associated with code
          */
         public static Register fromString(String value) {
             if (value != null) {
                 for (Register b : Register.values()) {
                     String[] values = b.value.split(",");
                     List<String> valueSet = Arrays.asList(values);
                     if(valueSet.contains(value)){
                         return b;
                     }
                 }
             }
             return NOVALUE;
         }

        public List<String> getCode() {
                    return Arrays.asList(value.split(","));
                }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_7.getCode().get(0), register7Value, register7SkipCodeBox, register7SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_47.getCode().get(0), register47Value, register47SkipCodeBox, register47SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_10.getCode().get(0), register10Value, register10SkipCodeBox, register10SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_20.getCode().get(0), register20Value, register20SkipCodeBox, register20SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_50.getCode().get(0), register50Value, register50SkipCodeBox, register50SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_60.getCode().get(0), register60Value, register60SkipCodeBox, register60SkipCodeList,registerValidationError);
        return valid;
    }

}
