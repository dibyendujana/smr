package au.com.wp.corp.sys.corporate.selfreader.server.servlet;

import gwtupload.server.UploadAction;
import gwtupload.server.exceptions.UploadActionException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 19/03/12
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class UploadServlet extends UploadAction {
    private static final String filePath = "/web/networks/internet_data/selfreader/uploaded/imagefiles";

    private static final long serialVersionUID = 1L;

    Hashtable<String, String> receivedContentTypes = new Hashtable<String, String>();
    /**
     * Maintain a list with received files and their content types.
     */
    Hashtable<String, File> receivedFiles = new Hashtable<String, File>();

    /**
     * Override executeAction to save the received files in a custom place
     * and delete this items from session.
     */
    @Override
    public String executeAction(HttpServletRequest request, List<FileItem> sessionFiles) throws UploadActionException {
        String response = "";
        int cont = 0;
        for (FileItem item : sessionFiles) {
            if (false == item.isFormField()) {
                cont++;
                try {


                    String fileName = item.getName();
                    String extension = "";
                    // get only the file name not whole path
                    if (fileName != null) {
                        extension = FilenameUtils.getExtension(fileName);
                    }


                    File dir = new File(filePath);
                    File uploadedFile = File.createTempFile(getFilePrefix(), "." + extension, dir);

                    item.write(uploadedFile);
                    response = String.format("{ \"fileId\": \"%s\" }", uploadedFile.getName());

                    if(uploadedFile.length()==0){
                        throw new Exception("You have attempted to upload an empty image file");
                    }


                    /// Save a list with the received files
                    receivedFiles.put(item.getFieldName(), uploadedFile);
                    receivedContentTypes.put(item.getFieldName(), item.getContentType());

                } catch (Exception e) {
                    removeSessionFileItems(request);
                    throw new UploadActionException(e);
                }
            }
        }

        /// Remove files from session because we have a copy of them
        removeSessionFileItems(request);

        /// Send your customized message to the client.
        return response;
    }

       /**
     * Return a prefix for the file as a time down to milliseconds
     * @return a prefix to the file as a time in milliseconds
     */
    protected String getFilePrefix() {
        Calendar now = Calendar.getInstance();
        DateFormat format = new SimpleDateFormat("yyMMddHHmmssSSS");
        return format.format(now.getTime());
    }

     /**
   * Remove a file when the user sends a delete request.
   */
  @Override
  public void removeItem(HttpServletRequest request, String fieldName)  throws UploadActionException {
    File file = receivedFiles.get(fieldName);
    receivedFiles.remove(fieldName);
    receivedContentTypes.remove(fieldName);
    if (file != null) {
      file.delete();
    }
  }


}
