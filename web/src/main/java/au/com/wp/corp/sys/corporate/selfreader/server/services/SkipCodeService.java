package au.com.wp.corp.sys.corporate.selfreader.server.services;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;

import java.util.List;

/**
 * A service that interrogates a webmethods webservice for a list of skipcodes. These are cached on a daily basis for performance reasons.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public interface SkipCodeService {

    List<SkipCodeDTO> getSkipCodes();
}
