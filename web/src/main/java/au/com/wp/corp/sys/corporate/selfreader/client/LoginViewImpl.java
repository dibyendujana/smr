package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.validation.NMINumberValidator;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.PopupPanelAction;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.SelfreaderValidationMessagesImpl;
import com.claudiushauptmann.gwt.recaptcha.client.RecaptchaWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.DefaultValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.actions.StyleAction;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;
import eu.maydu.gwt.validation.client.validators.standard.NotEmptyValidator;


public class LoginViewImpl extends Composite implements LoginView {
    private static LoginViewImplUiBinder uiBinder = GWT.create(LoginViewImplUiBinder.class);

    interface LoginViewImplUiBinder extends UiBinder<Widget, LoginViewImpl> {
    }

    private Presenter presenter;
    @UiField
    TextBox nmiField;
    @UiField
    TextBox meterNumberField;
    @UiField
    Button loginButton;
    private PopupPanel loadingPanel;
    private ErrorPanel errorPanel;

    @UiField
    FocusPanel recaptchaPanel;
    @UiField
    HTMLPanel verificationPanel;
    @UiField
    Label recaptchaErrorLabel;
    @UiField
    TextBox checksumField;

    private ValidationProcessor validator;
    private ValidationMessages messages;

    final static RecaptchaWidget rw = new RecaptchaWidget("6LdPGMsSAAAAAFxua2aTyvDVwqqfGCTpmKvgJ8sJ");

    public LoginViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));

        createLoadingPanel();
        errorPanel = new ErrorPanel();

        setupValidator();
        recaptchaPanel.add(rw);
    }


    private void createLoadingPanel() {
        loadingPanel = new PopupPanel(false, true);
        loadingPanel.setGlassEnabled(true);
        loadingPanel.setTitle("Loading");
        VerticalPanel loadingContent = new VerticalPanel();
        loadingContent.add(new Label("Loading"));
        loadingContent.add(new Image("images/loading.gif"));
        loadingPanel.setWidget(loadingContent);
    }

    @Override
    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void show() {
        nmiField.setFocus(true);
    }

    @Override
    public void setNmiAndMeter(String nmi, String meterNumber) {
        nmiField.setText(nmi.substring(0, 10));
        checksumField.setText(nmi.substring(10, 11));
        meterNumberField.setText(meterNumber);
    }

    @Override
    public void reset() {
        nmiField.setText(null);
        meterNumberField.setText(null);
        checksumField.setText(null);
    }

    @Override
    public void showLoading() {
        loadingPanel.center();
    }

    @Override
    public void hideLoading() {
        loadingPanel.hide();
    }

    @UiHandler("loginButton")
    public void login(ClickEvent e) {
        resetValidator();
        String nmi = nmiField.getText() + checksumField.getText();

        //captialise the meter number field
        meterNumberField.setText(meterNumberField.getText().toUpperCase());
        String meterNumber = meterNumberField.getText();

        if (doValidation()) {
            presenter.login(nmi, meterNumber);
        }
    }

    /**
     * Sets up the login validation
     */
    private void setupValidator() {
        messages = GWT.create(SelfreaderValidationMessagesImpl.class);
        validator = new DefaultValidationProcessor(messages);
    }

    private boolean doValidation() {
        nmiValidator();
        checksumValidator();
        meterValidator();
        return validator.validate();
    }

    public void resetValidator() {
        validator.reset();
        validator.removeValidatorsAndGlobalActions();
    }

    private void nmiValidator() {
        validator.addValidators("nmiField", new NMINumberValidator(nmiField)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new PopupPanelAction(PopupPanelAction.Position.SIDE))
        );

    }

    private void meterValidator() {
        validator.addValidators("meterNumberField", new NotEmptyValidator(meterNumberField, "meterNumber")
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new PopupPanelAction(PopupPanelAction.Position.SIDE))
        );
    }

    private void checksumValidator() {
        validator.addValidators("checkSumField", new NotEmptyValidator(checksumField, "checkSumField")
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new PopupPanelAction(PopupPanelAction.Position.SIDE))
        );
    }

    @Override
    public void displayRecaptchaError() {
        recaptchaErrorLabel.setVisible(true);
        recaptchaErrorLabel.setText("The words you typed don't seem to match what we expect.  Please try again.");
    }

    /**
     * Returns the RecaptchaWidget object that is currently shown on the screen
     *
     * @return the current RecaptchaWidget object
     */
    private RecaptchaWidget getRw() {
        return rw;
    }

    @Override
    public String getRecaptchaChallenge() {
        return getRw().getChallenge();
    }

    @Override
    public String getRecaptchaResponse() {
        return getRw().getResponse();
    }

    @Override
    public void reloadRecaptcha() {
        getRw().reload();
    }
}
