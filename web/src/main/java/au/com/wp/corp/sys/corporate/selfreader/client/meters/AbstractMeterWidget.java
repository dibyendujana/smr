package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.HelpPanel;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.*;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterError;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.actions.StyleAction;

import java.util.List;

/**
 * Abstract base class for Meters
 *
 * @author Isaac Stephens
 */
public abstract class AbstractMeterWidget extends Composite implements MeterWidget {

    protected List<SkipCodeDTO> skipCodes;
    protected MeterDTO meter;
    protected SelfReaderView.Presenter presenter;
    protected ValidationProcessor validator;
    //defaults to false, but should be set in constructor of sub-classes
    protected boolean multiRegister = false;

    private static final String SUPPLIED_READING_GENERIC_ERROR = "Supplied reading is not what we expected. Click into the box/es highlighted in red to see why.";

    protected void initMeter(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor val) {
        this.meter = meter;
        this.skipCodes = skipCodes;
        this.presenter = presenter;
        this.validator = val;
        processSkipCodes();

        validator.reset();
        validator.removeValidatorsAndGlobalActions();
        setupMeter();
        validator.validate();
    }

    protected abstract void processSkipCodes();

    protected abstract void setupMeter();

    protected void addSkipCodesToListBox(ListBox box) {
        box.addItem("Please select...", "");
        for (SkipCodeDTO skipCode : skipCodes) {
            box.addItem(skipCode.getText(), skipCode.getCode());
        }
    }

    protected boolean validateRegister(IntegerBox value, CheckBox checkBox, ListBox listBox) {
        boolean valid = true;
        if (true == checkBox.getValue()) {
            value.removeStyleName("invalidField");
            if (listBox.getSelectedIndex() == 0) {
                valid = false;
                listBox.addStyleName("invalidField");
                listBox.setTitle("If a register is unreadable, a reason must be specified");
            } else {
                listBox.setTitle("");
                listBox.removeStyleName("invalidField");
            }
        } else {
            listBox.setTitle("");
            listBox.removeStyleName("invalidField");
            if (value.getValue() == null) {
                valid = false;
                value.addStyleName("invalidField");
                value.setTitle("A numeric reading must be specified");
            } else {
                value.removeStyleName("invalidField");
            }
        }
        return valid;
    }

    protected boolean validateRegister(ValidationProcessor validate, String validationName, IntegerBox value, CheckBox checkBox, ListBox listBox, ValidationErrorWidget errorWidget) {
        //validate the value entered if readable, otherwise validate the reason why the register is not readable.
        if (value.isEnabled() && ! checkBox.getValue())
            validate.addValidators(validationName + "0", new NotNullRegisterIntegerBoxValidator(value, checkBox, listBox)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget)));
        else
            validate.addValidators(validationName + "1", new NotNullRegisterListBoxValidator(value, checkBox, listBox)
                    .addActionForFailure(new StyleAction("validationFailedBorder"))
                    .addActionForFailure(new ValidationErrorWidgetAction(errorWidget)));

        return validate.validate();
    }

    protected void assignValuesToRegister(RegisterDTO registerDTO, CheckBox skipCodeBox, IntegerBox value, ListBox skipCodeList) {
        if (!skipCodeBox.getValue()) {
            registerDTO.setValue(value.getValue());
            registerDTO.setSkipCode(null);
        } else {
            int selectedIndex = skipCodeList.getSelectedIndex();
            SkipCodeDTO skipCode = new SkipCodeDTO(skipCodeList.getValue(selectedIndex), skipCodeList.getItemText(selectedIndex));
            registerDTO.setSkipCode(skipCode);
        }
    }

    protected void assignValuesToRegisterAndCreate(String registerId, CheckBox skipCodeBox, IntegerBox value, ListBox skipCodeList) {
        RegisterDTO registerDTO = meter.getRegister(registerId);
        if (registerDTO == null) {
            registerDTO = new RegisterDTO(registerId, null, null);
        }
        assignValuesToRegister(registerDTO, skipCodeBox, value, skipCodeList);
    }

    protected void assignValuesToRegisterAndCreate(List<String> registerIdList, CheckBox skipCodeBox, IntegerBox value, ListBox skipCodeList) {
        RegisterDTO registerDTO = meter.getRegister(registerIdList);
        if (registerDTO == null) {
            //just use the first code
            registerDTO = new RegisterDTO(registerIdList.get(0), null, null);
        }
        assignValuesToRegister(registerDTO, skipCodeBox, value, skipCodeList);
    }

    protected void assignValuesFromRegister(RegisterDTO registerDTO, CheckBox skipCodeBox, Label oldValue, IntegerBox value, ListBox skipCodeList, ValidationErrorWidget errorWidget) {
        value.setEnabled(true);
        skipCodeBox.setEnabled(true);

        if (registerDTO.getSkipCode() == null) {
            // Set the Value field
            String registerValue = "";
            if (registerDTO.getValue() != null) {
                registerValue = String.valueOf(registerDTO.getValue());
            }
            value.setText(registerValue);


            // Set validation error
            RegisterError error = registerDTO.getErrorType();

            if (multiRegister) {
                validator.addValidators("val" + registerDTO.getId(), new ServerRegisterValidator(value, error)
                        .addActionForFailure(new StyleAction("validationFailedBorder"))
                        .addActionForFailure(new ValidationErrorWidgetAction(errorWidget, true, SUPPLIED_READING_GENERIC_ERROR))
                        .addActionForFailure(new PopupPanelAction(PopupPanelAction.Position.SIDE))
                );
            } else {
                validator.addValidators("val" + registerDTO.getId(), new ServerRegisterValidator(value, error)
                        .addActionForFailure(new StyleAction("validationFailedBorder"))
                        .addActionForFailure(new ValidationErrorWidgetAction(errorWidget))
                );
            }

        } else {
            skipCodeBox.setValue(true);
            value.setEnabled(false);

            for (int i = 0; i < skipCodeList.getItemCount(); i++) {
                if (skipCodeList.getValue(i).equals(registerDTO.getSkipCode().getCode())) {
                    skipCodeList.setItemSelected(i, true);
                    break;
                }
            }

            skipCodeList.setEnabled(true);
        }

        String registerOldValue = "";
        if (registerDTO.getOldValue() != null) {
            registerOldValue = String.valueOf(registerDTO.getOldValue());
        }
        oldValue.setText(registerOldValue);

    }

    protected void assignValuesFromCommentToMeter(TextArea commentsField) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            meter.setComment("");
        } else {
            meter.setComment(commentsField.getText());
        }
    }

    protected void assignValuesFromMeterToComment(TextArea commentsField) {
        if ((meter.getComment() == null) || ("".equals(meter.getComment().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        } else {
            commentsField.setText(meter.getComment());
        }
    }

    /**
     * Default Meter Help
     *
     * @return default meter help
     */
    protected String getDefaultNonDialMeterHelp() {
        String help =
                "Please enter the meter reading values for each register.</br></br>" +
                        "If you receive an error message advising that a Validation error has occurred" +
                        " then please check the reason for the error and re-enter you meter reading.</br></br>" +
                        "If you continue to receive a Validation error but you are confident your reading is correct then you can override the validation check and submit your readings." +
                        "This reading will be reported and managed by Western Power.</br></br>" +
                        "If your meter is unreadable please 'click' on the box marked 'Unreadable' and select a reason from the drop down list.</br></br>" +
                        "Your 'last reading' is displayed for your information. If your last reading was an estimate then '(estimate)' is displayed next to your last reading.";
        return help;
    }

}
