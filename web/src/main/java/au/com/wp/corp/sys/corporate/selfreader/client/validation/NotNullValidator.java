package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import com.google.gwt.user.client.ui.HasValue;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

/**
 * Custom Validator for checking the meter type field returned from the web service
 * according to business rules
 */
public class NotNullValidator extends Validator<NotNullValidator> {

    private final HasValue<?> hasValue;


    /**
     * Constructor
     *
     * @param hasValue
     */
    public NotNullValidator(HasValue<?> hasValue) {
        this.hasValue = hasValue;
    }

    /**
     * Checks that the 2 email fields match and
     *
     * @param messages
     * @return
     */
    @Override
    public <V extends ValidationMessages> ValidationResult validate(V messages) {

        SelfreaderValidationMessages msgs = (SelfreaderValidationMessages) messages.getStandardMessages();

        if (hasValue.getValue() == null || hasValue.getValue().toString().trim().length() == 0) {
            return new ValidationResult(getErrorMessage(messages, msgs.notEmpty()));
        }

        return null;
    }

    /**
     * Method that invokes the actions that are to be performed
     * if the validate method returns a non null value (indicating)
     * validation has failed)
     *
     * @param validationResult
     */
    @Override
    public void invokeActions(ValidationResult validationResult) {
        for (ValidationAction va : this.getFailureActions())
            va.invoke(validationResult, hasValue);
    }
}
