package au.com.wp.corp.sys.corporate.selfreader.client;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * The LoginView and Presenter display the login screen and handle the authentication of the user's meter/nmi details.
 */
public interface LoginView extends IsWidget {

    public interface Presenter {
		void go(HasWidgets root);
        void login(String nmi, String meterNumber);
        void show(boolean show);

        void setNmiAndMeter(String nmi, String meterNumber);

        void resetCaptcha();
    }
	
	void setPresenter(Presenter presenter);
    void showLoading();
    void hideLoading();
    void show();
    void reset();
    void setNmiAndMeter(String nmi, String meterNumber);
    void resetValidator();
    String getRecaptchaChallenge(); // Returns the reCaptcha challenge string
    String getRecaptchaResponse();  // Reloads the reCaptcha respponse string
    void reloadRecaptcha();         // Reloads the reCaptcha challenge
    void displayRecaptchaError();   // Display the error returned from reCaptcha


}
