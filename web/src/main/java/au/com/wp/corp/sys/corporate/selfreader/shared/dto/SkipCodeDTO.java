package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

import java.io.Serializable;

/**
 * A Skip Code object.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class SkipCodeDTO implements Serializable {
    private String code;
    private String text;

    @SuppressWarnings("unused")
    private SkipCodeDTO() {
    }

    public SkipCodeDTO(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
