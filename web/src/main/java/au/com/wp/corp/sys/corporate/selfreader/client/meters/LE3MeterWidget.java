package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.Arrays;
import java.util.List;

/**
 * A MeterWidget for the RB1 meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class LE3MeterWidget extends AbstractMeterWidget {

    interface MyUiBinder extends UiBinder<HTMLPanel, LE3MeterWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register7Value;
    @UiField
    CheckBox register7SkipCodeBox;
    @UiField
    ListBox register7SkipCodeList;
    @UiField
    Label register7PreviousValue;
    @UiField
    IntegerBox register107Value;
    @UiField
    CheckBox register107SkipCodeBox;
    @UiField
    ListBox register107SkipCodeList;
    @UiField
    Label register107PreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;
    @UiField
    Label register010PreviousValue;
    @UiField
    IntegerBox register010Value;
    @UiField
    CheckBox register010SkipCodeBox;
    @UiField
    ListBox register010SkipCodeList;
    @UiField
    Label register110PreviousValue;
    @UiField
    IntegerBox register110Value;
    @UiField
    CheckBox register110SkipCodeBox;
    @UiField
    ListBox register110SkipCodeList;
    @UiField
    Label register020PreviousValue;
    @UiField
    IntegerBox register020Value;
    @UiField
    CheckBox register020SkipCodeBox;
    @UiField
    ListBox register020SkipCodeList;
    @UiField
    Label register120PreviousValue;
    @UiField
    IntegerBox register120Value;
    @UiField
    CheckBox register120SkipCodeBox;
    @UiField
    ListBox register120SkipCodeList;

    public LE3MeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter,validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_7:
                    assignValuesFromRegister(register, register7SkipCodeBox, register7PreviousValue, register7Value, register7SkipCodeList,serverError);
                    break;
                case REGISTER_107:
                    assignValuesFromRegister(register, register107SkipCodeBox, register107PreviousValue, register107Value, register107SkipCodeList,serverError);
                    break;
                case REGISTER_010:
                    assignValuesFromRegister(register, register010SkipCodeBox, register010PreviousValue, register010Value, register010SkipCodeList, serverError);
                    break;
                case REGISTER_110:
                    assignValuesFromRegister(register, register110SkipCodeBox, register110PreviousValue, register110Value, register110SkipCodeList, serverError);
                    break;
                case REGISTER_020:
                    assignValuesFromRegister(register, register020SkipCodeBox, register020PreviousValue, register020Value, register020SkipCodeList, serverError);
                    break;
                case REGISTER_120:
                    assignValuesFromRegister(register, register120SkipCodeBox, register120PreviousValue, register120Value, register120SkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);
    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(register7SkipCodeList);
        addSkipCodesToListBox(register107SkipCodeList);
        addSkipCodesToListBox(register010SkipCodeList);
        addSkipCodesToListBox(register110SkipCodeList);
        addSkipCodesToListBox(register020SkipCodeList);
        addSkipCodesToListBox(register120SkipCodeList);
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register7Value, register7SkipCodeBox, register7SkipCodeList);
        valid = valid & validateRegister(register107Value, register107SkipCodeBox, register107SkipCodeList);
        valid = valid & validateRegister(register010Value, register010SkipCodeBox, register010SkipCodeList);
        valid = valid & validateRegister(register110Value, register110SkipCodeBox, register110SkipCodeList);
        valid = valid & validateRegister(register020Value, register020SkipCodeBox, register020SkipCodeList);
        valid = valid & validateRegister(register120Value, register120SkipCodeBox, register120SkipCodeList);
        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_7.getCode(), register7SkipCodeBox, register7Value, register7SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_107.getCode(), register107SkipCodeBox, register107Value, register107SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_010.getCode(), register010SkipCodeBox, register010Value, register010SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_110.getCode(), register110SkipCodeBox, register110Value, register110SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_020.getCode(), register020SkipCodeBox, register020Value, register020SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_120.getCode(), register120SkipCodeBox, register120Value, register120SkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }


    @Override
    public void disable(boolean disable) {
        register7Value.setEnabled(!disable);
        register7SkipCodeList.setEnabled(!disable);
        register7SkipCodeBox.setEnabled(!disable);
        register107Value.setEnabled(!disable);
        register107SkipCodeList.setEnabled(!disable);
        register107SkipCodeBox.setEnabled(!disable);
        register010Value.setEnabled(!disable);
        register010SkipCodeList.setEnabled(!disable);
        register010SkipCodeBox.setEnabled(!disable);
        register110Value.setEnabled(!disable);
        register110SkipCodeList.setEnabled(!disable);
        register110SkipCodeBox.setEnabled(!disable);
        register020Value.setEnabled(!disable);
        register020SkipCodeList.setEnabled(!disable);
        register020SkipCodeBox.setEnabled(!disable);
        register120Value.setEnabled(!disable);
        register120SkipCodeList.setEnabled(!disable);
        register120SkipCodeBox.setEnabled(!disable);
    }

    @UiHandler("register7SkipCodeBox")
    public void onReg7UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register7SkipCodeBox.getValue();
        register7Value.setEnabled(!unreadable);
        register7SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register7SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register107SkipCodeBox")
    public void onReg107UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register107SkipCodeBox.getValue();
        register107Value.setEnabled(!unreadable);
        register107SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register107SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register010SkipCodeBox")
    public void onReg010UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register010SkipCodeBox.getValue();
        register010Value.setEnabled(!unreadable);
        register010SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register010SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register110SkipCodeBox")
    public void onReg110UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register110SkipCodeBox.getValue();
        register110Value.setEnabled(!unreadable);
        register110SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register110SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register020SkipCodeBox")
    public void onReg020UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register020SkipCodeBox.getValue();
        register020Value.setEnabled(!unreadable);
        register020SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register020SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register120SkipCodeBox")
    public void onReg120UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register120SkipCodeBox.getValue();
        register120Value.setEnabled(!unreadable);
        register120SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register120SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }
    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_7("007,07,7"),
        REGISTER_107("107"),
        REGISTER_010("010,10"),
        REGISTER_020("020,20"),
        REGISTER_110("110"),
        REGISTER_120("120"),
        NOVALUE("");

        private String value;

        Register(String value) {
            this.value=value;
        }

        /**
         * If we cant find the specific register we return novalue
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    String[] values = b.value.split(",");
                    List<String> valueSet = Arrays.asList(values);
                    if(valueSet.contains(value)){
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public List<String> getCode() {
                    return Arrays.asList(value.split(","));
                }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_7.getCode().get(0), register7Value, register7SkipCodeBox, register7SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_107.getCode().get(0), register107Value, register107SkipCodeBox, register107SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_010.getCode().get(0), register010Value, register010SkipCodeBox, register010SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_110.getCode().get(0), register110Value, register110SkipCodeBox, register110SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_020.getCode().get(0), register020Value, register020SkipCodeBox, register020SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_120.getCode().get(0), register120Value, register120SkipCodeBox, register120SkipCodeList,registerValidationError);
        return valid;
    }

}
