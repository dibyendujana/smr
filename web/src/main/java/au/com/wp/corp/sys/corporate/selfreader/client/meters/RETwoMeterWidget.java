package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReader;
import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.List;

/**
 * A MeterWidget for the RE2 meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class RETwoMeterWidget extends AbstractMeterWidget {
    interface MyUiBinder extends UiBinder<HTMLPanel, RETwoMeterWidget> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox registerExportTotalValue;
    @UiField
    CheckBox registerExportTotalSkipCodeBox;
    @UiField
    ListBox registerExportTotalSkipCodeList;
    @UiField
    IntegerBox registerAValue;
    @UiField
    CheckBox registerASkipCodeBox;
    @UiField
    ListBox registerASkipCodeList;
    @UiField
    IntegerBox registerCValue;
    @UiField
    CheckBox registerCSkipCodeBox;
    @UiField
    ListBox registerCSkipCodeList;
    @UiField
    IntegerBox registerBValue;
    @UiField
    CheckBox registerBSkipCodeBox;
    @UiField
    ListBox registerBSkipCodeList;
    @UiField
    IntegerBox registerDValue;
    @UiField
    CheckBox registerDSkipCodeBox;
    @UiField
    ListBox registerDSkipCodeList;
    @UiField
    Label registerExportTotalPreviousValue;
    @UiField
    Label registerAPreviousValue;
    @UiField
    Label registerCPreviousValue;
    @UiField
    Label registerBPreviousValue;
    @UiField
    Label registerDPreviousValue;
    @UiField
    IntegerBox registerImportTotalValue;
    @UiField
    CheckBox registerImportTotalSkipCodeBox;
    @UiField
    ListBox registerImportTotalSkipCodeList;
    @UiField
    IntegerBox registerAMinusValue;
    @UiField
    CheckBox registerAMinusSkipCodeBox;
    @UiField
    ListBox registerAMinusSkipCodeList;
    @UiField
    IntegerBox registerCMinusValue;
    @UiField
    CheckBox registerCMinusSkipCodeBox;
    @UiField
    ListBox registerCMinusSkipCodeList;
    @UiField
    IntegerBox registerBMinusValue;
    @UiField
    CheckBox registerBMinusSkipCodeBox;
    @UiField
    ListBox registerBMinusSkipCodeList;
    @UiField
    IntegerBox registerDMinusValue;
    @UiField
    CheckBox registerDMinusSkipCodeBox;
    @UiField
    ListBox registerDMinusSkipCodeList;
    @UiField
    Label registerImportTotalPreviousValue;
    @UiField
    Label registerAMinusPreviousValue;
    @UiField
    Label registerCMinusPreviousValue;
    @UiField
    Label registerBMinusPreviousValue;
    @UiField
    Label registerDMinusPreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    public RETwoMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter, validator);
    }

    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_IMPORT_TOTAL:
                    assignValuesFromRegister(register, registerImportTotalSkipCodeBox, registerImportTotalPreviousValue, registerImportTotalValue, registerImportTotalSkipCodeList, serverError);
                    break;
                case REGISTER_A:
                    assignValuesFromRegister(register, registerASkipCodeBox, registerAPreviousValue, registerAValue, registerASkipCodeList, serverError);
                    break;
                case REGISTER_B:
                    assignValuesFromRegister(register, registerBSkipCodeBox, registerBPreviousValue, registerBValue, registerBSkipCodeList, serverError);
                    break;
                case REGISTER_C:
                    assignValuesFromRegister(register, registerCSkipCodeBox, registerCPreviousValue, registerCValue, registerCSkipCodeList, serverError);
                    break;
                case REGISTER_D:
                    assignValuesFromRegister(register, registerDSkipCodeBox, registerDPreviousValue, registerDValue, registerDSkipCodeList, serverError);
                    break;
                case REGISTER_EXPORT_TOTAL:
                    assignValuesFromRegister(register, registerExportTotalSkipCodeBox, registerExportTotalPreviousValue, registerExportTotalValue, registerExportTotalSkipCodeList, serverError);
                    break;
                case REGISTER_A_MINUS:
                    assignValuesFromRegister(register, registerAMinusSkipCodeBox, registerAMinusPreviousValue, registerAMinusValue, registerAMinusSkipCodeList, serverError);
                    break;
                case REGISTER_B_MINUS:
                    assignValuesFromRegister(register, registerBMinusSkipCodeBox, registerBMinusPreviousValue, registerBMinusValue, registerBMinusSkipCodeList, serverError);
                    break;
                case REGISTER_C_MINUS:
                    assignValuesFromRegister(register, registerCMinusSkipCodeBox, registerCMinusPreviousValue, registerCMinusValue, registerCMinusSkipCodeList, serverError);
                    break;
                case REGISTER_D_MINUS:
                    assignValuesFromRegister(register, registerDMinusSkipCodeBox, registerDMinusPreviousValue, registerDMinusValue, registerDMinusSkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);
    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(registerExportTotalSkipCodeList);
        addSkipCodesToListBox(registerASkipCodeList);
        addSkipCodesToListBox(registerCSkipCodeList);
        addSkipCodesToListBox(registerBSkipCodeList);
        addSkipCodesToListBox(registerDSkipCodeList);
        addSkipCodesToListBox(registerImportTotalSkipCodeList);
        addSkipCodesToListBox(registerAMinusSkipCodeList);
        addSkipCodesToListBox(registerCMinusSkipCodeList);
        addSkipCodesToListBox(registerBMinusSkipCodeList);
        addSkipCodesToListBox(registerDMinusSkipCodeList);
    }


    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(registerExportTotalValue, registerExportTotalSkipCodeBox, registerExportTotalSkipCodeList);
        valid = valid & validateRegister(registerAValue, registerASkipCodeBox, registerASkipCodeList);
        valid = valid & validateRegister(registerCValue, registerCSkipCodeBox, registerCSkipCodeList);
        valid = valid & validateRegister(registerBValue, registerBSkipCodeBox, registerBSkipCodeList);
        valid = valid & validateRegister(registerDValue, registerDSkipCodeBox, registerDSkipCodeList);
        valid = valid & validateRegister(registerImportTotalValue, registerImportTotalSkipCodeBox, registerImportTotalSkipCodeList);
        valid = valid & validateRegister(registerAMinusValue, registerAMinusSkipCodeBox, registerAMinusSkipCodeList);
        valid = valid & validateRegister(registerCMinusValue, registerCMinusSkipCodeBox, registerCMinusSkipCodeList);
        valid = valid & validateRegister(registerBMinusValue, registerBMinusSkipCodeBox, registerBMinusSkipCodeList);
        valid = valid & validateRegister(registerDMinusValue, registerDMinusSkipCodeBox, registerDMinusSkipCodeList);

        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_EXPORT_TOTAL.getCode(), registerExportTotalSkipCodeBox, registerExportTotalValue, registerExportTotalSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_A.getCode(), registerASkipCodeBox, registerAValue, registerASkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_C.getCode(), registerCSkipCodeBox, registerCValue, registerCSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_B.getCode(), registerBSkipCodeBox, registerBValue, registerBSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_D.getCode(), registerDSkipCodeBox, registerDValue, registerDSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_IMPORT_TOTAL.getCode(), registerImportTotalSkipCodeBox, registerImportTotalValue, registerImportTotalSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_A_MINUS.getCode(), registerAMinusSkipCodeBox, registerAMinusValue, registerAMinusSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_C_MINUS.getCode(), registerCMinusSkipCodeBox, registerCMinusValue, registerCMinusSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_B_MINUS.getCode(), registerBMinusSkipCodeBox, registerBMinusValue, registerBMinusSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_D_MINUS.getCode(), registerDMinusSkipCodeBox, registerDMinusValue, registerDMinusSkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }

    @Override
    public void disable(boolean disable) {
        registerExportTotalValue.setEnabled(!disable);
        registerExportTotalSkipCodeList.setEnabled(!disable);
        registerExportTotalSkipCodeBox.setEnabled(!disable);
        registerAValue.setEnabled(!disable);
        registerASkipCodeList.setEnabled(!disable);
        registerASkipCodeBox.setEnabled(!disable);
        registerCValue.setEnabled(!disable);
        registerCSkipCodeList.setEnabled(!disable);
        registerCSkipCodeBox.setEnabled(!disable);
        registerBValue.setEnabled(!disable);
        registerBSkipCodeList.setEnabled(!disable);
        registerBSkipCodeBox.setEnabled(!disable);
        registerDValue.setEnabled(!disable);
        registerDSkipCodeList.setEnabled(!disable);
        registerDSkipCodeBox.setEnabled(!disable);
        registerImportTotalValue.setEnabled(!disable);
        registerImportTotalSkipCodeList.setEnabled(!disable);
        registerImportTotalSkipCodeBox.setEnabled(!disable);
        registerAMinusValue.setEnabled(!disable);
        registerAMinusSkipCodeList.setEnabled(!disable);
        registerAMinusSkipCodeBox.setEnabled(!disable);
        registerCMinusValue.setEnabled(!disable);
        registerCMinusSkipCodeList.setEnabled(!disable);
        registerCMinusSkipCodeBox.setEnabled(!disable);
        registerBMinusValue.setEnabled(!disable);
        registerBMinusSkipCodeList.setEnabled(!disable);
        registerBMinusSkipCodeBox.setEnabled(!disable);
        registerDMinusValue.setEnabled(!disable);
        registerDMinusSkipCodeList.setEnabled(!disable);
        registerDMinusSkipCodeBox.setEnabled(!disable);

    }

    @UiHandler("registerExportTotalSkipCodeBox")
    public void onRegExportTotalUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerExportTotalSkipCodeBox.getValue();
        registerExportTotalValue.setEnabled(!unreadable);
        registerExportTotalSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerExportTotalSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerASkipCodeBox")
    public void onRegAUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerASkipCodeBox.getValue();
        registerAValue.setEnabled(!unreadable);
        registerASkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerASkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerCSkipCodeBox")
    public void onRegCUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerCSkipCodeBox.getValue();
        registerCValue.setEnabled(!unreadable);
        registerCSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerCSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerBSkipCodeBox")
    public void onRegBUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerBSkipCodeBox.getValue();
        registerBValue.setEnabled(!unreadable);
        registerBSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerBSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerDSkipCodeBox")
    public void onRegDUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerDSkipCodeBox.getValue();
        registerDValue.setEnabled(!unreadable);
        registerDSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerDSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerImportTotalSkipCodeBox")
    public void onRegImportTotalUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerImportTotalSkipCodeBox.getValue();
        registerImportTotalValue.setEnabled(!unreadable);
        registerImportTotalSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerImportTotalSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerAMinusSkipCodeBox")
    public void onRegAMinusUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerAMinusSkipCodeBox.getValue();
        registerAMinusValue.setEnabled(!unreadable);
        registerAMinusSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerAMinusSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerCMinusSkipCodeBox")
    public void onReg1CMinusUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerCMinusSkipCodeBox.getValue();
        registerCMinusValue.setEnabled(!unreadable);
        registerCMinusSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerCMinusSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerBSkipCodeBox")
    public void onReg1BMinusUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerBMinusSkipCodeBox.getValue();
        registerBMinusValue.setEnabled(!unreadable);
        registerBMinusSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerBMinusSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerDSkipCodeBox")
    public void onReg1DMinusUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerDMinusSkipCodeBox.getValue();
        registerDMinusValue.setEnabled(!unreadable);
        registerDMinusSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
           registerDMinusSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }

    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("" .equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
//        REGISTER_EXPORT_TOTAL("07"),
//        REGISTER_A("10A"),
//        REGISTER_C("20C"),
//        REGISTER_B("30B"),
//        REGISTER_D("40D"),
//        REGISTER_IMPORT_TOTAL("47-"),
//        REGISTER_A_MINUS("50A-"),
//        REGISTER_C_MINUS("60C-"),
//        REGISTER_B_MINUS("70B-"),
//        REGISTER_D_MINUS("80D-"),
        REGISTER_EXPORT_TOTAL("07"),
        REGISTER_A("H10"),
        REGISTER_C("H20"),
        REGISTER_B("H30"),
        REGISTER_D("H40"),
        REGISTER_IMPORT_TOTAL("47-"),
        REGISTER_A_MINUS("H50-"),
        REGISTER_C_MINUS("H60-"),
        REGISTER_B_MINUS("H70-"),
        REGISTER_D_MINUS("H80-"),
        NOVALUE("");

        private String value;

        Register(String value) {
            this.value = value;
        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    if (value.equalsIgnoreCase(b.value)) {
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public String getCode() {
            return value;
        }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_EXPORT_TOTAL.getCode(), registerExportTotalValue, registerExportTotalSkipCodeBox, registerExportTotalSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_A.getCode(), registerAValue, registerASkipCodeBox, registerASkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_C.getCode(), registerCValue, registerCSkipCodeBox, registerCSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_B.getCode(), registerBValue, registerBSkipCodeBox, registerBSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_D.getCode(), registerDValue, registerDSkipCodeBox, registerDSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_IMPORT_TOTAL.getCode(), registerImportTotalValue, registerImportTotalSkipCodeBox, registerImportTotalSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_A_MINUS.getCode(), registerAMinusValue, registerAMinusSkipCodeBox, registerAMinusSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_C_MINUS.getCode(), registerCMinusValue, registerCMinusSkipCodeBox, registerCMinusSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_B_MINUS.getCode(), registerBMinusValue, registerBMinusSkipCodeBox, registerBMinusSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_D_MINUS.getCode(), registerDMinusValue, registerDMinusSkipCodeBox, registerDMinusSkipCodeList, registerValidationError);

        return valid;
    }

}
