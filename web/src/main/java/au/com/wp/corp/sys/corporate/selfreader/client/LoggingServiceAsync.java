package au.com.wp.corp.sys.corporate.selfreader.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * GWT RPC Service (async client interface) to log client side exceptions
 *
 * @author Thomas Buckel (n041982)
 */
public interface LoggingServiceAsync {

    public static final AsyncCallback<Void> NIL_CALLBACK = new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
        }
        @Override
        public void onSuccess(Void result) {
        }
    };

    void logUncaughtException(String message, String exceptionClassName, StackTraceElement[] stackTrace, AsyncCallback<Void> callback);

}
