package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

import java.io.Serializable;

/**
 * The details of a Register belonging to a Meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class RegisterDTO implements Serializable {
    private String id;
    private Integer value;
    private RegisterError errorType;
    private SkipCodeDTO skipCode;
    private Integer oldValue;

    @SuppressWarnings("unused")
    private RegisterDTO() {
    }

    public RegisterDTO(String id, RegisterError errorType, Integer oldValue) {
        this.setId(id);
        this.errorType = errorType;
        this.oldValue = oldValue;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public RegisterError getErrorType() {
        return errorType;
    }

    public void setErrorType(RegisterError errorType) {
        this.errorType = errorType;
    }

    public SkipCodeDTO getSkipCode() {
        return skipCode;
    }

    public void setSkipCode(SkipCodeDTO skipCode) {
        this.skipCode = skipCode;
    }

    public Integer getOldValue() {
        return oldValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
