package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;


import java.util.Arrays;
import java.util.List;

/**
 * A MeterWidget for the SMA meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class SMAMeterWidget extends AbstractMeterWidget {
    interface MyUiBinder extends UiBinder<HTMLPanel, SMAMeterWidget> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox registerDefaultValue;
    @UiField
    CheckBox registerDefaultSkipCodeBox;
    @UiField
    ListBox registerDefaultSkipCodeList;
    @UiField
    IntegerBox registerAValue;
    @UiField
    CheckBox registerASkipCodeBox;
    @UiField
    ListBox registerASkipCodeList;
    @UiField
    IntegerBox registerCValue;
    @UiField
    CheckBox registerCSkipCodeBox;
    @UiField
    ListBox registerCSkipCodeList;
    @UiField
    IntegerBox registerBValue;
    @UiField
    CheckBox registerBSkipCodeBox;
    @UiField
    ListBox registerBSkipCodeList;
    @UiField
    IntegerBox registerDValue;
    @UiField
    CheckBox registerDSkipCodeBox;
    @UiField
    ListBox registerDSkipCodeList;
    @UiField
    Label registerDefaultPreviousValue;
    @UiField
    Label registerAPreviousValue;
    @UiField
    Label registerCPreviousValue;
    @UiField
    Label registerBPreviousValue;
    @UiField
    Label registerDPreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    public SMAMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter, validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {

        registerDefaultValue.setEnabled(false);
        registerAValue.setEnabled(false);
        registerBValue.setEnabled(false);
        registerCValue.setEnabled(false);
        registerDValue.setEnabled(false);

        registerDefaultSkipCodeBox.setEnabled(false);
        registerASkipCodeBox.setEnabled(false);
        registerBSkipCodeBox.setEnabled(false);
        registerCSkipCodeBox.setEnabled(false);
        registerDSkipCodeBox.setEnabled(false);

        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_DEFAULT:
                    assignValuesFromRegister(register, registerDefaultSkipCodeBox, registerDefaultPreviousValue, registerDefaultValue, registerDefaultSkipCodeList, serverError);
                    break;
                case REGISTER_A:
                    assignValuesFromRegister(register, registerASkipCodeBox, registerAPreviousValue, registerAValue, registerASkipCodeList, serverError);
                    break;
                case REGISTER_B:
                    assignValuesFromRegister(register, registerBSkipCodeBox, registerBPreviousValue, registerBValue, registerBSkipCodeList, serverError);
                    break;
                case REGISTER_C:
                    assignValuesFromRegister(register, registerCSkipCodeBox, registerCPreviousValue, registerCValue, registerCSkipCodeList, serverError);
                    break;
                case REGISTER_D:
                    assignValuesFromRegister(register, registerDSkipCodeBox, registerDPreviousValue, registerDValue, registerDSkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);

    }


    protected void processSkipCodes() {
        addSkipCodesToListBox(registerDefaultSkipCodeList);
        addSkipCodesToListBox(registerASkipCodeList);
        addSkipCodesToListBox(registerCSkipCodeList);
        addSkipCodesToListBox(registerBSkipCodeList);
        addSkipCodesToListBox(registerDSkipCodeList);
    }


    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(registerDefaultValue, registerDefaultSkipCodeBox, registerDefaultSkipCodeList);
        valid = valid & validateRegister(registerAValue, registerASkipCodeBox, registerASkipCodeList);
        valid = valid & validateRegister(registerCValue, registerCSkipCodeBox, registerCSkipCodeList);
        valid = valid & validateRegister(registerBValue, registerBSkipCodeBox, registerBSkipCodeList);
        valid = valid & validateRegister(registerDValue, registerDSkipCodeBox, registerDSkipCodeList);

        return valid;
    }


    @Override
    public MeterDTO getMeter() {

        assignValuesToRegisterAndCreate(Register.REGISTER_DEFAULT.getCode(), registerDefaultSkipCodeBox, registerDefaultValue, registerDefaultSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_A.getCode(), registerASkipCodeBox, registerAValue, registerASkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_C.getCode(), registerCSkipCodeBox, registerCValue, registerCSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_B.getCode(), registerBSkipCodeBox, registerBValue, registerBSkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_D.getCode(), registerDSkipCodeBox, registerDValue, registerDSkipCodeList);
        assignValuesFromCommentToMeter(commentsField);


        return meter;
    }


    @Override
    public void disable(boolean disable) {
        registerDefaultValue.setEnabled(!disable);
        registerDefaultSkipCodeList.setEnabled(!disable);
        registerDefaultSkipCodeBox.setEnabled(!disable);
        registerAValue.setEnabled(!disable);
        registerASkipCodeList.setEnabled(!disable);
        registerASkipCodeBox.setEnabled(!disable);
        registerCValue.setEnabled(!disable);
        registerCSkipCodeList.setEnabled(!disable);
        registerCSkipCodeBox.setEnabled(!disable);
        registerBValue.setEnabled(!disable);
        registerBSkipCodeList.setEnabled(!disable);
        registerBSkipCodeBox.setEnabled(!disable);
        registerDValue.setEnabled(!disable);
        registerDSkipCodeList.setEnabled(!disable);
        registerDSkipCodeBox.setEnabled(!disable);

    }

    @UiHandler("registerDefaultSkipCodeBox")
    public void onRegDefaultUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerDefaultSkipCodeBox.getValue();
        registerDefaultValue.setEnabled(!unreadable);
        registerDefaultSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            registerDefaultSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerASkipCodeBox")
    public void onRegAUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerASkipCodeBox.getValue();
        registerAValue.setEnabled(!unreadable);
        registerASkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            registerASkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerBSkipCodeBox")
    public void onRegBUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerBSkipCodeBox.getValue();
        registerBValue.setEnabled(!unreadable);
        registerBSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            registerBSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerCSkipCodeBox")
    public void onRegCUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerCSkipCodeBox.getValue();
        registerCValue.setEnabled(!unreadable);
        registerCSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            registerCSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("registerDSkipCodeBox")
    public void onRegDUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerDSkipCodeBox.getValue();
        registerDValue.setEnabled(!unreadable);
        registerDSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            registerDSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }

    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("" .equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_DEFAULT("07,007"),
        REGISTER_A("10A"),
        REGISTER_B("30B"),
        REGISTER_C("20C"),
        REGISTER_D("40D"),
        NOVALUE("");


        private String value;

        Register(String value) {
            this.value = value;
        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    String[] values = b.value.split(",");
                    List<String> valueSet = Arrays.asList(values);
                    if (valueSet.contains(value)) {
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public List<String> getCode() {
            return Arrays.asList(value.split(","));
        }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_DEFAULT.getCode().get(0), registerDefaultValue, registerDefaultSkipCodeBox, registerDefaultSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_A.getCode().get(0), registerAValue, registerASkipCodeBox, registerASkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_C.getCode().get(0), registerCValue, registerCSkipCodeBox, registerCSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_B.getCode().get(0), registerBValue, registerBSkipCodeBox, registerBSkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_D.getCode().get(0), registerDValue, registerDSkipCodeBox, registerDSkipCodeList, registerValidationError);
        return valid;
    }
}
