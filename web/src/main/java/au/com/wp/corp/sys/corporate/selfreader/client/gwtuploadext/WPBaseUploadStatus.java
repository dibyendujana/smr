package au.com.wp.corp.sys.corporate.selfreader.client.gwtuploadext;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import com.google.gwt.user.client.Window;
import gwtupload.client.BaseUploadStatus;
import gwtupload.client.IUploadStatus;

/**
 * Extends the BaseUploadStatus in file upload so we can throw a custom error message rather then the Blat we get from
 * the default implementation
 * @author Isaac Stephens <isaac.stephens@westernpower.com.au>
 */
public class WPBaseUploadStatus extends BaseUploadStatus {
    protected SelfReaderView presenter;

    public WPBaseUploadStatus(SelfReaderView presenter) {
        super();
        this.presenter = presenter;
    }

    /* (non-Javadoc)
    * @see gwtupload.client.IUploadStatus#setError(java.lang.String)
    */
    @Override
    public void setError(String msg) {
        setStatus(Status.ERROR);
        presenter.showErrorInformation(msg.replaceAll("\\\\n", "\\n"));
    }

    /* (non-Javadoc)
    * @see gwtupload.client.IUploadStatus#newInstance()
    */
    @Override
    public IUploadStatus newInstance() {
        IUploadStatus ret = new WPBaseUploadStatus(presenter);
        ret.setCancelConfiguration(cancelCfg);
        return ret;
    }
}
