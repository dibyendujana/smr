package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.events.HidePopupEvent;
import au.com.wp.corp.sys.corporate.selfreader.client.events.SubmissionSuccessfulEvent;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.HelpPanel;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.MessageBox;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.InvalidImageFileException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.RegisterSubmissionFailedException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.SubmittedWithWarningException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.WebmethodsException;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import java.util.List;

public class SelfReaderPresenter implements SelfReaderView.Presenter, HidePopupEvent.Handler {
    private final EventBus eventBus;
    private final SelfReaderServiceAsync serviceAsync;
    private final SelfReaderView display;
    private boolean arrivedByLink = false;
    private String uploadedImageFileName;
    private HelpPanel helpPanel;
    private MessageBox messageBox;

    private static final String SELF_SERVICE_ERROR_MESSAGE = "The Self Read portal is currenlty experiencing problems. Please contact Western Power on 13 10 87.";
    private static final String INVALID_IMAGE_FILE_MESSAGE = "The uploaded image file is not valid.";
    private static final String REGISTER_VALIDATION_MESSAGE = "There was a validation warning when validating the register submissions.";


    private int attempts;

    @Inject
    public SelfReaderPresenter(EventBus eventBus, SelfReaderServiceAsync serviceAsync, SelfReaderView display, MessageBox messageBox, HelpPanel helpPanel) {
        this.eventBus = eventBus;
        this.serviceAsync = serviceAsync;
        this.display = display;
        this.helpPanel = helpPanel;
        this.messageBox = messageBox;
        display.setPresenter(this);
        eventBus.addHandler(HidePopupEvent.TYPE,this);
    }

    @Override
    public void go(HasWidgets root) {
        Widget w = this.display.asWidget();
        root.add(w);
        w.setVisible(false);
    }

    @Override
    public void setArrivedByLink(boolean arrivedByLink) {
        this.arrivedByLink = arrivedByLink;
    }

    @Override
    public void setUploadedImageFileName(String fileName) {
        uploadedImageFileName = fileName;
    }

    @Override
    public void displayMeter(MeterDTO meter) {
        attempts = 0;
        uploadedImageFileName = null;
        getDisplay().setMeter(meter, false);
    }

    @Override
    public void setSkipCodes(List<SkipCodeDTO> skipCodes) {
        GWT.log("setting " + skipCodes.size() + " skipcodes");
        getDisplay().setSkipCodes(skipCodes);
    }

    @Override
    public void show(boolean show) {
        getDisplay().asWidget().setVisible(show);
    }

    public SelfReaderView getDisplay() {
        return display;
    }

    @Override
    public void clear() {
        getDisplay().setMeter(null, false);
        getDisplay().hideWarningMessage();
        helpPanel.hide();
        attempts = 0;
        uploadedImageFileName = null;
    }

    @Override
    public void showUnsubmittableMeterWarningMessage() {
        getDisplay().showUnsubmittableMeterMessage();
    }

    @Override
    public void displayHelp(String helpMsgHtml, Widget target) {
        helpPanel.show(helpMsgHtml, target);
    }

    @Override
    public void showInformation(String message) {
        messageBox.showWarningInformation(message);
    }


    @Override
    public void showErrorInformation(String message) {
        messageBox.showErrorInformation(message);
    }

    @Override
    public void submitMeter(MeterDTO meter, boolean overrideFlag) {
        getDisplay().showSubmitting();

        // Determine the source system and pass it through
        String browser = Window.Navigator.getUserAgent();
        String source = "WEB";
        source += " ~ " + browser;

        serviceAsync.submitMeterDetails(meter, overrideFlag, attempts, source, uploadedImageFileName, new AsyncCallback<MeterDTO>() {
            @Override
            public void onFailure(Throwable caught) {
                if (caught instanceof InvalidImageFileException) {
                     messageBox.showErrorInformation(INVALID_IMAGE_FILE_MESSAGE);
                 }
                if (caught instanceof WebmethodsException) {
                    messageBox.showErrorInformation(SELF_SERVICE_ERROR_MESSAGE);
                }
                if (caught instanceof SubmittedWithWarningException) {
                    // disable all of the fields (eg make it read only apart from the logout button)
                    getDisplay().disableFields();
                    getDisplay().showSuccessWithDisclaimerMessage(caught.getMessage());
                }
                if (caught instanceof RegisterSubmissionFailedException) {
                    attempts++;
                    messageBox.showWarningInformation(REGISTER_VALIDATION_MESSAGE);
                    getDisplay().setMeter(((RegisterSubmissionFailedException) caught).getMeter(), true);
                }
                //TODO handle meter being outside window (eg due to the readDate)
                //TODO handle meter being outside window (eg due to the window being closed)
                //TODO handle register(s) being invalid
                //TODO handle all other issues
                getDisplay().hideSubmitting();
            }

            @Override
            public void onSuccess(MeterDTO result) {
                getDisplay().hideSubmitting();
                // disable all of the fields (eg make it read only apart from the logout button)
                getDisplay().disableFields();
                // display successful message
                // getDisplay().showSuccessMessage();

                eventBus.fireEvent(new SubmissionSuccessfulEvent());
            }
        });

    }

    @Override
    public void onEvent() {
        if (getDisplay() != null) {
            getDisplay().hideShowingCalendar();
        }
    }

}
