package au.com.wp.corp.sys.corporate.selfreader.server.handler;

import au.com.westernpower.wp.mr.wsp.meterreadingsservices.*;
import au.com.wp.corp.sys.corporate.selfreader.server.util.CalendarHelper;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.*;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.InvalidMeterException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.WebmethodsException;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.InvalidImageFileException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.gwtwidgets.server.spring.ServletUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.XMLGregorianCalendar;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Helper class for transforming DTOs of SelfReader Webservice back and forth
 * @author Isaac Stephens <isaac.stephens@westernpower.com.au>
 */
@Component
public class SelfReaderServiceHelper {
    private final static Logger log = Logger.getLogger(SelfReaderServiceHelper.class);


    @Value("${fileupload.location.directory}")
    private String filePath;

    /**
     * Mapp the MeterDTO to the retrieve input to the retrieve webservice
     *
     * @param nmi the nmi plus checkdigit
     * @param meterNumber the meter number
     * @param arrivedByLink how the user got to the service via email or browser
     * @param browser the version of the clients browser
     */
    public GetMeterReferenceDataInput mapMeterReferenceDataFromDTO(String nmi, String meterNumber, boolean arrivedByLink, String browser) {
        ObjectFactory objectFactory = new ObjectFactory();
        GetMeterReferenceDataInput getMeterReferenceDataInput = objectFactory.createGetMeterReferenceDataInput();
        GetMeterReferenceDataRequest getMeterReferenceDataRequest = objectFactory.createGetMeterReferenceDataRequest();
        NMI requestNMI = objectFactory.createNMI();
        requestNMI.setNMI(nmi.substring(0, 10));
        requestNMI.setNMICheckDigit(objectFactory.createNMINMICheckDigit(nmi.substring(10)));
        getMeterReferenceDataRequest.setNMI(requestNMI);
        getMeterReferenceDataRequest.setMeterNumber(meterNumber);
        String source = arrivedByLink ? "EMAIL" : "WEB";
        source += " ~ " + browser;
        getMeterReferenceDataRequest.setSourceSystem(source);
        getMeterReferenceDataInput.setGetMeterReferenceData(getMeterReferenceDataRequest);
        return getMeterReferenceDataInput;
    }


    /**
     * Map the MeterDTO to the submission input to the submit webservice
     *
     * @param meterDTO the meterDTO to submit
     * @return the output to the webservice
     */
    protected SubmitReadingInput mapParametersToSubmitReadingInput(MeterDTO meterDTO, String browser, Integer attemptNumber, Boolean forceSubmission, String fileName) throws InvalidImageFileException, Exception {
        ObjectFactory factory = new ObjectFactory();
        SubmitReadingInput submitReadingInput = factory.createSubmitReadingInput();

        Reading reading = factory.createReading();
        reading.setMeter(mapFromMeterDTOtoMeter(meterDTO));



        String ipAddress = getClientIpAddress();
        log.debug("Client IP Address: " + ipAddress);

        AuditInformation auditInformation = mapAuditInformation(browser, ipAddress, attemptNumber, fileName);
        reading.setAudit(auditInformation);
        reading.setMeterAssetDisplayType(meterDTO.getMeterType().getCode());

        reading.setComments(factory.createReadingComments(meterDTO.getComment()));
        reading.setForceSubmission(forceSubmission);
        reading.setMeterNumber(meterDTO.getMeterNumber());

        NMI nmi = factory.createNMI();

        String nmiNoCheckDigit = getNMINoCheckDigit(meterDTO.getNmi());
        String nmiCheckDigit = getNMICheckDigit(meterDTO.getNmi());

        nmi.setNMI(nmiNoCheckDigit);
        nmi.setNMICheckDigit(factory.createNMINMICheckDigit(nmiCheckDigit));

        reading.setNMI(nmi);

        reading.setReadingDate(CalendarHelper.convertDateToXMLGregorianCalendar(meterDTO.getActualReadDate()));

        submitReadingInput.setSubmitReading(reading);

        return submitReadingInput;
    }



    private String getNMICheckDigit(String nmi) {
        return nmi.substring(10, 11);
    }

    private String getNMINoCheckDigit(String nmi) {
        return nmi.substring(0,10);
    }

    /**
     * Returns the client IP address. There is an assumption that the webserver passes this information to us
     * in the Header Paramater "proxy_ip". If this assumption is incorrect the value will be wrong.
     *
     * @return the ip address of the client browser
     */
    private String getClientIpAddress() {
        HttpServletRequest request = ServletUtils.getRequest();
        // Attempt to find the client IP address from a webserver redirect (this is what happens currently
        // at westernpower in DEV/TEST/PROD environments. Figured this out by sniffing in DEV.)
        String clientIP = request.getHeader("proxy-ip");
        if (clientIP == null) {
            // Werent able to find it from a webserver
            clientIP = request.getRemoteAddr();
            if (clientIP == null) {
                // Still couldnt figure out the address GRRRR so default it to an error
                clientIP="0.0.0.0";
            }
        }
        return clientIP;
    }



    /**
     * Maps a MeterDTO object to webservice Meter object
     * @param meterDTO dto to map
     * @return webservice meter object
     */
    private Meter mapFromMeterDTOtoMeter(MeterDTO meterDTO) {
        ObjectFactory factory = new ObjectFactory();
        Meter meter = factory.createMeter();
        // Add the registers to the meter
        for (RegisterDTO registerDTO : meterDTO.getRegisters()) {
            Register register = mapRegisterDTOtoRegister(registerDTO);
            meter.getRegisters().add(register);
        }
        return meter;
    }

/**
     * Maps a DTO object to webservice Register object for submission
     * @param registerDTO dto to map
     * @return webservice Register object
     */
    private Register mapRegisterDTOtoRegister(RegisterDTO registerDTO) {
        ObjectFactory factory = new ObjectFactory();
        Register register = factory.createRegister();

        register.setId(registerDTO.getId());
        // Ignore History for submission

        if (registerDTO.getSkipCode() != null) {
            register.setSkipCode(factory.createRegisterSkipCode(registerDTO.getSkipCode().getCode()));
        }
        // Ignore validation for submission
        if (registerDTO.getValue() != null) {
            register.setValue(factory.createRegisterValue(BigInteger.valueOf(registerDTO.getValue())));
        }
        return register;
    }

    /**
      * Creates an Audit object
      * @param sourceSystem the source system
      * @param ipAddress the ip address
      * @param attempts the number of submission attempts
      * @return the audit information
      */
     private AuditInformation mapAuditInformation(String sourceSystem, String ipAddress, Integer attempts, String fileName) throws InvalidImageFileException, Exception {
         ObjectFactory factory = new ObjectFactory();
         AuditInformation auditInformation = factory.createAuditInformation();
         auditInformation.setSourceSystem(sourceSystem);
         auditInformation.setAttempts(BigInteger.valueOf(attempts));
         auditInformation.setIdentifier(ipAddress);
         auditInformation.setTimeStamp(CalendarHelper.now());

         // Only if filename seems to exist
         if (fileName != null && !fileName.equals("")) {
            MeterImage image = mapMeterImage(fileName);
            if (image.getImageData().length > 0)
                auditInformation.setMeterPhoto(image);
            else
                throw new InvalidImageFileException("The image file is not valid");
         }
         return auditInformation;
     }

    private MeterImage mapMeterImage(String fileName) throws Exception {
        ObjectFactory factory = new ObjectFactory();
        MeterImage image = factory.createMeterImage();
        image.setFileName(fileName);
        image.setImageData(getFile(filePath + "/" + fileName));
        return image;
    }

    /**
     * Retrieve the file as a byte array
     * @param fileName the file name
     * @return a byte stream of the file
     * @throws Exception something happened while reading the file
     */
    private byte[] getFile(String fileName) throws Exception {
        return FileUtils.readFileToByteArray(new File(fileName));
    }

    private List<RegisterDTO> mapRegistersToDTO(List<Register> referenceRegisters) {
        List<RegisterDTO> dtos = new ArrayList<RegisterDTO>(referenceRegisters.size());
        for (Register register : referenceRegisters) {
            HistoryType history = register.getHistory().getValue();
            Integer oldValue = null;
            ReadingType oldReadingType = null;
            if (history != null) {
                BigInteger bigOldValue = history.getReadingValue();
                oldValue = bigOldValue.intValue();
                //TODO retrieve this from the returned meter information and add it to the register dto
                //ReadingType.valueOf(history.getReadingType());
            }

            RegisterError errorType= null;
            if (register.getValidation() != null) {
                ValidationType validationType = register.getValidation().getValue();
                if ((validationType != null) && (!validationType.isValid())) {
                    errorType = new RegisterError(validationType.getComment().getValue());
                }
            }
            RegisterDTO dto = new RegisterDTO(register.getId(), errorType, oldValue);
            dtos.add(dto);

        }
        return dtos;
    }

public MeterDTO mapMeterToDTO(GetMeterReferenceDataResponse response) throws WebmethodsException, InvalidMeterException {
        // validate that the meter has the correct number of registers
        MeterDTO meter = null;
        ReferenceData referenceData = response.getReferenceData();
        AddressType referenceAddress = referenceData.getAddress();
        Meter referenceMeter = referenceData.getMeter();
        String referenceMeterNumber = referenceData.getMeterNumber();
        NMI referenceNmi = referenceData.getNMI();
        String referenceMeterType = referenceData.getMeterAssetDisplayType();
        Date lastReadDate = getLastReadDate(referenceMeter);
        Date nextReadDate = getNextReadDate(response);
        MeterType meterType = null;
        if (notEmpty(referenceMeterType)) {
            meterType = MeterType.getFromCode(referenceMeterType);
        }
        String emailAddress = null;
        // TODO check that the number of registers matches the expected number for the meter type.
        List<Register> referenceRegisters = referenceMeter.getRegisters();
        if (referenceRegisters.size() > 0) {
            List<RegisterDTO> registers = mapRegistersToDTO(referenceRegisters);
            meter = new MeterDTO(referenceNmi.getNMI()+referenceNmi.getNMICheckDigit().getValue(), referenceMeterNumber, referenceAddress.getAddressLine1()+referenceAddress.getAddressLine2(), lastReadDate, nextReadDate, null, meterType, true, registers, emailAddress);
        } else {
            throw new InvalidMeterException();
        }

        return meter;
    }

    /**
     * Returns the last read date of a meter.
     *
     * Currently we determine the last read date by inspecting the first register and working out when it was
     * read. We default this as the last read date for everything.
     *
     * @param meter the meter from the webservice call
     * @return the last read date of the meter (assumption above is important)
     */
    private Date getLastReadDate(Meter meter) {
        Date date = null;
        List<Register> registers = meter.getRegisters();
        if (registers == null)
            return null;
        Register register = registers.get(0);
        HistoryType historyType = register.getHistory().getValue();
        if (historyType != null) {
            date = CalendarHelper.convertXMLGregorianCalendarToDate(historyType.getReadingDate());
        }
        return date;
    }


    /**
     * Returns the next read date of a meter.
     *
     * @param response the response from the webservice call
     * @return the next read date of the meter
     */
    private Date getNextReadDate(GetMeterReferenceDataResponse response) {
        Date date = null;
        ReferenceData referenceData = response.getReferenceData();
        if (referenceData != null) {
            XMLGregorianCalendar xmlCalendar = referenceData.getNextScheduledReadDate();
            if (xmlCalendar != null) {
                GregorianCalendar gregorianCalendar = xmlCalendar.toGregorianCalendar();
                date = gregorianCalendar.getTime();
            }
        }
        return date;
    }


    private boolean notEmpty(String s) {
        return (s != null) && (s.trim().length() > 0);
    }


    public String getSubmitResponseStatusCode(SubmitReadingOutput output) {
        return output.getSubmitReadingResponse().getHeader().getResponseStatusCode();
    }

    public String getSubmitResponseStatusDescription(SubmitReadingOutput output) {
        return output.getSubmitReadingResponse().getHeader().getResponseStatusDesc();
    }

    public String getRetrieveResponseStatusCode(GetMeterReferenceDataOutput response) {
          return response.getGetMeterReferenceDataResponse().getHeader().getResponseStatusCode();
    }

}
