package au.com.wp.corp.sys.corporate.selfreader.server.util;

/**
 * Created by IntelliJ IDEA.
 * User: N041844
 * Date: 2/12/11
 * Time: 9:42 AM
 * To change this template use File | Settings | File Templates.
 */
import javax.servlet.http.HttpServletRequest;

/**
 * Interface of a Spring Bean providing access to the current {@code HttpServletRequest}. The current
 * {@code HttpServletRequest} is set by {@link SpringExtendedDispatcherServlet}.
 *
 * @author n041982
 */
public interface HttpServletRequestHolder {

    void setHttpServletRequest(HttpServletRequest request);

    HttpServletRequest getHttpServletRequest();

    AuthenticationWrapper getAuthenticationWrapper();

}
