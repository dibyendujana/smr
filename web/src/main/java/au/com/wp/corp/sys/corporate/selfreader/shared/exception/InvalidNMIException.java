package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import java.io.Serializable;

/**
 * Indicates that the NMI is not valid, or that the nmi/meter combination is not registered for self reader service.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class InvalidNMIException extends Exception implements Serializable {

    public InvalidNMIException() {
    }
}
