package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

import java.io.Serializable;

/**
 * Represents an Address in MBS
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class AddressDTO implements Serializable {
    private String line1;
    private String line2;
    private String line3;
    private String suburb;
    private String state;
    private String postcode;
    private String country;

    public AddressDTO() {
    }

    public AddressDTO(String line1, String line2, String line3, String suburb, String state, String postcode, String country) {
        this.line1 = line1;
        this.line2 = line2;
        this.line3 = line3;
        this.suburb = suburb;
        this.state = state;
        this.postcode = postcode;
        this.country = country;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
