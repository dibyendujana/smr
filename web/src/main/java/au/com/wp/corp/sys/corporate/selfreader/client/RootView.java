package au.com.wp.corp.sys.corporate.selfreader.client;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * The RootView is the Root of the client presentation. All screens/pages are driven from this View and presenter.
 */
public interface RootView extends IsWidget {

    public interface Presenter {
		void go(HasWidgets root);

        void logout();

        void setNmiAndMeter(String nmi, String meterNumber);

        void onScrollEvent();

    }
	
	void setPresenter(Presenter presenter);
    HasWidgets getContentPanel();
    void showLogoutButton(boolean show);
    void showUnsubmittableMeterMessage();

}
