package au.com.wp.corp.sys.corporate.selfreader.server.services;

import au.com.westernpower.wp.mr.wsp.meterreadingsservices.MeterReadingsServicesPortType;

/**
 * Provides the webmethods webservice port, handling issues of locating the server etc...
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public interface WebmethodsService {
    MeterReadingsServicesPortType getMeterReadingsServicesPort();
}
