package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.ioc.DesktopInjectorWrapper;
import au.com.wp.corp.sys.corporate.selfreader.client.ioc.InjectorWrapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;

public class SelfReader implements EntryPoint {
	final private InjectorWrapper injectorWrapper = GWT.create(DesktopInjectorWrapper.class);
	
	@Override
	public void onModuleLoad() {
		SelfReaderApp application = injectorWrapper.getInjector().getApplication();
		application.run(RootLayoutPanel.get());
        Element loadingElement = DOM.getElementById("loading");
        if (loadingElement != null) {
            DOM.removeChild(RootPanel.getBodyElement(), loadingElement);
        }
	}
	

}
