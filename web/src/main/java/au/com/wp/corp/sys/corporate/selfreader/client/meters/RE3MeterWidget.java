package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.List;

/**
 * A MeterWidget for the RE3 meter.
 *
 * @author Noel Bannon
 */
public class RE3MeterWidget extends AbstractMeterWidget {
    interface MyUiBinder extends UiBinder<HTMLPanel, RE3MeterWidget> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register7Value;
    @UiField
    CheckBox register7SkipCodeBox;
    @UiField
    ListBox register7SkipCodeList;
    @UiField
    Label register7PreviousValue;
    @UiField
    IntegerBox register10Value;
    @UiField
    CheckBox register10SkipCodeBox;
    @UiField
    ListBox register10SkipCodeList;
    @UiField
    Label register10PreviousValue;
    @UiField
    IntegerBox register20Value;
    @UiField
    CheckBox register20SkipCodeBox;
    @UiField
    ListBox register20SkipCodeList;
    @UiField
    Label register20PreviousValue;
    @UiField
    IntegerBox register30Value;
    @UiField
    CheckBox register30SkipCodeBox;
    @UiField
    ListBox register30SkipCodeList;
    @UiField
    Label register30PreviousValue;
    @UiField
    IntegerBox register40Value;
    @UiField
    CheckBox register40SkipCodeBox;
    @UiField
    ListBox register40SkipCodeList;
    @UiField
    Label register40PreviousValue;
    @UiField
    IntegerBox register47Value;
    @UiField
    CheckBox register47SkipCodeBox;
    @UiField
    ListBox register47SkipCodeList;
    @UiField
    Label register47PreviousValue;
    @UiField
    IntegerBox register50Value;
    @UiField
    CheckBox register50SkipCodeBox;
    @UiField
    ListBox register50SkipCodeList;
    @UiField
    Label register50PreviousValue;
    @UiField
    IntegerBox register60Value;
    @UiField
    CheckBox register60SkipCodeBox;
    @UiField
    ListBox register60SkipCodeList;
    @UiField
    Label register60PreviousValue;

    @UiField
    IntegerBox register70Value;
    @UiField
    CheckBox register70SkipCodeBox;
    @UiField
    ListBox register70SkipCodeList;
    @UiField
    Label register70PreviousValue;
    @UiField
    IntegerBox register80Value;
    @UiField
    CheckBox register80SkipCodeBox;
    @UiField
    ListBox register80SkipCodeList;
    @UiField
    Label register80PreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    public RE3MeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter, validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_7:
                    assignValuesFromRegister(register, register7SkipCodeBox, register7PreviousValue, register7Value, register7SkipCodeList, serverError);
                    break;
                case REGISTER_10:
                    assignValuesFromRegister(register, register10SkipCodeBox, register10PreviousValue, register10Value, register10SkipCodeList, serverError);
                    break;
                case REGISTER_20:
                    assignValuesFromRegister(register, register20SkipCodeBox, register20PreviousValue, register20Value, register20SkipCodeList, serverError);
                    break;
                case REGISTER_30:
                    assignValuesFromRegister(register, register30SkipCodeBox, register30PreviousValue, register30Value, register30SkipCodeList, serverError);
                    break;
                case REGISTER_40:
                    assignValuesFromRegister(register, register40SkipCodeBox, register40PreviousValue, register40Value, register40SkipCodeList, serverError);
                    break;
                 case REGISTER_47:
                    assignValuesFromRegister(register, register47SkipCodeBox, register47PreviousValue, register47Value, register47SkipCodeList, serverError);
                    break;
                case REGISTER_50:
                    assignValuesFromRegister(register, register50SkipCodeBox, register50PreviousValue, register47Value, register47SkipCodeList, serverError);
                    break;
                case REGISTER_60:
                    assignValuesFromRegister(register, register60SkipCodeBox, register60PreviousValue, register60Value, register60SkipCodeList, serverError);
                    break;
                case REGISTER_70:
                    assignValuesFromRegister(register, register70SkipCodeBox, register70PreviousValue, register70Value, register70SkipCodeList, serverError);
                    break;
                case REGISTER_80:
                    assignValuesFromRegister(register, register80SkipCodeBox, register80PreviousValue, register80Value, register80SkipCodeList, serverError);
                    break;

                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);

    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(register7SkipCodeList);
        addSkipCodesToListBox(register10SkipCodeList);
        addSkipCodesToListBox(register20SkipCodeList);
        addSkipCodesToListBox(register30SkipCodeList);
        addSkipCodesToListBox(register40SkipCodeList);
        addSkipCodesToListBox(register47SkipCodeList);
        addSkipCodesToListBox(register50SkipCodeList);
        addSkipCodesToListBox(register60SkipCodeList);
        addSkipCodesToListBox(register70SkipCodeList);
        addSkipCodesToListBox(register80SkipCodeList);
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register7Value, register7SkipCodeBox, register7SkipCodeList);
        valid = valid & validateRegister(register10Value, register10SkipCodeBox, register10SkipCodeList);
        valid = valid & validateRegister(register20Value, register20SkipCodeBox, register20SkipCodeList);
        valid = valid & validateRegister(register30Value, register30SkipCodeBox, register30SkipCodeList);
        valid = valid & validateRegister(register40Value, register40SkipCodeBox, register40SkipCodeList);
        valid = valid & validateRegister(register47Value, register47SkipCodeBox, register47SkipCodeList);
        valid = valid & validateRegister(register50Value, register50SkipCodeBox, register50SkipCodeList);
        valid = valid & validateRegister(register60Value, register60SkipCodeBox, register60SkipCodeList);
        valid = valid & validateRegister(register70Value, register70SkipCodeBox, register70SkipCodeList);
        valid = valid & validateRegister(register80Value, register80SkipCodeBox, register80SkipCodeList);
        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_7.getCode(), register7SkipCodeBox, register7Value, register7SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_10.getCode(), register10SkipCodeBox, register10Value, register10SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_20.getCode(), register20SkipCodeBox, register20Value, register20SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_30.getCode(), register30SkipCodeBox, register30Value, register30SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_40.getCode(), register40SkipCodeBox, register40Value, register40SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_47.getCode(), register47SkipCodeBox, register47Value, register47SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_50.getCode(), register50SkipCodeBox, register50Value, register50SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_60.getCode(), register60SkipCodeBox, register60Value, register60SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_70.getCode(), register70SkipCodeBox, register70Value, register70SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_80.getCode(), register80SkipCodeBox, register80Value, register80SkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }


    @Override
    public void disable(boolean disable) {
        register7Value.setEnabled(!disable);
        register7SkipCodeList.setEnabled(!disable);
        register7SkipCodeBox.setEnabled(!disable);
        register10Value.setEnabled(!disable);
        register10SkipCodeList.setEnabled(!disable);
        register10SkipCodeBox.setEnabled(!disable);
        register20Value.setEnabled(!disable);
        register20SkipCodeList.setEnabled(!disable);
        register20SkipCodeBox.setEnabled(!disable);
        register30Value.setEnabled(!disable);
        register30SkipCodeList.setEnabled(!disable);
        register30SkipCodeBox.setEnabled(!disable);
        register40Value.setEnabled(!disable);
        register40SkipCodeList.setEnabled(!disable);
        register40SkipCodeBox.setEnabled(!disable);
        register47Value.setEnabled(!disable);
        register47SkipCodeList.setEnabled(!disable);
        register47SkipCodeBox.setEnabled(!disable);
        register50Value.setEnabled(!disable);
        register50SkipCodeList.setEnabled(!disable);
        register50SkipCodeBox.setEnabled(!disable);
        register60Value.setEnabled(!disable);
        register60SkipCodeList.setEnabled(!disable);
        register60SkipCodeBox.setEnabled(!disable);
        register70Value.setEnabled(!disable);
        register70SkipCodeList.setEnabled(!disable);
        register70SkipCodeBox.setEnabled(!disable);
        register80Value.setEnabled(!disable);
        register80SkipCodeList.setEnabled(!disable);
        register80SkipCodeBox.setEnabled(!disable);

    }

    @UiHandler("register7SkipCodeBox")
    public void onReg7UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register7SkipCodeBox.getValue();
        register7Value.setEnabled(!unreadable);
        register7SkipCodeList.setEnabled(unreadable);
        if (!unreadable)
            register7SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("register10SkipCodeBox")
    public void onReg10UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register10SkipCodeBox.getValue();
        register10Value.setEnabled(!unreadable);
        register10SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register10SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register20SkipCodeBox")
    public void onReg20UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register20SkipCodeBox.getValue();
        register20Value.setEnabled(!unreadable);
        register20SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register20SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register30SkipCodeBox")
    public void onReg30UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register30SkipCodeBox.getValue();
        register30Value.setEnabled(!unreadable);
        register30SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register30SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register40SkipCodeBox")
    public void onReg40UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register40SkipCodeBox.getValue();
        register40Value.setEnabled(!unreadable);
        register40SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register40SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register47SkipCodeBox")
    public void onReg47UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register47SkipCodeBox.getValue();
        register47Value.setEnabled(!unreadable);
        register47SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register47SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register50SkipCodeBox")
    public void onReg50UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register50SkipCodeBox.getValue();
        register50Value.setEnabled(!unreadable);
        register50SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register50SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register60SkipCodeBox")
    public void onReg60UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register60SkipCodeBox.getValue();
        register60Value.setEnabled(!unreadable);
        register60SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register60SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register70SkipCodeBox")
    public void onReg70UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register70SkipCodeBox.getValue();
        register70Value.setEnabled(!unreadable);
        register70SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register70SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register80SkipCodeBox")
    public void onReg80UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register80SkipCodeBox.getValue();
        register80Value.setEnabled(!unreadable);
        register80SkipCodeList.setEnabled(unreadable);

        if (!unreadable)
            register80SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }

    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_7("07"),
        REGISTER_10("10"),
        REGISTER_20("20"),
        REGISTER_30("30"),
        REGISTER_40("40"),
        REGISTER_47("47"),
        REGISTER_50("50"),
        REGISTER_60("60"),
        REGISTER_70("70"),
        REGISTER_80("80"),
        NOVALUE("");


        private String value;

        Register(String value) {
            this.value = value;
        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    if (value.equalsIgnoreCase(b.value)) {
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public String getCode() {
            return value;
        }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_7.getCode(), register7Value, register7SkipCodeBox, register7SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_10.getCode(), register10Value, register10SkipCodeBox, register10SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_20.getCode(), register20Value, register20SkipCodeBox, register20SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_30.getCode(), register30Value, register30SkipCodeBox, register30SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_40.getCode(), register40Value, register40SkipCodeBox, register40SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_47.getCode(), register47Value, register47SkipCodeBox, register47SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_50.getCode(), register50Value, register50SkipCodeBox, register50SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_60.getCode(), register60Value, register60SkipCodeBox, register60SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_70.getCode(), register70Value, register70SkipCodeBox, register70SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_80.getCode(), register80Value, register80SkipCodeBox, register80SkipCodeList, registerValidationError);
        return valid;
    }
}
