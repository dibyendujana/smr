package au.com.wp.corp.sys.corporate.selfreader.server.servlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.web.HttpRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Servlet handles uploading of images
 * @author Isaac Stephens <isaac.stephens@westernpower.com.au>
 */
public class ImageFileUploadServletHandler implements HttpRequestHandler {
    private static Logger log = Logger.getLogger(ImageFileUploadServletHandler.class);

    // Maximum file size 1MB
    private long MAXIMUM_FILE_SIZE = 1*1024*1024; // 1MB

    private String filePath;

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void handleRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        // process only multipart requests
        if (ServletFileUpload.isMultipartContent(request)) {

            /*
                Need to do this so the response can be returned correctly as a JSON message. Basically if we dont do that
                browsers wrap stuff in specific tags this works for non standard stuff.
             */
            response.setContentType("text/html");

            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(MAXIMUM_FILE_SIZE);

            // Parse the request
            try {
                List<FileItem> items = upload.parseRequest(request);
                for (FileItem item : items) {
                    // process only file upload - discard other form item types
                    if (item.isFormField()) continue;

                    String fileName = item.getName();
                    String extension = "";
                    // get only the file name not whole path
                    if (fileName != null) {
                        extension = FilenameUtils.getExtension(fileName);
                    }


                    File dir = new File(filePath);
                    File uploadedFile = File.createTempFile(getFilePrefix(), "." + extension, dir);

                    item.write(uploadedFile);
                    response.setStatus(HttpServletResponse.SC_CREATED);
                    response.getWriter().printf("{ \"fileId\": \"%s\" }", uploadedFile.getName());
                    response.flushBuffer();

                }
            } catch (FileUploadBase.SizeLimitExceededException e) {
                // File size is too great so send message back to client
                log.debug("File upload failed because it was too large");
                response.setStatus(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE);
                response.getWriter().printf("{ \"error\": \"%s\" }", e.getMessage());
            } catch (Exception e) {
                log.error("An exception occurred while trying to upload the file",e);
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,"An error occurred while creating the file : " + e.getMessage());
            }

        } else {
            log.error("A submission occurred to the servlet occured indicating the wrong media type ie not multipart/form encoding");
            response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE,
                    "Request contents type is not supported by the servlet.");
        }
    }


    /**
     * Return a prefix for the file as a time down to milliseconds
     * @return a prefix to the file as a time in milliseconds
     */
    protected String getFilePrefix() {
        Calendar now = Calendar.getInstance();
        DateFormat format = new SimpleDateFormat("yyMMddHHmmssSSS");
        return format.format(now.getTime());
    }

}
