package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.ui.HasValue;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

/**
 * Custom Validator for checking the nmi field
 */
public class NMINumberValidator extends Validator<NMINumberValidator> {

    private final HasValue<?> hasValue;


    /**
     * Constructor
     *
     * @param hasValue
     */
    public NMINumberValidator(HasValue<?> hasValue) {
        this.hasValue = hasValue;
    }

    /**
     * Checks that the 2 email fields match and
     *
     * @param messages
     * @return
     */
    @Override
    public <V extends ValidationMessages> ValidationResult validate(V messages) {

        SelfreaderValidationMessages msgs = (SelfreaderValidationMessages) messages.getStandardMessages();

        boolean valid = true;
        String nmi = hasValue.getValue().toString();
        if ((nmi == null) || (nmi.length()!=10)) {
            valid = false;
        } else {
            //regex check for non numeric values
            RegExp regExp = RegExp.compile("\\D+");
            MatchResult matcher = regExp.exec(nmi);

            if (regExp.test(nmi)) {
                valid = false;
            }
        }


        if (!valid) {
            return new ValidationResult(getErrorMessage(messages, msgs.nmiNumber()));
        }

        return null;
    }

    /**
     * Method that invokes the actions that are to be performed
     * if the validate method returns a non null value (indicating)
     * validation has failed)
     *
     * @param validationResult
     */
    @Override
    public void invokeActions(ValidationResult validationResult) {
        for (ValidationAction va : this.getFailureActions())
            va.invoke(validationResult, hasValue);
    }
}
