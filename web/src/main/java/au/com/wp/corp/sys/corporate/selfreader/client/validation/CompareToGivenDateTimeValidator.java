package au.com.wp.corp.sys.corporate.selfreader.client.validation;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 3/04/12
 * Time: 10:13 AM
 * To change this template use File | Settings | File Templates.
 */

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

import java.util.Date;

/**
 * Custom Validator for Date to do some comparisons with date from another Date
 */
public class CompareToGivenDateTimeValidator extends Validator<CompareToGivenDateTimeValidator> {
    private final DateBox dateBox;
    private final Date date;
    private final Date compareToDate;
    private final String compareToWidgetName;
    private final BeforeOrAfterCompareDate dateComparitor;

    public enum BeforeOrAfterCompareDate {BEFORE,AFTER};

     /**
     * Constructor
     *
     * @param dateBox             - the date box being validated
      * param date                - the actual date to compare
     * @param compareToDate       - optional date to compare the target actual date to
     * @param compareToWidgetName - optional widget name if we are doing a compare to validation
     * @param dateComparitor
     */
    public CompareToGivenDateTimeValidator(DateBox dateBox, Date date, Date compareToDate, String compareToWidgetName, BeforeOrAfterCompareDate dateComparitor) {
        this.dateBox = dateBox;
        this.date = date;
        this.compareToDate = compareToDate;
        this.compareToWidgetName = compareToWidgetName;
        this.dateComparitor = dateComparitor;
    }

    /**
        * Method that performs the actual validation logic
        *
        * @param messages
        * @param <V>
        * @return
        */
       @Override
       public <V extends ValidationMessages> ValidationResult validate(V messages) {
           Date today = new Date(System.currentTimeMillis());
           SelfreaderValidationMessages msgs = (SelfreaderValidationMessages) messages.getStandardMessages();
           String msg = null;


           boolean valid = true;

           switch (dateComparitor) {

               case BEFORE:
                   if (date != null && date.before(compareToDate)) {
                       valid = false;
                       msg = msgs.dateIsBeforeGivenDate(compareToWidgetName);
                   }
                   break;

               case AFTER:
                   if (date != null && date.after(compareToDate)) {
                       valid = false;
                       msg = msgs.dateIsAfterGivenDate(compareToWidgetName);
                   }

                   break;

               default:

           }

           if (!valid)
               return new ValidationResult(getErrorMessage(messages, msg));


           return null;
       }

       /**
        * Method that invokes the actions that are to be performed
        * if the validate method returns a non null value (indicating)
        * validation has failed)
        *
        * @param validationResult
        */
       @Override
       public void invokeActions(ValidationResult validationResult) {

           for (ValidationAction<TextBox> va : this.getFailureActions())
               va.invoke(validationResult, dateBox.getTextBox());
       }



}
