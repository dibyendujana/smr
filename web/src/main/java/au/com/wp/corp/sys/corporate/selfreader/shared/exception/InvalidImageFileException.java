package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import java.io.Serializable;

/**
 * An exception representing when the uploaded image file is empty.
 *
 * @author Grazyna Szadkowski <grazyna.szadkowski@westernpower.com.au>
 */
public class InvalidImageFileException extends Exception implements Serializable {
    @SuppressWarnings("unused")
    private InvalidImageFileException() {
    }

    public InvalidImageFileException(String message) {
        super(message);
    }
}
