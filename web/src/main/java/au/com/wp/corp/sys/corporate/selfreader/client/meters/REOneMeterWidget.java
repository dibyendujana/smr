package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.List;

/**
 * A MeterWidget for the RE1 meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class REOneMeterWidget extends AbstractMeterWidget {
    interface MyUiBinder extends UiBinder<HTMLPanel, REOneMeterWidget> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register7Value;
    @UiField
    CheckBox register7SkipCodeBox;
    @UiField
    ListBox register7SkipCodeList;
    @UiField
    IntegerBox register10Value;
    @UiField
    CheckBox register10SkipCodeBox;
    @UiField
    ListBox register10SkipCodeList;
    @UiField
    IntegerBox register20Value;
    @UiField
    CheckBox register20SkipCodeBox;
    @UiField
    ListBox register20SkipCodeList;
    @UiField
    IntegerBox register30Value;
    @UiField
    CheckBox register30SkipCodeBox;
    @UiField
    ListBox register30SkipCodeList;
    @UiField
    IntegerBox register40Value;
    @UiField
    CheckBox register40SkipCodeBox;
    @UiField
    ListBox register40SkipCodeList;
    @UiField
    Label register7PreviousValue;
    @UiField
    Label register10PreviousValue;
    @UiField
    Label register20PreviousValue;
    @UiField
    Label register30PreviousValue;
    @UiField
    Label register40PreviousValue;
    @UiField
    IntegerBox register107Value;
    @UiField
    CheckBox register107SkipCodeBox;
    @UiField
    ListBox register107SkipCodeList;
    @UiField
    IntegerBox register110Value;
    @UiField
    CheckBox register110SkipCodeBox;
    @UiField
    ListBox register110SkipCodeList;
    @UiField
    IntegerBox register120Value;
    @UiField
    CheckBox register120SkipCodeBox;
    @UiField
    ListBox register120SkipCodeList;
    @UiField
    IntegerBox register130Value;
    @UiField
    CheckBox register130SkipCodeBox;
    @UiField
    ListBox register130SkipCodeList;
    @UiField
    IntegerBox register140Value;
    @UiField
    CheckBox register140SkipCodeBox;
    @UiField
    ListBox register140SkipCodeList;
    @UiField
    Label register107PreviousValue;
    @UiField
    Label register110PreviousValue;
    @UiField
    Label register120PreviousValue;
    @UiField
    Label register130PreviousValue;
    @UiField
    Label register140PreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    public REOneMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter, validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_7:
                    assignValuesFromRegister(register, register7SkipCodeBox, register7PreviousValue, register7Value, register7SkipCodeList, serverError);
                    break;
                case REGISTER_10:
                    assignValuesFromRegister(register, register10SkipCodeBox, register10PreviousValue, register10Value, register10SkipCodeList, serverError);
                    break;
                case REGISTER_20:
                    assignValuesFromRegister(register, register20SkipCodeBox, register20PreviousValue, register20Value, register20SkipCodeList, serverError);
                    break;
                case REGISTER_30:
                    assignValuesFromRegister(register, register30SkipCodeBox, register30PreviousValue, register30Value, register30SkipCodeList, serverError);
                    break;
                case REGISTER_40:
                    assignValuesFromRegister(register, register40SkipCodeBox, register40PreviousValue, register40Value, register40SkipCodeList, serverError);
                    break;
                case REGISTER_107:
                    assignValuesFromRegister(register, register107SkipCodeBox, register107PreviousValue, register107Value, register107SkipCodeList, serverError);
                    break;
                case REGISTER_110:
                    assignValuesFromRegister(register, register110SkipCodeBox, register110PreviousValue, register110Value, register110SkipCodeList, serverError);
                    break;
                case REGISTER_120:
                    assignValuesFromRegister(register, register120SkipCodeBox, register120PreviousValue, register120Value, register120SkipCodeList, serverError);
                    break;
                case REGISTER_130:
                    assignValuesFromRegister(register, register130SkipCodeBox, register130PreviousValue, register130Value, register130SkipCodeList, serverError);
                    break;
                case REGISTER_140:
                    assignValuesFromRegister(register, register140SkipCodeBox, register140PreviousValue, register140Value, register140SkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);

    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(register7SkipCodeList);
        addSkipCodesToListBox(register10SkipCodeList);
        addSkipCodesToListBox(register20SkipCodeList);
        addSkipCodesToListBox(register30SkipCodeList);
        addSkipCodesToListBox(register40SkipCodeList);
        addSkipCodesToListBox(register107SkipCodeList);
        addSkipCodesToListBox(register110SkipCodeList);
        addSkipCodesToListBox(register120SkipCodeList);
        addSkipCodesToListBox(register130SkipCodeList);
        addSkipCodesToListBox(register140SkipCodeList);
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register7Value, register7SkipCodeBox, register7SkipCodeList);
        valid = valid & validateRegister(register10Value, register10SkipCodeBox, register10SkipCodeList);
        valid = valid & validateRegister(register20Value, register20SkipCodeBox, register20SkipCodeList);
        valid = valid & validateRegister(register30Value, register30SkipCodeBox, register30SkipCodeList);
        valid = valid & validateRegister(register40Value, register40SkipCodeBox, register40SkipCodeList);
        valid = valid & validateRegister(register107Value, register107SkipCodeBox, register107SkipCodeList);
        valid = valid & validateRegister(register110Value, register110SkipCodeBox, register110SkipCodeList);
        valid = valid & validateRegister(register120Value, register120SkipCodeBox, register120SkipCodeList);
        valid = valid & validateRegister(register130Value, register130SkipCodeBox, register130SkipCodeList);
        valid = valid & validateRegister(register140Value, register140SkipCodeBox, register140SkipCodeList);
        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_7.getCode(), register7SkipCodeBox, register7Value, register7SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_10.getCode(), register10SkipCodeBox, register10Value, register10SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_20.getCode(), register20SkipCodeBox, register20Value, register20SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_30.getCode(), register30SkipCodeBox, register30Value, register30SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_40.getCode(), register40SkipCodeBox, register40Value, register40SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_107.getCode(), register107SkipCodeBox, register107Value, register107SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_110.getCode(), register110SkipCodeBox, register110Value, register110SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_120.getCode(), register120SkipCodeBox, register120Value, register120SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_130.getCode(), register130SkipCodeBox, register130Value, register130SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_140.getCode(), register140SkipCodeBox, register140Value, register140SkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }


    @Override
    public void disable(boolean disable) {
        register7Value.setEnabled(!disable);
        register7SkipCodeList.setEnabled(!disable);
        register7SkipCodeBox.setEnabled(!disable);
        register10Value.setEnabled(!disable);
        register10SkipCodeList.setEnabled(!disable);
        register10SkipCodeBox.setEnabled(!disable);
        register20Value.setEnabled(!disable);
        register20SkipCodeList.setEnabled(!disable);
        register20SkipCodeBox.setEnabled(!disable);
        register30Value.setEnabled(!disable);
        register30SkipCodeList.setEnabled(!disable);
        register30SkipCodeBox.setEnabled(!disable);
        register40Value.setEnabled(!disable);
        register40SkipCodeList.setEnabled(!disable);
        register40SkipCodeBox.setEnabled(!disable);
        register107Value.setEnabled(!disable);
        register107SkipCodeList.setEnabled(!disable);
        register107SkipCodeBox.setEnabled(!disable);
        register110Value.setEnabled(!disable);
        register110SkipCodeList.setEnabled(!disable);
        register110SkipCodeBox.setEnabled(!disable);
        register120Value.setEnabled(!disable);
        register120SkipCodeList.setEnabled(!disable);
        register120SkipCodeBox.setEnabled(!disable);
        register130Value.setEnabled(!disable);
        register130SkipCodeList.setEnabled(!disable);
        register130SkipCodeBox.setEnabled(!disable);
        register140Value.setEnabled(!disable);
        register140SkipCodeList.setEnabled(!disable);
        register140SkipCodeBox.setEnabled(!disable);

    }

    @UiHandler("register7SkipCodeBox")
    public void onReg7UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register7SkipCodeBox.getValue();
        register7Value.setEnabled(!unreadable);
        register7SkipCodeList.setEnabled(unreadable);
        if(!unreadable)
            register7SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("register10SkipCodeBox")
    public void onReg10UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register10SkipCodeBox.getValue();
        register10Value.setEnabled(!unreadable);
        register10SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register10SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register20SkipCodeBox")
    public void onReg20UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register20SkipCodeBox.getValue();
        register20Value.setEnabled(!unreadable);
        register20SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register20SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register30SkipCodeBox")
    public void onReg30UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register30SkipCodeBox.getValue();
        register30Value.setEnabled(!unreadable);
        register30SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register30SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register40SkipCodeBox")
    public void onReg40UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register40SkipCodeBox.getValue();
        register40Value.setEnabled(!unreadable);
        register40SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register40SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register107SkipCodeBox")
    public void onReg107UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register107SkipCodeBox.getValue();
        register107Value.setEnabled(!unreadable);
        register107SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register107SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register110SkipCodeBox")
    public void onReg110UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register110SkipCodeBox.getValue();
        register110Value.setEnabled(!unreadable);
        register110SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register110SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register120SkipCodeBox")
    public void onReg120UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register120SkipCodeBox.getValue();
        register120Value.setEnabled(!unreadable);
        register120SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register120SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register130SkipCodeBox")
    public void onReg130UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register130SkipCodeBox.getValue();
        register130Value.setEnabled(!unreadable);
        register130SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register130SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register140SkipCodeBox")
    public void onReg140UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register140SkipCodeBox.getValue();
        register140Value.setEnabled(!unreadable);
        register140SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register140SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }

    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("" .equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_7("007"),
        REGISTER_10("010"),
        REGISTER_20("020"),
        REGISTER_30("030"),
        REGISTER_40("040"),
        REGISTER_107("107"),
        REGISTER_110("110"),
        REGISTER_120("120"),
        REGISTER_130("130"),
        REGISTER_140("140"),
        NOVALUE("");


        private String value;

        Register(String value) {
            this.value = value;
        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    if (value.equalsIgnoreCase(b.value)) {
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public String getCode() {
            return value;
        }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_7.getCode(), register7Value, register7SkipCodeBox, register7SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_10.getCode(), register10Value, register10SkipCodeBox, register10SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_20.getCode(), register20Value, register20SkipCodeBox, register20SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_30.getCode(), register30Value, register30SkipCodeBox, register30SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_40.getCode(), register40Value, register40SkipCodeBox, register40SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_107.getCode(), register107Value, register107SkipCodeBox, register107SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_110.getCode(), register110Value, register110SkipCodeBox, register110SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_120.getCode(), register120Value, register120SkipCodeBox, register120SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_130.getCode(), register130Value, register130SkipCodeBox, register130SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_140.getCode(), register140Value, register140SkipCodeBox, register140SkipCodeList, registerValidationError);
        return valid;
    }
}
