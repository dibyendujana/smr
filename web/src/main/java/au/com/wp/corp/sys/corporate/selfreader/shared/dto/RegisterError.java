package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

import java.io.Serializable;

/**
 * The type of validation errors that can be thrown by the WebMethods submission service.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class RegisterError implements Serializable{

    private String message;

    @SuppressWarnings("unused")
    private RegisterError() {
    }

    public RegisterError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message=message;
    }
}
