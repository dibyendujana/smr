package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The details of the Meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class MeterDTO implements Serializable {
    private String nmi;
    private String meterNumber;
    private String siteAddress;
    private Date lastReadDate;
    private Date nextReadDate;
    private Date actualReadDate;
    private MeterType meterType;
    private List<RegisterDTO> registers;
    private String comment;
    private Boolean submittable;
    private String emailAddress;

    @SuppressWarnings("unused")
    private MeterDTO() {
    }

    public MeterDTO(String nmi, String meterNumber, String siteAddress, Date lastReadDate, Date nextReadDate, Date actualReadDate, MeterType meterType, Boolean submittable, List<RegisterDTO> registers, String emailAddress) {
        this.nmi = nmi;
        this.meterNumber = meterNumber;
        this.siteAddress = siteAddress;
        this.lastReadDate = lastReadDate;
        this.nextReadDate = nextReadDate;
        this.actualReadDate = actualReadDate;
        this.meterType = meterType;
        this.submittable = submittable;
        this.registers = registers;
        this.emailAddress = emailAddress;
    }

    public String getNmi() {
        return nmi;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public String getSiteAddress() {
        return siteAddress;
    }
    public Date getLastReadDate() {
        return lastReadDate;
    }

    public Date getNextReadDate() {
        return nextReadDate;
    }

    public Date getActualReadDate() {
        return actualReadDate;
    }

    public MeterType getMeterType() {
        return meterType;
    }

    public List<RegisterDTO> getRegisters() {
        return registers;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setActualReadDate(Date actualReadDate) {
        this.actualReadDate = actualReadDate;
    }

    public void clearRegisters() {
        this.registers.clear();
    }

    public void setRegister(RegisterDTO register) {
        this.registers.add(register);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getSubmittable() {
        return submittable;
    }

    public void setSubmittable(Boolean submittable) {
        this.submittable=submittable;
    }

    /**
     * Returns the register with value id
     * @param id the id of the register to return
     * @return the register dto
     */
    public RegisterDTO getRegister(String id) {
        RegisterDTO retValue = null;
        if (registers != null) {
            for (RegisterDTO register : registers) {
                if (register != null) {
                    if (register.getId().equals(id)) {
                        retValue = register;
                    }
                }
            }
        }
        return retValue;
    }

    /**
     * Returns the register with value id
     * @param idList the list of potential id for the register
     * @return the register dto
     */
    public RegisterDTO getRegister(List<String> idList) {
        RegisterDTO retValue = null;
        if (registers != null) {
            for (RegisterDTO register : registers) {
                if (register != null) {
                    if (idList.contains(register.getId())) {
                        retValue = register;
                    }
                }
            }
        }
        return retValue;
    }



}
