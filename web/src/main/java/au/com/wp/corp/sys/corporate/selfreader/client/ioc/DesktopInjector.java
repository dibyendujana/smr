package au.com.wp.corp.sys.corporate.selfreader.client.ioc;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderApp;
import com.google.gwt.inject.client.GinModules;

/**
 * Date: 18/07/11
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
@GinModules(value = { SelfReaderModule.class })
public interface DesktopInjector extends SelfReaderInjector {

	SelfReaderApp getApplication();
}