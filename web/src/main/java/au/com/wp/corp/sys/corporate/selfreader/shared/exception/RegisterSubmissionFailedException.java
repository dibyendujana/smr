package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;

import java.io.Serializable;

/**
 * Thrown when the Webmethods webservice returns that the submission of the Meter Reading has been rejected.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class RegisterSubmissionFailedException extends Exception implements Serializable {
    private MeterDTO meter;

    @SuppressWarnings("unused")
    private RegisterSubmissionFailedException() {
    }

    public RegisterSubmissionFailedException(MeterDTO meter) {
        this.meter = meter;
    }

    public MeterDTO getMeter() {
        return meter;
    }
}
