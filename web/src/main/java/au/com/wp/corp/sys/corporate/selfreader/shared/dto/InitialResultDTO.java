package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Contains the Meter and the List of skip codes that can be used to explain why a register can't be read.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class InitialResultDTO implements Serializable {
    private MeterDTO meterDTO;
    private List<SkipCodeDTO> skipCodes;

    @SuppressWarnings("unused")
    private InitialResultDTO() {
    }

    public InitialResultDTO(MeterDTO meterDTO, List<SkipCodeDTO> skipCodes) {
        this.meterDTO = meterDTO;
        this.skipCodes = skipCodes;
    }

    public MeterDTO getMeterDTO() {
        return meterDTO;
    }

    public List<SkipCodeDTO> getSkipCodes() {
        return skipCodes;
    }
}
