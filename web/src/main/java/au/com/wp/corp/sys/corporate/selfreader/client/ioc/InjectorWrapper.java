package au.com.wp.corp.sys.corporate.selfreader.client.ioc;

public interface InjectorWrapper {
	SelfReaderInjector getInjector();
}
