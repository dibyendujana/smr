package au.com.wp.corp.sys.corporate.selfreader.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event broadcast on successful Meter Submission.
 * @author Isaac Stephens <isaac.stephens@westernpower.com.au>
 */
public class SubmissionSuccessfulEvent extends GwtEvent<SubmissionSuccessfulEvent.Handler> {
    public static final Type<SubmissionSuccessfulEvent.Handler> TYPE = new Type<SubmissionSuccessfulEvent.Handler>();


    public interface Handler extends EventHandler {
        void onSubmissionSuccessfull();
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onSubmissionSuccessfull();
    }
}
