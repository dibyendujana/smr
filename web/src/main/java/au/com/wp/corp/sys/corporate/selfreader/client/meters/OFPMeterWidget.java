package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.List;

/**
 * A MeterWidget for the OFP meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class OFPMeterWidget extends AbstractMeterWidget {
    interface MyUiBinder extends UiBinder<HTMLPanel, OFPMeterWidget> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register3Value;
    @UiField
    CheckBox register3SkipCodeBox;
    @UiField
    ListBox register3SkipCodeList;
    @UiField
    Label register3PreviousValue;
    @UiField
    IntegerBox register7Value;
    @UiField
    CheckBox register7SkipCodeBox;
    @UiField
    ListBox register7SkipCodeList;
    @UiField
    Label register7PreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    public OFPMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter, validator);
    }

    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_3:
                    assignValuesFromRegister(register, register3SkipCodeBox, register3PreviousValue, register3Value, register3SkipCodeList, serverError);
                    break;
                case REGISTER_7:
                    assignValuesFromRegister(register, register7SkipCodeBox, register7PreviousValue, register7Value, register7SkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);
    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(register3SkipCodeList);
        addSkipCodesToListBox(register7SkipCodeList);
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register3Value, register3SkipCodeBox, register3SkipCodeList);
        valid = valid & validateRegister(register7Value, register7SkipCodeBox, register7SkipCodeList);
        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_3.getCode(), register3SkipCodeBox, register3Value, register3SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_7.getCode(), register7SkipCodeBox, register7Value, register7SkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }

    @Override
    public void disable(boolean disable) {
        register3Value.setEnabled(!disable);
        register3SkipCodeList.setEnabled(!disable);
        register3SkipCodeBox.setEnabled(!disable);
        register7Value.setEnabled(!disable);
        register7SkipCodeList.setEnabled(!disable);
        register7SkipCodeBox.setEnabled(!disable);
    }

    @UiHandler("register7SkipCodeBox")
    public void onReg7UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register7SkipCodeBox.getValue();
        register7Value.setEnabled(!unreadable);
        register7SkipCodeList.setEnabled(unreadable);

         if(!unreadable)
            register7SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("register3SkipCodeBox")
    public void onReg3UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register3SkipCodeBox.getValue();
        register3Value.setEnabled(!unreadable);
        register3SkipCodeList.setEnabled(unreadable);

         if(!unreadable)
            register3SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }

    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("" .equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_3("007"),
        REGISTER_7("020"),
        NOVALUE("");


        private String value;

        Register(String value) {
            this.value = value;
        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    if (value.equalsIgnoreCase(b.value)) {
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public String getCode() {
            return value;
        }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_3.getCode(), register3Value, register3SkipCodeBox, register3SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_7.getCode(), register7Value, register7SkipCodeBox, register7SkipCodeList, registerValidationError);
        return valid;
    }
}
