package au.com.wp.corp.sys.corporate.selfreader.client.validation;

/**
 * @Author: egszadk
 */
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox ;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;
import org.springframework.beans.factory.annotation.Value;

/**
 * Custom Validator for checking format of email address of the customer
 */
public class EmailAddressFormatValidator extends Validator<EmailAddressFormatValidator> {

    private final TextBox textBox;

    private static final String INITIAL_EMAIL_ADDRESS_MESSAGE = "Enter your email address here if receipt required.";

    /**
     * Constructor
     *
     * @param textBox
     */
    public EmailAddressFormatValidator(TextBox textBox) {
        this.textBox = textBox;
    }

    /**
     * Perfomr the validation
     *
     * @param messages
     * @return
     */
    @Override
    public <V extends ValidationMessages> ValidationResult validate(V messages) {

        SelfreaderValidationMessages msgs = (SelfreaderValidationMessages) messages.getStandardMessages();

        if ((textBox.getText() != null) && !(textBox.getText().matches("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"))
        && !(textBox.getText().equals(INITIAL_EMAIL_ADDRESS_MESSAGE))) {
            return new ValidationResult(getErrorMessage(messages, msgs.notAValidEmail(textBox.getText())));
        }

        return null;
    }

    /**
     * Method that invokes the actions that are to be performed
     * if the validate method returns a non null value (indicating)
     * validation has failed)
     *
     * @param validationResult
     */
    @Override
    public void invokeActions(ValidationResult validationResult) {
        for (ValidationAction va : this.getFailureActions())
            va.invoke(validationResult, textBox);
    }
}

