package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.utils.MessageBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.UmbrellaException;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;


public class SelfReaderApp {
	private final RootView.Presenter presenter;
    private final MessageBox messageBox;
    private final LoggingServiceAsync loggingService = (LoggingServiceAsync) GWT.create(LoggingService.class);
    private static EventBus eventBus;

	@Inject
	public SelfReaderApp(RootView.Presenter presenter, MessageBox messageBox, EventBus eventBus) {
		this.presenter = presenter;
        this.messageBox = messageBox;
        SelfReaderApp.eventBus = eventBus;
	}

    //so other non-guiced classes can get access to the singleton eventBus
    public static EventBus getEventBus() {
        return eventBus;
    }
	
	public void run(HasWidgets root) {
        // TODO replace this with the History functionality
        String nmi = com.google.gwt.user.client.Window.Location.getParameter("nmi");
        String meterNumber = com.google.gwt.user.client.Window.Location.getParameter("meter");

        if ((nmi != null) && (nmi.trim().length() > 0) && (meterNumber != null) && (meterNumber.trim().length() > 0)) {
            presenter.setNmiAndMeter(nmi, meterNumber);
        }

	    GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
	        public void onUncaughtException(Throwable e) {
                GWT.log("Uncaught exception", e);
                  reportExceptionToServer(e);
                  Throwable firstCause = getFirstNonUmbrellaException(e);
                  messageBox.showDefaultErrorDialog(firstCause.getMessage());
	        }
	      });

	    presenter.go(root);
	}


    private void reportExceptionToServer(Throwable t) {
        if (t instanceof UmbrellaException) {
            UmbrellaException umbrellaException = (UmbrellaException) t;
            for (Throwable cause : umbrellaException.getCauses()) {
                reportExceptionToServer(cause);
            }
        } else {
            String message = t.getMessage();
            String exceptionClass = t.getClass().getName();
            StackTraceElement[] stackTrace = t.getStackTrace();
            loggingService.logUncaughtException(message, exceptionClass, stackTrace, LoggingServiceAsync.NIL_CALLBACK);
        }
    }


    /**
     * @param t   Exception, possibly an UmbrellaException.
     * @return The first non UmbrellaException in the hierarchy of causes.
     *         If {@code t} is an UmbrellaException, it's first cause is check recursively, otherwise {@code t} is
     *         returned.
     */
    private Throwable getFirstNonUmbrellaException(Throwable t) {
        if (t instanceof UmbrellaException) {
            UmbrellaException umbrellaException = (UmbrellaException) t;
            if (umbrellaException.getCauses().size() > 0) {
                return getFirstNonUmbrellaException(umbrellaException.getCauses().iterator().next());
            } else {
                return t;
            }
        } else {
            return t;
        }
    }
	
}
