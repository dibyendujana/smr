package au.com.wp.corp.sys.corporate.selfreader.client.ioc;

import au.com.wp.corp.sys.corporate.selfreader.client.*;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.HelpPanel;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.HelpPanelImpl;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.MessageBox;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.MessageBoxImpl;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;

public class SelfReaderModule extends AbstractGinModule {
	
    @Override
    protected void configure() {
        bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
        bind(RootView.Presenter.class).to(RootPresenter.class);
        bind(RootView.class).to(RootViewImpl.class);
        bind(SelfReaderView.Presenter.class).to(SelfReaderPresenter.class);
        bind(SelfReaderView.class).to(SelfReaderViewImpl.class);
        bind(LoginView.Presenter.class).to(LoginPresenter.class);
        bind(LoginView.class).to(LoginViewImpl.class);

        bind(SubmissionSuccessfullView.class).to(SubmissionSuccessfullViewImpl.class);
        bind(SubmissionSuccessfullView.Presenter.class).to(SubmissionSuccessfullPresenter.class);

        bind(MessageBox.class).to(MessageBoxImpl.class);
        bind(HelpPanel.class).to(HelpPanelImpl.class);

    }


}
