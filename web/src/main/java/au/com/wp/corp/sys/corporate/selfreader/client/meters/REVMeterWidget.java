package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.List;

/**
 * A MeterWidget for the REV meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class REVMeterWidget extends AbstractDialMeterWidget {
    interface MyUiBinder extends UiBinder<HTMLPanel, REVMeterWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    CheckBox registerSkipCodeBox;
    @UiField
    ListBox registerSkipCodeList;
    @UiField
    IntegerBox fiveZero;
    @UiField
    IntegerBox fourZero;
    @UiField
    IntegerBox threeZero;
    @UiField
    IntegerBox twoZero;
    @UiField
    IntegerBox oneZero;
    @UiField
    IntegerBox noZero;
    @UiField
    Label previousRegisterValue;
    @UiField
    Image meterHelp;
    @UiField
    Image noZeroImage;
    @UiField
    Image oneZeroImage;
    @UiField
    Image twoZeroImage;
    @UiField
    Image threeZeroImage;
    @UiField
    Image fourZeroImage;
    @UiField
    Image fiveZeroImage;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    public REVMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator) {
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter,skipCodes, presenter,validator );
    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(registerSkipCodeList);
    }


    protected void setupMeter() {
        RegisterDTO register = meter.getRegister(Register.REGISTER_DEFAULT.getCode());
        if (register != null) {
            assignValuesFromRegister(register,registerSkipCodeBox,previousRegisterValue,fiveZero,fourZero,threeZero,twoZero,oneZero,noZero,registerSkipCodeList,serverError);
            assignValuesFromMeterToComment(commentsField);
            setupImages();
        }
    }

    public void setupImages() {
        noZeroImage.setUrl(getClockWiseImage(noZero.getValue()));
        oneZeroImage.setUrl(getClockAntiImage(oneZero.getValue()));
        twoZeroImage.setUrl(getClockWiseImage(twoZero.getValue()));
        threeZeroImage.setUrl(getClockAntiImage(threeZero.getValue()));
        fourZeroImage.setUrl(getClockWiseImage(fourZero.getValue()));
        fiveZeroImage.setUrl(getClockAntiImage(fiveZero.getValue()));
    }

    @Override
    public boolean isValid() {
        if (registerSkipCodeBox.getValue()) {
            fiveZero.removeStyleName("invalidField");
            fourZero.removeStyleName("invalidField");
            threeZero.removeStyleName("invalidField");
            twoZero.removeStyleName("invalidField");
            oneZero.removeStyleName("invalidField");
            noZero.removeStyleName("invalidField");
            boolean skipCodeChosen = registerSkipCodeList.getSelectedIndex() > 0;
            if (skipCodeChosen) {
                registerSkipCodeList.removeStyleName("invalidField");
                return true;
            } else {
                registerSkipCodeList.setTitle("If the register is unreadable, a reason must be specified");
                registerSkipCodeList.addStyleName("invalidField");
                return false;
            }
        } else {
            registerSkipCodeList.removeStyleName("invalidField");
            return isFieldValid(fiveZero) & isFieldValid(fourZero) & isFieldValid(threeZero)
                 & isFieldValid(twoZero) & isFieldValid(oneZero) & isFieldValid(noZero);
        }
    }


    @Override
    public MeterDTO getMeter() {
        RegisterDTO register = meter.getRegister(Register.REGISTER_DEFAULT.getCode());

        if (registerSkipCodeBox.getValue()) {
            register.setValue(null);
            int selectedIndex = registerSkipCodeList.getSelectedIndex();
            if (selectedIndex > -1) {
                register.setSkipCode(new SkipCodeDTO(registerSkipCodeList.getValue(selectedIndex), registerSkipCodeList.getItemText(selectedIndex)));
            } else {
                register.setSkipCode(null);
            }
        } else {
            register.setSkipCode(null);
            Integer fiveZeroValue = fiveZero.getValue();
            Integer fourZeroValue = fourZero.getValue();
            Integer threeZeroValue = threeZero.getValue();
            Integer twoZeroValue = twoZero.getValue();
            Integer oneZeroValue = oneZero.getValue();
            Integer noZeroValue = noZero.getValue();
            Integer value = 0;
            if ((fiveZeroValue != null) && (fourZeroValue != null) && (threeZeroValue != null) &&
                (twoZeroValue != null) && (oneZeroValue != null) && (noZeroValue != null)) {
                value = (100000* fiveZeroValue)
                      + (10000* fourZeroValue)
                      + (1000* threeZeroValue)
                      + (100* twoZeroValue)
                      + (10* oneZeroValue)
                      + (1* noZeroValue);
            }
            register.setValue(value);
        }

        assignValuesFromCommentToMeter(commentsField);

        return meter;
    }

    @Override
    public void disable(boolean disable) {
        fiveZero.setEnabled(!disable);
        fourZero.setEnabled(!disable);
        threeZero.setEnabled(!disable);
        twoZero.setEnabled(!disable);
        oneZero.setEnabled(!disable);
        noZero.setEnabled(!disable);
        registerSkipCodeList.setEnabled(!disable);
        registerSkipCodeBox.setEnabled(!disable);
    }

    @UiHandler("registerSkipCodeBox")
    public void onRegUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerSkipCodeBox.getValue();
        fiveZero.setEnabled(!unreadable);
        fourZero.setEnabled(!unreadable);
        threeZero.setEnabled(!unreadable);
        twoZero.setEnabled(!unreadable);
        oneZero.setEnabled(!unreadable);
        noZero.setEnabled(!unreadable);
        registerSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
              registerSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }
    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    @UiHandler("noZero")
    public void onKeyDownEventNoZero(KeyUpEvent event) {
        Integer value = noZero.getValue();
        noZeroImage.setUrl(getClockWiseImage(value));

    }

    @UiHandler("oneZero")
    public void onKeyDownEventOneZero(KeyUpEvent event) {
        Integer value = oneZero.getValue();
        oneZeroImage.setUrl(getClockAntiImage(value));
    }

    @UiHandler("twoZero")
    public void onKeyDownEventTwoZero(KeyUpEvent event) {
        Integer value = twoZero.getValue();
        twoZeroImage.setUrl(getClockWiseImage(value));
    }

    @UiHandler("threeZero")
    public void onKeyDownEventThreeZero(KeyUpEvent event) {
        Integer value = threeZero.getValue();
        threeZeroImage.setUrl(getClockAntiImage(value));
    }

    @UiHandler("fourZero")
    public void onKeyDownEventFourZero(KeyUpEvent event) {
        Integer value = fourZero.getValue();
        fourZeroImage.setUrl(getClockWiseImage(value));
    }

    @UiHandler("fiveZero")
    public void onKeyDownEventFiveZero(KeyUpEvent event) {
        Integer value = fiveZero.getValue();
        fiveZeroImage.setUrl(getClockAntiImage(value));
    }


    public enum Register {
        REGISTER_DEFAULT("007"),
        NOVALUE("");


        private String value;

        Register(String value) {
            this.value = value;
        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    if (value.equalsIgnoreCase(b.value)) {
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public String getCode() {
            return value;
        }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
         boolean valid = true;
        validateFieldRegister(validate,"noZero", noZero, registerSkipCodeBox, registerSkipCodeList, registerValidationError);
        validateFieldRegister(validate,"oneZero", oneZero, registerSkipCodeBox, registerSkipCodeList, registerValidationError);
        validateFieldRegister(validate,"twoZero", twoZero, registerSkipCodeBox, registerSkipCodeList, registerValidationError);
        validateFieldRegister(validate,"threeZero", threeZero, registerSkipCodeBox, registerSkipCodeList, registerValidationError);
        validateFieldRegister(validate,"fourZero", fourZero, registerSkipCodeBox, registerSkipCodeList, registerValidationError);
        validateFieldRegister(validate,"fiveZero", fiveZero, registerSkipCodeBox, registerSkipCodeList, registerValidationError);
        // A dummy integerbox for this
        validateListBoxRegister(validate,"fiveZero", threeZero, registerSkipCodeBox, registerSkipCodeList, registerValidationError);

        return valid;
    }
}
