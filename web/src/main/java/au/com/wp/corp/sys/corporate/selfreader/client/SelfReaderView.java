package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import java.util.List;

/**
 * The SelfReaderView and Presenter display the Meter screen and handle the display, and submission of meter readings.
 */
public interface SelfReaderView extends IsWidget {

    void hideShowingCalendar();

    public interface Presenter {
		void go(HasWidgets root);
        void displayMeter(MeterDTO meter);
        void show(boolean show);
        void clear();
        void submitMeter(MeterDTO meter, boolean overrideFlag);
        void setSkipCodes(List<SkipCodeDTO> skipCodes);
        void setArrivedByLink(boolean arrivedByLink);
        void setUploadedImageFileName(String fileName);
        void showUnsubmittableMeterWarningMessage();
        void showErrorInformation(String message);

        void displayHelp(String helpMsgHtml, Widget target);


        void showInformation(String message);
    }

    /**
     * Provides the view with the List of skip codes that a user can choose from to indicate why they haven't read
     * (and therefor provided a reading for) a register for their meter.
     *
     * @param skipCodes The @See{List} of skip codes that a user can select for a register.
     */
    void setSkipCodes(List<SkipCodeDTO> skipCodes);
	void setPresenter(Presenter presenter);

    /**
     * Shows the "Submitting" modal dialog.
     */
    void showSubmitting();

    /**
     * Hides the "Submitting" modal dialog.
     */
    void hideSubmitting();

    /**
     * Sets the meter that is to be displayed.
     *
     * @param meter The Meter DTO that is to be displayed.
     * @param displayOverride indicates whether the override checkbox should be displayed to the user.
     */
    void setMeter(MeterDTO meter, boolean displayOverride);
    void disableFields();

    /**
     * Shows the "Canned" success message to the user.
     */
    void showSuccessMessage();

    /**
     * Shows the "Canned" success with disclaimer message to the user.
     */
    void showSuccessWithDisclaimerMessage(String message);

    /**
     * Shows system failure message
     */
    void showFailureMessage(String message);

    /**
     * Shows register validation message
     */
    void showRegisterValidationMessage();

    /**
     * Display Error Message
     * @param message the message to display
     */
    void displayErrorMessage(String message);

    /**
     * Show/Hide the upload image file
     * @param visible
     */
    void showFileUpload(boolean visible);

    /**
     * Shows a warning message on the screen in a label telling user they can
     * submit but its not going to be billed
     */
    void showUnsubmittableMeterMessage();

    /**
     * Hides the warning message on the screen in a label
     */
    void hideWarningMessage();


    /**
     * Displays a warning message on the screen in a label
     * @param message the message to display to user
     */
    void showWarningMessage(String message);

    void showErrorInformation(String message);
}
