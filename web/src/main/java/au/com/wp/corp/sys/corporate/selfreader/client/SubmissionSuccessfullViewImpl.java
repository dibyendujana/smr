package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.SubmissionSuccessfullView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;


/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 19/03/12
 * Time: 7:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class SubmissionSuccessfullViewImpl extends Composite implements SubmissionSuccessfullView {
    private static SubmissionSuccessfullViewImplUiBinder uiBinder = GWT.create(SubmissionSuccessfullViewImplUiBinder.class);
    interface SubmissionSuccessfullViewImplUiBinder  extends UiBinder<Widget, SubmissionSuccessfullViewImpl> {
    }
    private Presenter presenter;
    @UiField
    Anchor logout;

    @Override
    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public SubmissionSuccessfullViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @UiHandler("logout")
    public void onLogoutClick(ClickEvent e) {
        GWT.log("Log out request");
        presenter.logout();
    }
}
