package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import com.google.gwt.user.client.ui.IsWidget;
import eu.maydu.gwt.validation.client.ValidationProcessor;

/**
 * This is the display part of a meter, representative of what they see on the card posted to them. It is comprised of
 * the register display section. Due to the dictated design, each meter is potentially significantly different.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public interface MeterWidget extends IsWidget {
    /**
     *
     * @return the MeterDTO with the currently entered register values.
     */
    MeterDTO getMeter();

    /**
     * Validates the register values to check that all compulsory values have been entered and that they are of a valid type.
     *
     * @return true if all fields are of a valid type and all compulsory fields are filled.
     */
    boolean isValid();

    /**
     * Validates the register values to check all compulsory values have been entered and that they are of a valid type
     *
     * @param validate the parent page validator
     * @return true if all fields are of a valid type and all compulsory fields are filled
     */
    boolean validateMeter(ValidationProcessor validate);

    /**
     * Disables all editable fields if disable is false, otherwise it enables all of the editable fields.
     * @param disable true if all editable fields should be disabled.
     */
    void disable(boolean disable);

    /** The text that appears in the comment field of the meter (if the meter contains one) when the field is empty and
     * does not have the focus
     */
    static final String COMMENT_FIELD_TEXT = "Please enter any comments here";

}
