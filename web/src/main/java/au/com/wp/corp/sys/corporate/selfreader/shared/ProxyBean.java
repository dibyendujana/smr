package au.com.wp.corp.sys.corporate.selfreader.shared;

/**
 Bean to represent a proxy host/port combo injected by Spring
 */
public class ProxyBean {
    String host;
    int port;

    public ProxyBean(){
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
