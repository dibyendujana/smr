package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.validation.NotNullRegisterIntegerBoxValidator;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.NotNullRegisterListBoxValidator;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.ValidationErrorWidgetAction;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterError;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.actions.StyleAction;

import java.util.List;

/**
 * A MeterWidget for the 7DL meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class SevenDLMeterWidget extends Composite implements MeterWidget {
    private ValidationProcessor validator;

    interface MyUiBinder extends UiBinder<HTMLPanel, SevenDLMeterWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);
    protected final String INVALID_FIELD_STYLE_NAME = "invalidField";

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox registerValue;
    @UiField
    CheckBox registerSkipCodeBox;
    @UiField
    ListBox registerSkipCodeList;
    @UiField
    Label registerPreviousValue;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    private MeterDTO meter;
    private List<SkipCodeDTO> skipCodes;

    public SevenDLMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, ValidationProcessor validator) {
        this.validator = validator;
        initWidget(uiBinder.createAndBindUi(this));
        this.meter = meter;
        this.skipCodes = skipCodes;
        processSkipCodes();

        if ((meter.getComment() == null) || ("".equals(meter.getComment().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        } else {
            commentsField.setText(meter.getComment());
        }
    }

    private void processSkipCodes() {
        addSkipCodesToListBox(registerSkipCodeList);
    }

    private void addSkipCodesToListBox(ListBox box) {
        box.addItem("Please select", "");
        for (SkipCodeDTO skipCode : skipCodes) {
            box.addItem(skipCode.getText(), skipCode.getCode());
        }
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(registerValue, registerSkipCodeBox, registerSkipCodeList);

        return valid;
    }

    private boolean validateRegister(IntegerBox value, CheckBox checkBox, ListBox listBox) {
       validator.reset();
        validator.removeValidatorsAndGlobalActions();

        validator.addValidators("val0", new NotNullRegisterIntegerBoxValidator(value, checkBox, listBox)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                 .addActionForFailure(new ValidationErrorWidgetAction(registerValidationError))
        );
        validator.addValidators("val1", new NotNullRegisterListBoxValidator(value, checkBox, listBox)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(registerValidationError))
        );
        return validator.validate();
    }

    @Override
    public MeterDTO getMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        assignValuesToRegister(registers.get(0), registerSkipCodeBox, registerValue, registerSkipCodeList);
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            meter.setComment("");
        } else {
            meter.setComment(commentsField.getText());
        }

        return meter;
    }

    private void assignValuesToRegister(RegisterDTO registerDTO, CheckBox skipCodeBox, IntegerBox value, ListBox skipCodeList) {
        if (!skipCodeBox.getValue()) {
            registerDTO.setValue(value.getValue());
            registerDTO.setSkipCode(null);
        } else {
            int selectedIndex = skipCodeList.getSelectedIndex();
            SkipCodeDTO skipCode = new SkipCodeDTO(skipCodeList.getValue(selectedIndex), skipCodeList.getItemText(selectedIndex));
            registerDTO.setSkipCode(skipCode);
        }

    }

    @Override
    public void disable(boolean disable) {
        registerValue.setEnabled(!disable);
        registerSkipCodeList.setEnabled(!disable);
        registerSkipCodeBox.setEnabled(!disable);
    }

    @UiHandler("registerSkipCodeBox")
    public void onRegUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerSkipCodeBox.getValue();
        registerValue.setEnabled(!unreadable);
        registerSkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            registerSkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }
    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        return isValid();
    }

    private void displayErrorDiv(String errorMessage, ValidationErrorWidget errorWidget) {
        errorWidget.setErrorMsg(errorMessage);
        errorWidget.setVisible(true);
    }
}
