package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.events.LogoutEvent;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.HelpPanel;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.MessageBox;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 19/03/12
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class SubmissionSuccessfullPresenter implements SubmissionSuccessfullView.Presenter {
    private final SubmissionSuccessfullView display;
    private final EventBus eventBus;
    private final MessageBox messageBox;


    @Inject
    public SubmissionSuccessfullPresenter(EventBus eventBus, SubmissionSuccessfullView display, MessageBox messageBox) {
        this.eventBus = eventBus;
        this.messageBox = messageBox;
        this.display = display;

        display.setPresenter(this);

    }

    @Override
    public void go(HasWidgets root) {
        Widget w = this.display.asWidget();
        root.add(w);
        w.setVisible(false);
    }

    @Override
    public void show(boolean show) {
        getDisplay().asWidget().setVisible(show);
    }

    @Override
    public void logout() {
        eventBus.fireEvent(new LogoutEvent());
    }

    public SubmissionSuccessfullView getDisplay() {
        return display;
    }
}
