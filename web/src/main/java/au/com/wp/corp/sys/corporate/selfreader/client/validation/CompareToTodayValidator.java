package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

import java.util.Date;

/**
 * Custom Validator for Datebox to do some date comparisons with date today
 */
public class CompareToTodayValidator extends Validator<CompareToTodayValidator> {


    private final DateBox dateBox;
    private final BeforeOrAfterTodaysDate comparitor;

    public enum BeforeOrAfterTodaysDate {BEFORE, AFTER}

    ;

    /**
     * Constructor
     *
     * @param dateBox    - the date being validated
     * @param comparitor
     */
    public CompareToTodayValidator(DateBox dateBox, BeforeOrAfterTodaysDate comparitor) {

        this.dateBox = dateBox;
        this.comparitor = comparitor;
    }


    /**
     * Method that performs the actual validation logic
     *
     * @param messages
     * @param <V>
     * @return
     */
    @Override
    public <V extends ValidationMessages> ValidationResult validate(V messages) {
        Date today = new Date(System.currentTimeMillis());
        SelfreaderValidationMessages msgs = (SelfreaderValidationMessages) messages.getStandardMessages();
        String msg = null;

        boolean valid = true;

        switch (comparitor) {

            case BEFORE:
                if (dateBox.getValue() != null && dateBox.getValue().before(today)) {
                    valid = false;
                    msg = msgs.dateInThePast();
                }

                break;

            case AFTER:
                if (dateBox.getValue() != null && dateBox.getValue().after(today)) {
                    valid = false;
                    msg = msgs.dateInTheFuture();
                }

                break;

            default:

        }

        if (!valid)
            return new ValidationResult(getErrorMessage(messages, msg));


        return null;
    }

    /**
     * Method that invokes the actions that are to be performed
     * if the validate method returns a non null value (indicating)
     * validation has failed)
     *
     * @param validationResult
     */
    @Override
    public void invokeActions(ValidationResult validationResult) {

        for (ValidationAction<TextBox> va : this.getFailureActions())
            va.invoke(validationResult, dateBox.getTextBox());
    }


}
