package au.com.wp.corp.sys.corporate.selfreader.client;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 19/03/12
 * Time: 7:34 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SubmissionSuccessfullView extends IsWidget {
    public interface Presenter {
        void go(HasWidgets root);
        void show(boolean show);
        void logout();
    }

    void setPresenter(Presenter presenter);
}
