package au.com.wp.corp.sys.corporate.selfreader.client;

import au.com.wp.corp.sys.corporate.selfreader.client.events.*;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.MessageBox;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.inject.Inject;

import java.util.List;

public class RootPresenter implements RootView.Presenter, MeterSelectedEvent.Handler, SkipCodesFoundEvent.Handler, SubmissionSuccessfulEvent.Handler, LogoutEvent.Handler {
    private final EventBus eventBus;
    private final SelfReaderServiceAsync serviceAsync;
    private final RootView display;
    private final LoginView.Presenter loginPresenter;
    private final SelfReaderView.Presenter selfReaderPresenter;
    private final SubmissionSuccessfullView.Presenter submissionSuccessfullPresenter;
    private final MessageBox messageBox;


    @Inject
    public RootPresenter(EventBus eventBus, SelfReaderServiceAsync serviceAsync, RootView display, LoginView.Presenter loginPresenter, SelfReaderView.Presenter selfReaderPresenter, SubmissionSuccessfullView.Presenter submissionSuccessfullPresenter, MessageBox messageBox) {
        this.eventBus = eventBus;
        this.serviceAsync = serviceAsync;
        this.display = display;
        display.setPresenter(this);
        this.loginPresenter = loginPresenter;
        this.selfReaderPresenter = selfReaderPresenter;
        this.submissionSuccessfullPresenter = submissionSuccessfullPresenter;
        this.messageBox = messageBox;
    }

    @Override
    public void setNmiAndMeter(String nmi, String meterNumber) {
        // Need to set arrived by link on other presenter as we probably got here from an email
        loginPresenter.setNmiAndMeter(nmi, meterNumber);
        selfReaderPresenter.setArrivedByLink(true);
    }

    @Override
    public void onScrollEvent() {
        eventBus.fireEvent(new HidePopupEvent());
    }

    @Override
    public void go(HasWidgets root) {
        root.add(this.display.asWidget());
        eventBus.addHandler(MeterSelectedEvent.TYPE, this);
        eventBus.addHandler(SkipCodesFoundEvent.TYPE, this);
        eventBus.addHandler(SubmissionSuccessfulEvent.TYPE, this);
        eventBus.addHandler(LogoutEvent.TYPE, this);
        loginPresenter.go(this.display.getContentPanel());
        selfReaderPresenter.go(this.display.getContentPanel());
        submissionSuccessfullPresenter.go(this.display.getContentPanel());

        loginPresenter.show(true);

        // TODO - IS remove below 2 lines
        //loginPresenter.show(false);
        //submissionSuccessfullPresenter.show(true);
        // IS - end remove
    }

    public RootView getDisplay() {
        return display;
    }

    @Override
    public void onMeterSelected(MeterDTO meter) {
        GWT.log("inside onMeterSelected - " + meter.getMeterNumber());
        if ((meter.getSubmittable() == null) || (!meter.getSubmittable())) {
            // display a warning message before allowing the user to continue that the meter is not submittable (eg outside it's viewing window), and submissions may be ignored.
            messageBox.showWarningInformation("This meter is outside its reading window. You can submit a reading, however it may not be used for billing purposes.");
            selfReaderPresenter.showUnsubmittableMeterWarningMessage();
        }
        loginPresenter.show(false);
        submissionSuccessfullPresenter.show(false);
        getDisplay().showLogoutButton(true);
        selfReaderPresenter.show(true);
        selfReaderPresenter.displayMeter(meter);

    }

    @Override
    public void onSkipCodesFound(List<SkipCodeDTO> skipCodes) {
        GWT.log("inside onSkipCodesFound - " + skipCodes.size());
        selfReaderPresenter.setSkipCodes(skipCodes);
    }

    @Override
    public void logout() {
        selfReaderPresenter.show(false);
        getDisplay().showLogoutButton(false);
        loginPresenter.show(true);
        submissionSuccessfullPresenter.show(false);
        selfReaderPresenter.clear();
        loginPresenter.resetCaptcha();
    }

    @Override
    public void onSubmissionSuccessfull() {
        selfReaderPresenter.show(false);
        loginPresenter.show(false);
        submissionSuccessfullPresenter.show(true);
    }

    @Override
    public void onLogout() {
        // Call default logout method
        logout();
    }


}
