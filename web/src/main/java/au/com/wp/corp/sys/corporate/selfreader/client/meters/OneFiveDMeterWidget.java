package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.utils.HelpPanel;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.Arrays;
import java.util.List;

/**
 * A MeterWidget for the 15D meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class OneFiveDMeterWidget extends AbstractMeterWidget {

    interface MyUiBinder extends UiBinder<HTMLPanel, OneFiveDMeterWidget> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox registerValue;
    @UiField
    CheckBox registerSkipCodeBox;
    @UiField
    ListBox registerSkipCodeList;
    @UiField
    Label registerPreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;


    public OneFiveDMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter, validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_DEFAULT:
                    assignValuesFromRegister(register, registerSkipCodeBox, registerPreviousValue, registerValue, registerSkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);
    }


    protected void processSkipCodes() {
        addSkipCodesToListBox(registerSkipCodeList);
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(registerValue, registerSkipCodeBox, registerSkipCodeList);

        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_DEFAULT.getCode(), registerSkipCodeBox, registerValue, registerSkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }


    @Override
    public void disable(boolean disable) {
        registerValue.setEnabled(!disable);
        registerSkipCodeList.setEnabled(!disable);
        registerSkipCodeBox.setEnabled(!disable);
    }

    @UiHandler("registerSkipCodeBox")
    public void onRegUnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = registerSkipCodeBox.getValue();
        registerValue.setEnabled(!unreadable);
        registerSkipCodeList.setEnabled(unreadable);
        if (!unreadable) {
            registerSkipCodeBox.removeStyleName("validationFailedBorder");
            registerSkipCodeList.setSelectedIndex(0);
        }
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }

    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_DEFAULT("007,07"),
        NOVALUE("");


        private String value;

        Register(String value) {
            this.value = value;
        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    String[] values = b.value.split(",");
                    List<String> valueSet = Arrays.asList(values);
                    if (valueSet.contains(value)) {
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public List<String> getCode() {
            return Arrays.asList(value.split(","));
        }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        //user first instance of list of codes for validation
        valid = valid & validateRegister(validate, Register.REGISTER_DEFAULT.getCode().get(0), registerValue, registerSkipCodeBox, registerSkipCodeList, registerValidationError);
        return valid;
    }
}
