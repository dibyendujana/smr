package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.UIObject;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;

import java.util.HashMap;
import java.util.Map;

/**
 * Custom Action class to be used to show NsaPopupDescription objects
 */
public class PopupPanelAction extends ValidationAction<UIObject> {

    Map<UIObject, SelfreaderPopupDescription> descriptionsMap;

    /**
     * Constructor
     */

    private Position position;

    public enum Position {BELOW, SIDE}

    ;

    public PopupPanelAction(Position panelPosition) {
        this.position = panelPosition;
    }

    public PopupPanelAction() {
        descriptionsMap = new HashMap<UIObject, SelfreaderPopupDescription>();
        this.position = Position.BELOW;
    }


    @Override
    public void invoke(ValidationResult validationResult, UIObject uiObject) {
        SelfreaderPopupDescription description = null;

        //if called from the server side
        if (descriptionsMap == null) {
            descriptionsMap = new HashMap<UIObject, SelfreaderPopupDescription>();
        }

        if (descriptionsMap.containsKey(uiObject)) {
            description = descriptionsMap.get(uiObject);
        } else {
            description = new SelfreaderPopupDescription(position);
            descriptionsMap.put(uiObject, description);
        }

        ValidationResult.ValidationError error = validationResult.getErrors().get(0);

        //bit of a hack - pass in the actual error instead of a key
        //saves us having to redefine the logic in NsaValidationMessagesImpl.getDescriptionMessage

        description.addDescription(error.error, (Focusable) uiObject);

    }

    @Override
    public void reset(UIObject obj) {
        if (obj != null) {
            obj.setTitle("");
        }
    }

    @Override
    public void reset() {
        if (descriptionsMap != null) {
            for (UIObject uiObject : descriptionsMap.keySet()) {
                SelfreaderPopupDescription description = descriptionsMap.get(uiObject);
                description.removeAll();
            }
        }
        descriptionsMap = null;
    }

}
