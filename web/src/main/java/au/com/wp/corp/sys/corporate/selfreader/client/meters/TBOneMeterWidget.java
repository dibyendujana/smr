package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.validation.NotNullRegisterIntegerBoxValidator;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.NotNullRegisterListBoxValidator;
import au.com.wp.corp.sys.corporate.selfreader.client.validation.ValidationErrorWidgetAction;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterError;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.actions.StyleAction;

import java.util.List;

/**
 * A MeterWidget for the TB1 meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class TBOneMeterWidget extends Composite implements MeterWidget {
    private ValidationProcessor validator;
    protected final String INVALID_FIELD_STYLE_NAME = "invalidField";
    interface MyUiBinder extends UiBinder<HTMLPanel, TBOneMeterWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register3Value;
    @UiField
    CheckBox register3SkipCodeBox;
    @UiField
    ListBox register3SkipCodeList;
    @UiField
    IntegerBox register4Value;
    @UiField
    CheckBox register4SkipCodeBox;
    @UiField
    ListBox register4SkipCodeList;
    @UiField
    IntegerBox register6Value;
    @UiField
    CheckBox register6SkipCodeBox;
    @UiField
    ListBox register6SkipCodeList;
    @UiField
    Label register3PreviousValue;
    @UiField
    Label register4PreviousValue;
    @UiField
    Label register6PreviousValue;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    private MeterDTO meter;
    private List<SkipCodeDTO> skipCodes;

    public TBOneMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, ValidationProcessor validator, boolean multiRegister) {
        this.validator = validator;
        initWidget(uiBinder.createAndBindUi(this));
        this.meter = meter;
        this.skipCodes = skipCodes;
        processSkipCodes();

        if ((meter.getComment() == null) || ("".equals(meter.getComment().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        } else {
            commentsField.setText(meter.getComment());
        }
    }

    private void processSkipCodes() {
        addSkipCodesToListBox(register3SkipCodeList);
        addSkipCodesToListBox(register4SkipCodeList);
        addSkipCodesToListBox(register6SkipCodeList);
    }

    private void addSkipCodesToListBox(ListBox box) {
        box.addItem("Please select", "");
        for (SkipCodeDTO skipCode : skipCodes) {
            box.addItem(skipCode.getText(), skipCode.getCode());
        }
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register3Value, register3SkipCodeBox, register3SkipCodeList);
        valid = valid & validateRegister(register4Value, register4SkipCodeBox, register4SkipCodeList);
        valid = valid & validateRegister(register6Value, register6SkipCodeBox, register6SkipCodeList);

        return valid;
    }

    private boolean validateRegister(IntegerBox value, CheckBox checkBox, ListBox listBox) {
        validator.reset();
        validator.removeValidatorsAndGlobalActions();

        validator.addValidators("val0", new NotNullRegisterIntegerBoxValidator(value, checkBox, listBox)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                 .addActionForFailure(new ValidationErrorWidgetAction(registerValidationError))
        );
        validator.addValidators("val1", new NotNullRegisterListBoxValidator(value, checkBox, listBox)
                .addActionForFailure(new StyleAction("validationFailedBorder"))
                .addActionForFailure(new ValidationErrorWidgetAction(registerValidationError))
        );
        return validator.validate();
    }

    @Override
    public MeterDTO getMeter() {
        // TODO change this to use the register type and not it's position.
        List<RegisterDTO> registers = meter.getRegisters();
        assignValuesToRegister(registers.get(0), register3SkipCodeBox, register3Value, register3SkipCodeList);
        assignValuesToRegister(registers.get(1), register4SkipCodeBox, register4Value, register4SkipCodeList);
        assignValuesToRegister(registers.get(2), register6SkipCodeBox, register6Value, register6SkipCodeList);
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            meter.setComment("");
        } else {
            meter.setComment(commentsField.getText());
        }

        return meter;
    }

    private void assignValuesToRegister(RegisterDTO registerDTO, CheckBox skipCodeBox, IntegerBox value, ListBox skipCodeList) {
        if (!skipCodeBox.getValue()) {
            registerDTO.setValue(value.getValue());
            registerDTO.setSkipCode(null);
        } else {
            int selectedIndex = skipCodeList.getSelectedIndex();
            SkipCodeDTO skipCode = new SkipCodeDTO(skipCodeList.getValue(selectedIndex), skipCodeList.getItemText(selectedIndex));
            registerDTO.setSkipCode(skipCode);
        }

    }

    @Override
    public void disable(boolean disable) {
        register3Value.setEnabled(!disable);
        register3SkipCodeList.setEnabled(!disable);
        register3SkipCodeBox.setEnabled(!disable);
        register4Value.setEnabled(!disable);
        register4SkipCodeList.setEnabled(!disable);
        register4SkipCodeBox.setEnabled(!disable);
        register6Value.setEnabled(!disable);
        register6SkipCodeList.setEnabled(!disable);
        register6SkipCodeBox.setEnabled(!disable);

    }

    @UiHandler("register3SkipCodeBox")
    public void onReg3UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register3SkipCodeBox.getValue();
        register3Value.setEnabled(!unreadable);
        register3SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
              register3SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register4SkipCodeBox")
    public void onReg4UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register4SkipCodeBox.getValue();
        register4Value.setEnabled(!unreadable);
        register4SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
              register4SkipCodeList.setSelectedIndex(0);

    }
    @UiHandler("register6SkipCodeBox")
    public void onReg6UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register6SkipCodeBox.getValue();
        register6Value.setEnabled(!unreadable);
        register6SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
              register6SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }
    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        return isValid();
    }

    private void displayErrorDiv(String errorMessage, ValidationErrorWidget errorWidget) {
        errorWidget.setErrorMsg(errorMessage);
        errorWidget.setVisible(true);
    }
}
