package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterError;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.ui.HasValue;
import eu.maydu.gwt.validation.client.ValidationAction;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

/**
 * Custom Validator for checking returned register reading errors from server
 */
public class ServerRegisterValidator extends Validator<ServerRegisterValidator> {

    private final HasValue<?> hasValue;
    private final RegisterError error;


    /**
     * Constructor
     *
     * @param hasValue
     * @param error
     */
    public ServerRegisterValidator(HasValue<?> hasValue, RegisterError error) {
        this.hasValue = hasValue;
        this.error = error;
    }

    /**
     * Checks that the 2 email fields match and
     *
     * @param messages
     * @return
     */
    @Override
    public <V extends ValidationMessages> ValidationResult validate(V messages) {

        SelfreaderValidationMessages msgs = (SelfreaderValidationMessages) messages.getStandardMessages();

        if (error!=null && error.getMessage()!=null) {
            return new ValidationResult(error.getMessage());
        }

        return null;
    }

    /**
     * Method that invokes the actions that are to be performed
     * if the validate method returns a non null value (indicating)
     * validation has failed)
     *
     * @param validationResult
     */
    @Override
    public void invokeActions(ValidationResult validationResult) {
        for (ValidationAction va : this.getFailureActions())
            va.invoke(validationResult, hasValue);
    }
}
