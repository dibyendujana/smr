package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.Arrays;
import java.util.List;

/**
 * A MeterWidget for the RB3 meter.
 *
 * @author Noel Bannon
 */
public class LZRMeterWidget extends AbstractMeterWidget {

    interface MyUiBinder extends UiBinder<HTMLPanel, LZRMeterWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register10Value;
    @UiField
    CheckBox register10SkipCodeBox;
    @UiField
    ListBox register10SkipCodeList;
    @UiField
    Label register10PreviousValue;
    @UiField
    IntegerBox register20Value;
    @UiField
    CheckBox register20SkipCodeBox;
    @UiField
    ListBox register20SkipCodeList;
    @UiField
    Label register20PreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget serverError;
    @UiField
    ValidationErrorWidget registerValidationError;

    public LZRMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter,validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {
        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_10:
                    assignValuesFromRegister(register, register10SkipCodeBox, register10PreviousValue, register10Value, register10SkipCodeList,serverError);
                    break;
                case REGISTER_20:
                    assignValuesFromRegister(register, register20SkipCodeBox, register20PreviousValue, register20Value, register20SkipCodeList,serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);
    }

    protected void processSkipCodes() {
        addSkipCodesToListBox(register10SkipCodeList);
        addSkipCodesToListBox(register20SkipCodeList);
    }

    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register10Value, register10SkipCodeBox, register10SkipCodeList);
        valid = valid & validateRegister(register20Value, register20SkipCodeBox, register20SkipCodeList);

        return valid;
    }


    @Override
    public MeterDTO getMeter() {
        assignValuesToRegisterAndCreate(Register.REGISTER_10.getCode(), register10SkipCodeBox, register10Value, register10SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_20.getCode(), register20SkipCodeBox, register20Value, register20SkipCodeList);
        assignValuesFromCommentToMeter(commentsField);
        return meter;
    }


    @Override
    public void disable(boolean disable) {
        register10Value.setEnabled(!disable);
        register10SkipCodeList.setEnabled(!disable);
        register10SkipCodeBox.setEnabled(!disable);
        register20Value.setEnabled(!disable);
        register20SkipCodeList.setEnabled(!disable);
        register20SkipCodeBox.setEnabled(!disable);
    }

    @UiHandler("register10SkipCodeBox")
    public void onReg7UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register10SkipCodeBox.getValue();
        register10Value.setEnabled(!unreadable);
        register10SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register10SkipCodeList.setSelectedIndex(0);
    }
    @UiHandler("register20SkipCodeBox")
    public void onReg107UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register20SkipCodeBox.getValue();
        register20Value.setEnabled(!unreadable);
        register20SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
               register20SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }
    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_10("010,10"),
        REGISTER_20("020,20"),
        NOVALUE("");

        private String value;

        Register(String value) {
            this.value=value;
        }

        /**
         * If we cant find the specific register we return novalue
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    String[] values = b.value.split(",");
                    List<String> valueSet = Arrays.asList(values);
                    if(valueSet.contains(value)){
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public List<String> getCode() {
                    return Arrays.asList(value.split(","));
                }

    }


    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        valid = valid & validateRegister(validate, Register.REGISTER_10.getCode().get(0), register10Value, register10SkipCodeBox, register10SkipCodeList,registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_20.getCode().get(0), register20Value, register20SkipCodeBox, register20SkipCodeList,registerValidationError);

        return valid;
    }

}
