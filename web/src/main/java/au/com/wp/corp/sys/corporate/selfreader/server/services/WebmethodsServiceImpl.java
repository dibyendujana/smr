package au.com.wp.corp.sys.corporate.selfreader.server.services;

import au.com.westernpower.wp.mr.wsp.meterreadingsservices.MeterReadingsServices;
import au.com.westernpower.wp.mr.wsp.meterreadingsservices.MeterReadingsServicesPortType;

import javax.xml.ws.BindingProvider;
import java.util.Map;

public class WebmethodsServiceImpl implements WebmethodsService {

    private String serviceLocation;

    public WebmethodsServiceImpl(String serviceLocation) {
        this.serviceLocation = serviceLocation;
    }

    @Override
    public MeterReadingsServicesPortType getMeterReadingsServicesPort() {
        MeterReadingsServices service = new MeterReadingsServices();
        MeterReadingsServicesPortType port = service.getWpMrWspMeterReadingsServicesPort();
        setEndpointForService((BindingProvider) port);
        return port;
    }

    private void setEndpointForService(BindingProvider port) {
        Map<String, Object> context = port.getRequestContext();
        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceLocation);
    }

}
