package au.com.wp.corp.sys.corporate.selfreader.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * GWT RPC Service to log client side exceptions
 *
 * @author Thomas Buckel (n041982)
 */
@RemoteServiceRelativePath("logService")
public interface LoggingService extends RemoteService {

    void logUncaughtException(String message, String exceptionClassName, StackTraceElement[] stackTrace);

}
