package au.com.wp.corp.sys.corporate.selfreader.server.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * User: N041844
 * Date: 2/12/11
 * Time: 9:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class AuthenticationWrapperImpl implements AuthenticationWrapper {

    private final HttpServletRequest httpServletRequest;

    public AuthenticationWrapperImpl(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    @Override
    public String getRemoteUser() {
        return httpServletRequest.getRemoteUser();
    }

    @Override
    public boolean isUserInRole(String roleName) {
        return httpServletRequest.isUserInRole(roleName);
    }
}

