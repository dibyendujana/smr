package au.com.wp.corp.sys.corporate.selfreader.shared.dto;

/**
 * This identifies what type of reading the reading was. Eg, was it web supplied, a Western Power meter reader, or an estimation...
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public enum ReadingType {
    //TODO find out what types of readings there are, and how to determine this from the webservice, and then implement.
}
