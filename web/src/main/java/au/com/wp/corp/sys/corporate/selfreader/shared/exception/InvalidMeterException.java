package au.com.wp.corp.sys.corporate.selfreader.shared.exception;

import java.io.Serializable;

/**
 * Indicates that the Meter is not valid (eg configuration problem such as missing mandatory information such as no registers).
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class InvalidMeterException extends Exception implements Serializable {

    public InvalidMeterException() {
    }
}
