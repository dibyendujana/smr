package au.com.wp.corp.sys.corporate.selfreader.client.events;

import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 20/03/12
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class LogoutEvent extends GwtEvent<LogoutEvent.Handler> {
    public static final Type<LogoutEvent.Handler> TYPE = new Type<LogoutEvent.Handler>();

    public interface Handler extends EventHandler {
            void onLogout();
        }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onLogout();
    }


}
