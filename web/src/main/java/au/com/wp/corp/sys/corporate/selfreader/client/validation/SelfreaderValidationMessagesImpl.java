package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import com.google.gwt.core.client.GWT;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 5/06/12
 * Time: 9:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class SelfreaderValidationMessagesImpl extends ValidationMessages {
    private SelfreaderValidationMessages messages;


    /**
     * Creates a new object which uses the the messages we provide to override
     * the standard messages provided.
     */

    public SelfreaderValidationMessagesImpl() {
        messages = GWT.create(SelfreaderValidationMessages.class);
    }


      public String getCustomMessage(String key, Object... parameters) {

        if (key.equals("meterNumber")) {
            return messages.meterNumber();
        } else if (key.equals("nmiNumber")){
            return messages.nmiNumber();
        } else if (key.equals("notEmpty")) {
            return messages.notEmpty();
        } else if (key.equals("dateInThePast")) {
            return messages.dateInThePast();
        } else if (key.equals("dateInTheFuture")) {
            return messages.dateInTheFuture();
        } else if(key.equals("captcha")){
            return messages.captchaField();
        } else if(key.equals("checkSumField")){
            return messages.checkSumField();
        }

        //return "no message for key: " + msgKey;
        return null;

    }

     /**
     * Override if you are using the description functionality for describing a field.
     *
     * @param msgKey
     * @return The message associated with the key
     */
    public String getDescriptionMessage(String msgKey) {

        if (msgKey.equals("meterNumber")) {
            return messages.meterNumber();
        } else if (msgKey.equals("nmiNumber")){
            return messages.nmiNumber();
        } else if (msgKey.equals("notEmpty")) {
            return messages.notEmpty();
        }

        //return "no message for key: " + msgKey;
        return null;
    }


      /**
     * Returns the standard validation messages.
     *
     * @return
     */
    @Override
    public SelfreaderValidationMessages getStandardMessages() {
        return messages;
    }
}
