package au.com.wp.corp.sys.corporate.selfreader.client.utils;

import com.google.gwt.user.client.ui.Widget;

/**
 * Help Panel to display help text relative to a calling widget
 * @author Isaac Stephens <isaac.stephens@westernpower.com.au>
 */
public interface HelpPanel {
    /**
     * Displays help message
     *
     * @param alert         the message to display
     * @param callingWidget the widget to display stuff relative to
     */
    void show(String alert, Widget callingWidget);

    /**
     * Hide the help panel
     */
    void hide();
}
