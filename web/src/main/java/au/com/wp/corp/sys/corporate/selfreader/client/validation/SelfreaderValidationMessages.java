package au.com.wp.corp.sys.corporate.selfreader.client.validation;

import com.google.gwt.i18n.client.Messages;
import eu.maydu.gwt.validation.client.i18n.StandardValidationMessages;

/**
 * Created by IntelliJ IDEA.
 * User: n020629
 * Date: 5/06/12
 * Time: 9:47 AM
 * To change this template use File | Settings | File Templates.
 */
public interface SelfreaderValidationMessages extends StandardValidationMessages, Messages {
    String notEmpty();

    String nmiNumber();

    String meterNumber();

    String dateInThePast();

    String dateInTheFuture();

    String dateIsBeforeGivenDate(String widget);

    String dateIsAfterGivenDate(String widget);

    String notEmptyIntegerBoxRegister();

    String notEmptyListBoxRegister();

    String captchaField();

    String checkSumField();

    String notNegativeIntegerBoxRegister();
}
