package au.com.wp.corp.sys.corporate.selfreader.server.handler;

import au.com.westernpower.wp.mr.wsp.meterreadingsservices.*;
import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderService;
import au.com.wp.corp.sys.corporate.selfreader.server.services.SkipCodeService;
import au.com.wp.corp.sys.corporate.selfreader.server.util.HttpServletRequestHolder;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.*;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.*;
import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaException;
import net.tanesha.recaptcha.ReCaptchaResponse;
import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;
import org.apache.log4j.Logger;
import org.gwtwidgets.server.spring.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class SelfReaderServiceImpl implements SelfReaderService {
    private final Logger log = Logger.getLogger(SelfReaderServiceImpl.class);

    private static final String RESPONSE_OK = "0";
    private static final String RESPONSE_SUCCESS_CANT_BE_USED_FOR_BILLING = "3";
    private static final String RESPONSE_INVALID_NMI_METER_COMBINATION = "4";
    private static final String RESPONSE_INVALID_METER_DATA_FOR_METER_DISPLAY_TYPE = "5";

    private static final String SUBMIT_RESPONSE_OK = "0";

    @Autowired
    SkipCodeService skipCodeService;

    @Autowired
    SelfReaderServiceHelper selfReaderServiceHelper;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    MeterReadingsServicesPortType meterReadingsServices;

    @Autowired
    HttpServletRequestHolder httpServletRequestHolder;


    private static JAXBContext submitJaxbContext;

    @Autowired
    ReCaptcha reCaptcha;

    static {
        try {
            submitJaxbContext = JAXBContext.newInstance("au.com.westernpower.wp.mr.wsp.meterreadingsservices");
        } catch (JAXBException e) {
            //consume exception
        }
    }

    @Override
    public InitialResultDTO getMeterDetails(String nmi, String meterNumber, String challenge, String response, boolean arrivedByLink, String browser) throws FailedCaptchaValidationException, InvalidMeterException, InvalidNMIException, WebmethodsException, InvalidMeterDataForDisplayTypeException, MeterDetailsMismatchException {
        checkReCaptcha(challenge, response);

        // audit the request
        log.info("Meter request received. nmi=" + nmi + "  meterNumber=" + meterNumber);
        if (!nmiIsValid(nmi)) {
            throw new InvalidNMIException();
        }

        GetMeterReferenceDataInput getMeterReferenceDataInput = selfReaderServiceHelper.mapMeterReferenceDataFromDTO(nmi, meterNumber, arrivedByLink, browser);
        GetMeterReferenceDataOutput meterReferenceDataOutput = null;
        try {
            meterReferenceDataOutput = meterReadingsServices.getMeterReferenceData(getMeterReferenceDataInput);
        } catch (Exception e) {
            log.error("Error retrieving meter from webservice", e);
            throw new WebmethodsException("Error retrieving meter from webservice");
        }

        Boolean submittable = true;

        MeterDTO meter = null;
        if ((meterReferenceDataOutput != null) && (meterReferenceDataOutput.getGetMeterReferenceDataResponse() != null)) {
            GetMeterReferenceDataResponse meterReferenceDataResponse = meterReferenceDataOutput.getGetMeterReferenceDataResponse();
            // check the header to verify that everything is a-ok.
            String responseStatusCode = selfReaderServiceHelper.getRetrieveResponseStatusCode(meterReferenceDataOutput);
            if (RESPONSE_INVALID_NMI_METER_COMBINATION.equals(responseStatusCode)) { // 4. NMI/Meter combination not found in Self Reader listing
                throw new InvalidNMIException();
            } else if (RESPONSE_SUCCESS_CANT_BE_USED_FOR_BILLING.equals(responseStatusCode)) {
                // Cant be used for billing
                submittable = false;
            } else if (RESPONSE_INVALID_METER_DATA_FOR_METER_DISPLAY_TYPE.equals(responseStatusCode)) {
                throw new InvalidMeterDataForDisplayTypeException();
            } else if (!RESPONSE_OK.equals(responseStatusCode)) {
                log.warn("webservice call getMeterReferenceData has returned a responseStatusCode of " + responseStatusCode);
                throw new WebmethodsException("Error retrieving meter from Webservice");
            }
            meter = selfReaderServiceHelper.mapMeterToDTO(meterReferenceDataResponse);
        }

        if (meter == null) {
            throw new InvalidNMIException();
        }

        if (!meter.getMeterNumber().equals(meterNumber)) {
            String msg = "Meter details returned by Web Service do not match the details entered by you.";
            log.error(msg);
            throw new MeterDetailsMismatchException(msg);
        }

        // Set whether Meter is submittable or not
        meter.setSubmittable(submittable);


        List<SkipCodeDTO> skipCodes = skipCodeService.getSkipCodes();

        InitialResultDTO dto = new InitialResultDTO(meter, skipCodes);
        return dto;
    }

    /**
     * Returns true if the NMI is a valid NMI
     *
     * @param nmi the nmi including the check digit to check
     * @return true if the checkdigit matches the nmi
     */
    private boolean nmiIsValid(String nmi) {
        boolean valid = true;
        if ((nmi != null) && (nmi.trim().length() == 11)) {
            int[] nmiNumbers = new int[10];
            try {

                StringBuffer buffer = new StringBuffer();
                buffer.append(nmi.substring(0, 10));

                int checkBit = Integer.parseInt(nmi.substring(10));
                valid = (checkBit == getChecksum(buffer));
            } catch (NumberFormatException e) {
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Algorithm
     * 1.Double the ASCII value of alternate digits within the NMI beginning with the right-most digit.
     * 2.Add the individual digits comprising the products obtained in step 1 to each of the unaffected ASCII value digits in the original number.
     * 3.Find the next highest multiple of 10.
     * 4.The check digit is the value obtained in step 2 subtracted from the value obtained in step 3. If the result of this subtraction is 10 then the check digit is 0.
     * <p/>
     * Note: I ripped this code from the project mbxw_internet where we also validate nmi's
     *
     * @param nmi the nmi excluding the check digit
     * @return the check digit for this nmi
     */

    private int getChecksum(StringBuffer nmi) {
        int v = 0;
        boolean multiply = true;
        for (int i = nmi.length(); i > 0; i--) {
            int d = (int) nmi.charAt(i - 1);
            if (multiply) {
                d *= 2;
            }
            multiply = !multiply;
            while (d > 0) {
                v += d % 10;
                d /= 10;
            }
        }
        int luhn = (10 - (v % 10)) % 10;
        return luhn;
    }

    public void checkReCaptcha(String challenge, String response) throws FailedCaptchaValidationException {
        HttpServletRequest request = httpServletRequestHolder.getHttpServletRequest();
        String url = request.getRemoteAddr();
       // String url = ServletUtils.getRequest().getRemoteAddr();
       // httpServletRequestHolder.setHttpServletRequest(ServletUtils.getRequest());
       // String url = httpServletRequestHolder.getHttpServletRequest().getRemoteAddr();

        ReCaptchaResponse result = reCaptcha.checkAnswer(url, challenge, response);

        if (!result.isValid()) {
            if (result.getErrorMessage().equalsIgnoreCase("recaptcha-not-reachable"))
                throw new FailedCaptchaValidationException("The recaptcha service is not reachable.  Please try again later");
            else if (result.getErrorMessage().equalsIgnoreCase("incorrect-captcha-sol"))
                throw new FailedCaptchaValidationException("The words you typed don't seem to match what we expect. Please try again.");
            throw new FailedCaptchaValidationException();
        }
    }

    @Override
    public MeterDTO submitMeterDetails(MeterDTO meter, boolean overrideFlag, int attemptNumber, String source, String uploadImageFileName) throws SubmittedWithWarningException, RegisterSubmissionFailedException, WebmethodsException, InvalidImageFileException {
        log.debug("inside submitMeterDetails(overrideFlag: " + overrideFlag + ",attemptNumber: " + attemptNumber + ", uploadImageFilename: " + uploadImageFileName);

        String meterNo = meter.getMeterNumber();

        Boolean forceSubmission = isSubmissionToBeForced(attemptNumber, overrideFlag);

        if (log.isDebugEnabled()) {
            log.debug("Force Submission: " + forceSubmission);
        }

        SubmitReadingInput submitReadingInput = null;
        try {
            submitReadingInput = selfReaderServiceHelper.mapParametersToSubmitReadingInput(meter, source, attemptNumber, forceSubmission, uploadImageFileName);
        } catch (InvalidImageFileException e) {
            log.error("The image file is not valid", e);
            throw new InvalidImageFileException("The image file is not valid");
          }
          catch (Exception e) {
            log.error("Exception translating input to webservice", e);
            throw new WebmethodsException("An error occurred translating input into the webservice");
          }

        log.info("SUBMITTING THE FOLLOWING REQUEST:[" + marshalRequest(submitReadingInput) + "]");
        SubmitReadingOutput output = null;
        try {
            output = meterReadingsServices.submitReading(submitReadingInput);
        } catch (RuntimeException ex) {
            log.fatal("An unknown runtime exception error occurred whilst submitting meter reading",ex);
            throw ex;
        }


        if (output != null) {
            String statusCode = selfReaderServiceHelper.getSubmitResponseStatusCode(output);
            String statusDescription = selfReaderServiceHelper.getSubmitResponseStatusDescription(output);
            if (!SUBMIT_RESPONSE_OK.equalsIgnoreCase(statusCode)) {
                log.error(String.format("Unable to submit to webservice NMI(%s) METER(%s): %s ", meter.getNmi(), meter.getMeterNumber(), statusDescription));
                throw new WebmethodsException("Failed to submit: " + statusDescription);
            }

            // Test if there are any validation issues with the registers
            if (!forceSubmission) {
                if (!validateAndUpdateReturnedRegisters(meter, output)) {
                    throw new RegisterSubmissionFailedException(meter);
                }
            }

        } else {
            log.error(String.format("Unable to submit to webservice NMI(%s) METER(%s): NULL RETURN OBJECT ", meter.getNmi(), meter.getMeterNumber()));
            throw new WebmethodsException("Submission returned an error object");

        }

        return meter;
    }

    private String marshalRequest(SubmitReadingInput submitReadingInput) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            Marshaller m = submitJaxbContext.createMarshaller();
            m.marshal(new JAXBElement(new QName("", submitReadingInput.getClass().getSimpleName()), SubmitReadingInput.class, submitReadingInput), bos);
        } catch (JAXBException e) {
            log.error("Error Serializing request message to log file", e);
            return "";
        }
        return bos.toString();
    }

    /**
     * Determines if we need to force a submission
     *
     * @param attemptNumber the number of attempts to submit to the webservice
     * @param overrideFlag  set if user requests to force submissions
     * @return true if we need to force the submission
     */
    private boolean isSubmissionToBeForced(Integer attemptNumber, Boolean overrideFlag) {
        // If overrideFlag is set ALWAYS force submission
        if (overrideFlag)
            return true;

        return (attemptNumber >= 3);
    }

    /**
     * Returns true if there is NO issue with validation of a register
     *
     * @param meterDTO the meter we are submitting
     * @param output   the output of the webservice call
     * @return true if there is no issue with validation of the registers
     */
    private boolean validateAndUpdateReturnedRegisters(MeterDTO meterDTO, SubmitReadingOutput output) throws WebmethodsException {
        try {
            boolean valid = true;

            List<Register> registers = output.getSubmitReadingResponse().getReading().getMeter().getRegisters();
            if (registers != null) {
                for (Register register : registers) {
                    boolean isRegisterValid = register.getValidation().getValue().isValid();

                    valid &= isRegisterValid;
                    RegisterDTO registerDTO = meterDTO.getRegister(register.getId());
                    RegisterError registerError = null;
                    if (!isRegisterValid) {
                        registerError = new RegisterError(register.getValidation().getValue().getComment().getValue());
                    }
                    registerDTO.setErrorType(registerError);
                }
            }
            return valid;
        } catch (Exception e) {
            log.error("Unable to read response", e);
            throw new WebmethodsException(("Failed in interpreting validation for registers"));
        }
    }

}