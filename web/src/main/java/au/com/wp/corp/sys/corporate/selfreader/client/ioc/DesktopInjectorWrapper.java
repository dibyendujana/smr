package au.com.wp.corp.sys.corporate.selfreader.client.ioc;

import com.google.gwt.core.client.GWT;

public class DesktopInjectorWrapper implements InjectorWrapper {

	public SelfReaderInjector getInjector() {
		return GWT.create(DesktopInjector.class);
	}
}
