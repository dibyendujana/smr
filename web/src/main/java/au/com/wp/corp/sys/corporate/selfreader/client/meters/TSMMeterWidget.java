package au.com.wp.corp.sys.corporate.selfreader.client.meters;

import au.com.wp.corp.sys.corporate.selfreader.client.SelfReaderView;
import au.com.wp.corp.sys.corporate.selfreader.client.widgets.ValidationErrorWidget;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.MeterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterDTO;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.RegisterError;
import au.com.wp.corp.sys.corporate.selfreader.shared.dto.SkipCodeDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import eu.maydu.gwt.validation.client.ValidationProcessor;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * A MeterWidget for the RSM meter.
 *
 * @author Terence Mackie <terence.mackie@westernpower.com.au>
 */
public class TSMMeterWidget extends AbstractMeterWidget implements MeterWidget {
    interface MyUiBinder extends UiBinder<HTMLPanel, TSMMeterWidget> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    TextArea commentsField;
    @UiField
    IntegerBox register7Value;
    @UiField
    CheckBox register7SkipCodeBox;
    @UiField
    ListBox register7SkipCodeList;
    @UiField
    IntegerBox register10Value;
    @UiField
    CheckBox register10SkipCodeBox;
    @UiField
    ListBox register10SkipCodeList;
    @UiField
    IntegerBox register20Value;
    @UiField
    CheckBox register20SkipCodeBox;
    @UiField
    ListBox register20SkipCodeList;
    @UiField
    IntegerBox register30Value;
    @UiField
    CheckBox register30SkipCodeBox;
    @UiField
    ListBox register30SkipCodeList;
    @UiField
    IntegerBox register40Value;
    @UiField
    CheckBox register40SkipCodeBox;
    @UiField
    ListBox register40SkipCodeList;
    @UiField
    Label register7PreviousValue;
    @UiField
    Label register10PreviousValue;
    @UiField
    Label register20PreviousValue;
    @UiField
    Label register30PreviousValue;
    @UiField
    Label register40PreviousValue;
    @UiField
    Image meterHelp;
    @UiField
    ValidationErrorWidget registerValidationError;
    @UiField
    ValidationErrorWidget serverError;

    public TSMMeterWidget(MeterDTO meter, List<SkipCodeDTO> skipCodes, SelfReaderView.Presenter presenter, ValidationProcessor validator, boolean multiRegister) {
        this.multiRegister = multiRegister;
        initWidget(uiBinder.createAndBindUi(this));
        initMeter(meter, skipCodes, presenter, validator);
    }

    /**
     * Set up the widget with the values for the meter dto
     */
    protected void setupMeter() {
        register7SkipCodeBox.setEnabled(false);
        register7Value.setEnabled(false);
        register10SkipCodeBox.setEnabled(false);
        register10Value.setEnabled(false);
        register20SkipCodeBox.setEnabled(false);
        register20Value.setEnabled(false);
        register30SkipCodeBox.setEnabled(false);
        register30Value.setEnabled(false);
        register40SkipCodeBox.setEnabled(false);
        register40Value.setEnabled(false);

        List<RegisterDTO> registers = meter.getRegisters();
        for (RegisterDTO register : registers) {
            switch (Register.fromString(register.getId())) {
                case REGISTER_7:
                    assignValuesFromRegister(register, register7SkipCodeBox, register7PreviousValue, register7Value, register7SkipCodeList, serverError);
                    break;
                case REGISTER_10:
                    assignValuesFromRegister(register, register10SkipCodeBox, register10PreviousValue, register10Value, register10SkipCodeList, serverError);
                    break;
                case REGISTER_20:
                    assignValuesFromRegister(register, register20SkipCodeBox, register20PreviousValue, register20Value, register20SkipCodeList, serverError);
                    break;
                case REGISTER_30:
                    assignValuesFromRegister(register, register30SkipCodeBox, register30PreviousValue, register30Value, register30SkipCodeList, serverError);
                    break;
                case REGISTER_40:
                    assignValuesFromRegister(register, register40SkipCodeBox, register40PreviousValue, register40Value, register40SkipCodeList, serverError);
                    break;
                default:
                    break;
            }
        }
        assignValuesFromMeterToComment(commentsField);

    }


    protected void processSkipCodes() {
        addSkipCodesToListBox(register7SkipCodeList);
        addSkipCodesToListBox(register10SkipCodeList);
        addSkipCodesToListBox(register20SkipCodeList);
        addSkipCodesToListBox(register30SkipCodeList);
        addSkipCodesToListBox(register40SkipCodeList);
    }


    @Override
    public boolean isValid() {
        boolean valid = true;
        valid = valid & validateRegister(register7Value, register7SkipCodeBox, register7SkipCodeList);
        valid = valid & validateRegister(register10Value, register10SkipCodeBox, register10SkipCodeList);
        valid = valid & validateRegister(register20Value, register20SkipCodeBox, register20SkipCodeList);
        valid = valid & validateRegister(register30Value, register30SkipCodeBox, register30SkipCodeList);
        valid = valid & validateRegister(register40Value, register40SkipCodeBox, register40SkipCodeList);

        return valid;
    }

    @Override
    public boolean validateMeter(ValidationProcessor validate) {
        boolean valid = true;
        //just user first instance of code list for validation as it is just a name
        valid = valid & validateRegister(validate, Register.REGISTER_7.getCode().get(0), register7Value, register7SkipCodeBox, register7SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_10.getCode().get(0), register10Value, register10SkipCodeBox, register10SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_20.getCode().get(0), register20Value, register20SkipCodeBox, register20SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_30.getCode().get(0), register30Value, register30SkipCodeBox, register30SkipCodeList, registerValidationError);
        valid = valid & validateRegister(validate, Register.REGISTER_40.getCode().get(0), register40Value, register40SkipCodeBox, register40SkipCodeList, registerValidationError);
        return valid;
    }


    @Override
    public MeterDTO getMeter() {

        assignValuesToRegisterAndCreate(Register.REGISTER_7.getCode(), register7SkipCodeBox, register7Value, register7SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_10.getCode(), register10SkipCodeBox, register10Value, register10SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_20.getCode(), register20SkipCodeBox, register20Value, register20SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_30.getCode(), register30SkipCodeBox, register30Value, register30SkipCodeList);
        assignValuesToRegisterAndCreate(Register.REGISTER_40.getCode(), register40SkipCodeBox, register40Value, register40SkipCodeList);

        assignValuesFromCommentToMeter(commentsField);

        return meter;
    }

    @Override
    public void disable(boolean disable) {
        register7Value.setEnabled(!disable);
        register7SkipCodeList.setEnabled(!disable);
        register7SkipCodeBox.setEnabled(!disable);
        register10Value.setEnabled(!disable);
        register10SkipCodeList.setEnabled(!disable);
        register10SkipCodeBox.setEnabled(!disable);
        register20Value.setEnabled(!disable);
        register20SkipCodeList.setEnabled(!disable);
        register20SkipCodeBox.setEnabled(!disable);
        register30Value.setEnabled(!disable);
        register30SkipCodeList.setEnabled(!disable);
        register30SkipCodeBox.setEnabled(!disable);
        register40Value.setEnabled(!disable);
        register40SkipCodeList.setEnabled(!disable);
        register40SkipCodeBox.setEnabled(!disable);
        commentsField.setEnabled(!disable);
    }

    @UiHandler("register7SkipCodeBox")
    public void onReg7UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register7SkipCodeBox.getValue();
        register7Value.setEnabled(!unreadable);
        register7SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register7SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register10SkipCodeBox")
    public void onReg10UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register10SkipCodeBox.getValue();
        register10Value.setEnabled(!unreadable);
        register10SkipCodeList.setEnabled(unreadable);

        if(!unreadable)
            register10SkipCodeList.setSelectedIndex(0);

    }

    @UiHandler("register20SkipCodeBox")
    public void onReg20UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register20SkipCodeBox.getValue();
        register20Value.setEnabled(!unreadable);
        register20SkipCodeList.setEnabled(unreadable);

      if(!unreadable)
            register20SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("register30SkipCodeBox")
    public void onReg30UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register30SkipCodeBox.getValue();
        register30Value.setEnabled(!unreadable);
        register30SkipCodeList.setEnabled(unreadable);

           if(!unreadable)
            register30SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("register40SkipCodeBox")
    public void onReg40UnreadableChange(ValueChangeEvent<Boolean> e) {
        boolean unreadable = register40SkipCodeBox.getValue();
        register40Value.setEnabled(!unreadable);
        register40SkipCodeList.setEnabled(unreadable);

           if(!unreadable)
            register40SkipCodeList.setSelectedIndex(0);
    }

    @UiHandler("commentsField")
    public void onCommentsFocus(FocusEvent e) {
        if (COMMENT_FIELD_TEXT.equals(commentsField.getText())) {
            commentsField.setText("");
        }
    }

    @UiHandler("commentsField")
    public void onCommentsBlur(BlurEvent e) {
        if ((commentsField.getText() == null) || ("".equals(commentsField.getText().trim()))) {
            commentsField.setText(COMMENT_FIELD_TEXT);
        }
    }

    @UiHandler("meterHelp")
    public void onMeterHelpClick(ClickEvent event) {
        String help = getDefaultNonDialMeterHelp();
        presenter.displayHelp(help, meterHelp);
    }

    public enum Register {
        REGISTER_7("007,07"),
        REGISTER_10("010,10,101"),
        REGISTER_20("020,20,203"),
        REGISTER_30("030,30,302"),
        REGISTER_40("040,40,404"),
        NOVALUE("");

        private String value;

        Register(String value) {
            this.value = value;


        }

        /**
         * If we cant find the specific register we return novalue
         *
         * @param value the value to convert to enum
         * @return the enum associated with code
         */
        public static Register fromString(String value) {
            if (value != null) {
                for (Register b : Register.values()) {
                    String[] values = b.value.split(",");
                    List<String> valueSet = Arrays.asList(values);
                    if(valueSet.contains(value)){
                        return b;
                    }
                }
            }
            return NOVALUE;
        }

        public List<String> getCode() {
            return Arrays.asList(value.split(","));
        }

    }
}
