package au.com.wp.corp.sys.corporate.selfreader.server;

import au.com.wp.corp.sys.corporate.selfreader.server.handler.SelfReaderServiceImpl;
import au.com.wp.corp.sys.corporate.selfreader.server.util.HttpServletRequestHolder;
import au.com.wp.corp.sys.corporate.selfreader.shared.ProxyBean;
import au.com.wp.corp.sys.corporate.selfreader.shared.ProxyConfigBean;
import au.com.wp.corp.sys.corporate.selfreader.shared.exception.FailedCaptchaValidationException;
import junit.framework.Assert;
import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test Class for the Recaptcha RPC service
 */
@RunWith(MockitoJUnitRunner.class)
public class RecaptchaRPCTest {
    @Mock
    ReCaptcha mockRecaptcha;

    @Mock
    HttpServletRequestHolder mockHttpServletRequestHolder;

    @Mock
    ProxyConfigBean proxyConfigBean;

    @InjectMocks
    SelfReaderServiceImpl rpc = new SelfReaderServiceImpl();

    String challenge = "03AHJ_VutWq1NRieZnIijR91TZhCaTMjmHeMzvnG3DFq-ksTqwp1rPTm74lOoeg-HL-KyDe3H6Hg4jkGnbnvasRnU1xgExmTeJJya6dYWN9ceyWxjTGC7o6Ts-tdi848tMDgnfUeydOEhCv9l6-L8gE9l4knbNBbbDTw";
    String response = "eailand eeded";
    boolean threwException = false;
    String url = "127.0.0.1";
    HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
    ReCaptchaResponse mockRecaptchaResponse = mock(ReCaptchaResponse.class);


    @Before
    public void setup() {
        threwException = false;

        when(mockHttpServletRequestHolder.getHttpServletRequest()).thenReturn(mockHttpServletRequest);
        when(mockHttpServletRequest.getRemoteAddr()).thenReturn(url);

        when(mockRecaptcha.checkAnswer(url, challenge, response)).thenReturn(mockRecaptchaResponse);

        when(proxyConfigBean.getProxyBeans()).thenReturn(new ArrayList<ProxyBean>());

    }

    /**
     * Test that if the recaptcha validation returns true, then no exception is thrown
     */
    @Test
    public void testValidationPass() {
        when(mockRecaptchaResponse.isValid()).thenReturn(true);

        try {
            rpc.checkReCaptcha(challenge, response);
        } catch (FailedCaptchaValidationException re){
            threwException = true;
        }

        verify(mockRecaptcha).checkAnswer(url, challenge, response);
        Assert.assertEquals(false, threwException);
    }

     /**
     * Test that if the recaptcha validation returns false, then a validation exception is thrown
     */
    @Test
    public void testValidationFail() {
        when(mockRecaptchaResponse.isValid()).thenReturn(false);
        when(mockRecaptchaResponse.getErrorMessage()).thenReturn("wrong");

        try {
            rpc.checkReCaptcha(challenge, response);
        } catch (FailedCaptchaValidationException e) {
            threwException = true;
        }

        verify(mockRecaptcha).checkAnswer(url, challenge, response);
        Assert.assertEquals(true, threwException);


    }

    /**
    * Test that if the recaptcha service is unavailable
     * then a recaptcha exception is thrown
    */
   @Test
   public void testRecaptureServerNotAvailable() {
       boolean threwNotReachableException = false;
       when(mockRecaptchaResponse.isValid()).thenReturn(false);
       when(mockRecaptchaResponse.getErrorMessage()).thenReturn("recaptcha-not-reachable");
       try {
           rpc.checkReCaptcha(challenge, response);
       } catch (FailedCaptchaValidationException e) {
           threwNotReachableException = true;
       }

       verify(mockRecaptcha).checkAnswer(url, challenge, response);
       Assert.assertEquals(true, threwNotReachableException);
       Assert.assertEquals(false, threwException);
   }
}
