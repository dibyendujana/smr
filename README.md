

Here are the links to each of the environments... 

[Local GWT Debug](http://127.0.0.1:8888/online/selfreader/SelfReader.html?gwt.codesvr=127.0.0.1:9997) use a remote debug session
connected to localhost:7000

[Dev](https://ho-gfazd01:38191/online/selfreader)

[Test]( https://ho-gfazt01:38191/online/selfreader)

[Production](https://services.westernpower.com.au/online/selfreader/)

Test NMI: 8001487139 5
Test Meter: 0200378901

To debug in local
1. Clean and package (root)
2. Run gwt:debug (web)
3. Create a Remote Configuration (on port 7000 for debugging purpose)
